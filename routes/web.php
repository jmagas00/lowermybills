<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     $title = 'Home';    
//     return view('welcome', compact('title'));
// });

// Route::get('/', function () {
//     $title = 'Home';    
//     return view('welcome', compact('title'));
// });
Route::get('/',function () {
    $title = 'Home';    
    return view('welcome', compact('title'));
});

Route::post('/', 'RegController@insert');

Route::get('contact', 'ContactUsController@create')->name('contact');
Route::post('contact', 'ContactUsController@store')->name('contact.store');

Route::get('/mortgagecalculator', function () {
    $title = 'Mortgage Calculator'; 
    return view('mortgage/mortgage-calculator', compact('title'));
});
Route::get('/mortgageanalysis', function () {
    $title = 'Mortgage Analysis'; 
    return view('mortgage/mortgage-analysis', compact('title'));
});
Route::get('/mortgageadvisor', function () {
    $title = 'Mortgage Advisor'; 
    return view('mortgage/mortgage-advisor', compact('title'));
});
Route::get('/refinancecalculator', function () {
    $title = 'Refinance Calculator'; 
    return view('mortgage/refinance-calculator', compact('title'));
});
Route::get('/mortgagecomparisontool', function () {
    $title = 'Mortgage Comparison Tool'; 
    return view('mortgage/mortgage-comparison-tool', compact('title'));
});
Route::get('/typesofmortgages', function () {
    $title = 'types-of-mortgages'; 
    return view('mortgage/types-of-mortgages', compact('title'));
});
Route::get('/mortgagebasics', function () {
    $title = 'mortgage-basics'; 
    return view('mortgage/mortgage-basics', compact('title'));
});
Route::get('/firsttimehomebuyers', function () {
    $title = 'first-time-home-buyers'; 
    return view('mortgage/first-time-home-buyers', compact('title'));
});
Route::get('/howtoguides', function () {
    $title = 'how-to-guides'; 
    return view('mortgage/how-to-guides', compact('title'));
});
Route::get('/disclosure', function () {
    $title = 'Disclosure'; 
    return view('disclosure', compact('title'));
});
//Types of Mortgages
Route::get('/read-and-learn/what-is-an-interest-rate-reduction-refinance-loan', function () {
    $title = 'What is an Interest Rate Reduction Refinance Loan?'; 
    return view('mortgage/read-and-learn/what-is-an-interest-rate-reduction-refinance-loan', compact('title'));
});
Route::get('/read-and-learn/what-is-a-home-equity-conversion-mortgage', function () {
    $title = 'What is a Home Equity Conversion Mortgage?'; 
    return view('mortgage/read-and-learn/what-is-a-home-equity-conversion-mortgage', compact('title'));
});
Route::get('/read-and-learn/15-year-fixed-rate-mortgage', function () {
    $title = '15 year Fixed Rate Mortgage'; 
    return view('mortgage/read-and-learn/15-year-fixed-rate-mortgage', compact('title'));
});
Route::get('/read-and-learn/what-is-a-subprime-mortgage', function () {
    $title = 'What is a Subprime Mortgage?'; 
    return view('mortgage/read-and-learn/what-is-a-subprime-mortgage', compact('title'));
});
Route::get('/read-and-learn/veteran-loans', function () {
    $title = 'Veteran Loans'; 
    return view('mortgage/read-and-learn/veteran-loans', compact('title'));
});
Route::get('/read-and-learn/fha-loans', function () {
    $title = 'FHA Loans'; 
    return view('mortgage/read-and-learn/fha-loans', compact('title'));
});
Route::get('/read-and-learn/fixed-vs-adjustable', function () {
    $title = 'Fixed vs Adjustable'; 
    return view('mortgage/read-and-learn/fixed-vs-adjustable', compact('title'));
});
Route::get('/read-and-learn/cash-out', function () {
    $title = 'Cash Out'; 
    return view('mortgage/read-and-learn/cash-out', compact('title'));
});
Route::get('/read-and-learn/15-year-vs-30-year', function () {
    $title = '15 year vs 30 year'; 
    return view('mortgage/read-and-learn/15-year-vs-30-year', compact('title'));
});
Route::get('/read-and-learn/30-year-fixed-rate-mortgage', function () {
    $title = '30 year Fixed Rate Mortgage'; 
    return view('mortgage/read-and-learn/30-year-fixed-rate-mortgage', compact('title'));
});
//Mortgage Basics
Route::get('/read-and-learn/why-are-interest-rates-so-important', function () {
    $title = 'Why are Interest Rates so important?'; 
    return view('mortgage/read-and-learn/why-are-interest-rates-so-important', compact('title'));
});
Route::get('/read-and-learn/how-trumps-tax-cuts-and-job-act-could-affect-the-market', function () {
    $title = 'How Trump\'s Tax Cuts and Job Act could affect the market?'; 
    return view('mortgage/read-and-learn/how-trumps-tax-cuts-and-job-act-could-affect-the-market', compact('title'));
});
Route::get('/read-and-learn/adjustable-rate-mortgage', function () {
    $title = 'Adjustable Rate Mortgage'; 
    return view('mortgage/read-and-learn/adjustable-rate-mortgage', compact('title'));
});
Route::get('/read-and-learn/different-types-mortgages', function () {
    $title = 'Different Types of Mortgages'; 
    return view('mortgage/read-and-learn/different-types-mortgages', compact('title'));
});
Route::get('/read-and-learn/why-should-i-refinance-my-home', function () {
    $title = 'Why should I Refinance my home?'; 
    return view('mortgage/read-and-learn/why-should-i-refinance-my-home', compact('title'));
});
Route::get('/read-and-learn/home-ownership-risks-come', function () {
    $title = 'Home Ownership risks come'; 
    return view('mortgage/read-and-learn/home-ownership-risks-come', compact('title'));
});
Route::get('/read-and-learn/condo-or-house', function () {
    $title = 'Condo or House?'; 
    return view('mortgage/read-and-learn/condo-or-house', compact('title'));
});
Route::get('/read-and-learn/what-is-the-difference-between-a-reverse-mortgage-and-a-cash-out-refinance', function () {
    $title = 'What is the difference between a Reverse Mortgage and a Cash Out Refinance?'; 
    return view('mortgage/read-and-learn/what-is-the-difference-between-a-reverse-mortgage-and-a-cash-out-refinance', compact('title'));
});
//First Time Home Buyers
Route::get('/read-and-learn/what-is-apr', function () {
    $title = 'What is APR?'; 
    return view('mortgage/read-and-learn/what-is-apr', compact('title'));
});
Route::get('/read-and-learn/should-i-see-my-bank-or-go-with-an-online-lender', function () {
    $title = 'Should I see my bank or go with an online lender?'; 
    return view('mortgage/read-and-learn/should-i-see-my-bank-or-go-with-an-online-lender', compact('title'));
});
Route::get('/read-and-learn/should-i-continue-renting-or-buy-a-home', function () {
    $title = 'Should I continue renting or buy a home?'; 
    return view('mortgage/read-and-learn/should-i-continue-renting-or-buy-a-home', compact('title'));
});
Route::get('/read-and-learn/closing-costs-hidden-costs-come-mortgage', function () {
    $title = 'The Hidden Costs That Come With a Mortgage'; 
    return view('mortgage/read-and-learn/closing-costs-hidden-costs-come-mortgage', compact('title'));
});
Route::get('/read-and-learn/youve-decided-buy-home-happens-now', function () {
    $title = 'So you\'ve decided to buy a home, happens now?'; 
    return view('mortgage/read-and-learn/youve-decided-buy-home-happens-now', compact('title'));
});
Route::get('/read-and-learn/buying-foreclosed-property', function () {
    $title = 'Buying Foreclosed Property'; 
    return view('mortgage/read-and-learn/buying-foreclosed-property', compact('title'));
});
Route::get('/read-and-learn/what-is-a-mortgage-broker', function () {
    $title = 'What is a Mortgage Broker?'; 
    return view('mortgage/read-and-learn/what-is-a-mortgage-broker', compact('title'));
});
//How to Guides
Route::get('/read-and-learn/how-to-buy-a-home-with-student-debt', function () {
    $title = 'How to buy a Home with Student Debt?'; 
    return view('mortgage/read-and-learn/how-to-buy-a-home-with-student-debt', compact('title'));
});
Route::get('/read-and-learn/how-to-buy-a-home-as-an-immigrant', function () {
    $title = 'How to buy a Home as an Immigrant?'; 
    return view('mortgage/read-and-learn/how-to-buy-a-home-as-an-immigrant', compact('title'));
});
Route::get('/read-and-learn/how-can-you-get-cash-from-your-home', function () {
    $title = 'How can you get cash from your Home?'; 
    return view('mortgage/read-and-learn/how-can-you-get-cash-from-your-home', compact('title'));
});
Route::get('/read-and-learn/expect-mortgage-process', function () {
    $title = 'Expect Mortgage Process'; 
    return view('mortgage/read-and-learn/expect-mortgage-process', compact('title'));
});
Route::get('/read-and-learn/questions-ask-meeting-lender', function () {
    $title = 'Questions Ask Meeting Lender'; 
    return view('mortgage/read-and-learn/questions-ask-meeting-lender', compact('title'));
});
Route::get('/read-and-learn/what-is-homeowners-insurance', function () {
    $title = 'What is Homeowners Insurance?'; 
    return view('mortgage/read-and-learn/what-is-homeowners-insurance', compact('title'));
});
//footer links
Route::get('/ad-targetting-policy', function () {
    $title = 'Ad Targetting Policy'; 
    return view('mortgage/ad-targetting-policy', compact('title'));
});
Route::get('/licenses-and-disclosure', function () {
    $title = 'Licenses and Disclosure'; 
    return view('mortgage/licenses-and-disclosure', compact('title'));
});
Route::get('/our-privacy-notice', function () {
    $title = 'Our Privacy Notice'; 
    return view('mortgage/our-privacy-notice', compact('title'));
});
Route::get('/terms-and-conditions', function () {
    $title = 'Terms and Conditions'; 
    return view('mortgage/terms-and-conditions', compact('title'));
});
Route::get('/unsubscribe', function () {
    $title = 'Unsubscribe'; 
    return view('mortgage/unsubscribe', compact('title'));
});
//mortgage-advisor
Route::get('/mortgage/mortgage-advisor/harp/{monthlypayment}', function () {
    $title = 'Harp'; 
    return view('mortgage/mortgage-advisor/harp', compact('title' , 'monthlypayment'));
});
Route::get('/mortgage/mortgage-advisor/no-match', function () {
    $title = 'No Match'; 
    return view('mortgage/mortgage-advisor/no-match', compact('title'));
});
Route::get('/mortgage/mortgage-advisor/undefined', function () {
    $title = 'Error 404'; 
    return view('mortgage/mortgage-advisor/undefined', compact('title'));
});
//mortgage-analysis
Route::get('/mortgage/mortgage-analysis/pay-less-interest', function () {
    $title = 'Lower Monthly Payment A'; 
    return view('mortgage/mortgage-analysis/pay-less-interest', compact('title'));
});
Route::get('/mortgage/mortgage-analysis/lower-monthly-payment-d', function () {
    $title = 'Lower Monthly Payment D'; 
    return view('mortgage/mortgage-analysis/lower-monthly-payment-d', compact('title'));
});
//subscription
Route::get('/subscription/registration', function () {
    $title = 'Payment Fixer Subscription'; 
    return view('subscription/registration', compact('title'));
});