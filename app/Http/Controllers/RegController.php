<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegController extends Controller
{
  
   public function insertform(){
      return view('welcome');
   }
	
   public function insert(Request $request){
      $firstname = $request->input('firstname');
      $lastname = $request->input('lastname');
      $age = $request->input('age');
      $phone_mobile = $request->input('phone_mobile');
      $mobile = $request->input('mobile');
      $address = $request->input('address');
      $city = $request->input('city');
      $state = $request->input('state');
      $zip = $request->input('zip');
      $mail = $request->input('mail');
      $Lowering_Mortgage = $request->input('Lowering_Mortgage');
      $Home_Owner_Insurance = $request->input('Home_Owner_Insurance');
      $Life_Insurance = $request->input('Life_Insurance');
      $Reverse_Mortgage = $request->input('Reverse_Mortgage');
      $Auto_Insurance = $request->input('Auto_Insurance');
      $Medical_Insurance_Plan = $request->input('Medical_Insurance_Plan');
      $Home_Security = $request->input('Home_Security');
      $Electric_Bill = $request->input('Electric_Bill');
      $Home_Warranty = $request->input('Home_Warranty');
      $Internet_Bill = $request->input('Internet_Bill');
      $Cell_Phone_Bill = $request->input('Cell_Phone_Bill');
      $Home_Phone_Bill = $request->input('Home_Phone_Bill');
      $Credit_Card_Bill = $request->input('Credit_Card_Bill');
      $Cable_Satellite_Tv_Bill = $request->input('Cable_Satellite_Tv_Bill');


      $_token= $request->input('_token');
      DB::insert('insert into users (firstname,lastname,age,phone_mobile,mobile,address,city,state,zip,mail,remember_token,Lowering_Mortgage,Home_Owner_Insurance,Life_Insurance,Reverse_Mortgage,Auto_Insurance,Medical_Insurance_Plan,Home_Security,Electric_Bill,Home_Warranty,Internet_Bill,Cell_Phone_Bill,Home_Phone_Bill,Credit_Card_Bill,Cable_Satellite_Tv_Bill) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[$firstname,
      	$lastname,
      	$age,
      	$phone_mobile,
      	$mobile,
      	$address,
      	$city,
      	$state,
      	$zip,
      	$mail,
      	$_token,
      	$Lowering_Mortgage,
	    $Home_Owner_Insurance,
	    $Life_Insurance,
	    $Reverse_Mortgage,
	    $Auto_Insurance,
	    $Medical_Insurance_Plan,
	    $Home_Security,
	    $Electric_Bill,
	    $Home_Warranty,
	    $Internet_Bill,
	    $Cell_Phone_Bill,
	    $Home_Phone_Bill,
	    $Credit_Card_Bill,
	    $Cable_Satellite_Tv_Bill]);
      return redirect()->back()->with('success', 'IT WORKS!');
   }





}


