<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use View;

class MortgageController extends Controller
{

    public function mortgagecalculator()
    {
        return view('/mortgage/mortgagecalculator');
    }

    public function mortgageanalysis()
    {
        return view('/mortgage/mortgageanalysis');
    }

    public function mortgageadvisor()
    {
        return view('/mortgage/mortgageadvisor');
    }

 
}
