<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname',50)->nullable();
            $table->string('lastname',50)->nullable();
            $table->string('age',3)->nullable();
            $table->string('phone_mobile',50)->nullable();
            $table->string('mobile',50)->nullable();
            $table->string('address',50)->nullable();
            $table->string('zip',11)->nullable();
            $table->string('city',50)->nullable();
            $table->string('state',50)->nullable();
            $table->string('Lowering_Mortgage',50)->nullable();
            $table->string('Home_Owner_Insurance',50)->nullable();
            $table->string('Life_Insurance',50)->nullable();
            $table->string('Reverse_Mortgage',50)->nullable();
            $table->string('Auto_Insurance',50)->nullable();
            $table->string('Medical_Insurance_Plan',50)->nullable();
            $table->string('Home_Security',50)->nullable();
            $table->string('Electric_Bill',50)->nullable();
            $table->string('Home_Warranty',50)->nullable();
            $table->string('Internet_Bill',50)->nullable();
            $table->string('Cell_Phone_Bill',50)->nullable();
            $table->string('Home_Phone_Bill',50)->nullable();
            $table->string('Credit_Card_Bill',50)->nullable();
            $table->string('Cable_Satellite_Tv_Bill',50)->nullable();
            $table->string('mail')->unique();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
