@extends('layout.app')

@section('main-content')
<section id="mu-service">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="">
							<!-- Title -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-title">
										<h1>Payment Fixer Login and Register Form</h1>
									</div>
								</div>
							</div>
                            <!-- Start Service Content -->
                           
							<div class="row">
								<div class="col-md-12">
									<div class="mu-single-service-content">
										<div class="row">
											<!-- Start single service -->
											
											<!-- End single service -->
											<!-- Start single service -->
											<div class="col-sm-6 ">
                                                <div class="form">
                                                <h2 align="center">Log In to our site</h2>
                                                    <form>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-12">
                                                         
                                                            <input type="email" class="form-control" id="email1" placeholder="Email">
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                   
                                                            <input type="password1" class="form-control" id="passwrd" placeholder="Password">
                                                            </div>
                                                        </div>
                                                       
                                       
                                                        <button type="submit" class="btn btn-primary">Log in</button>
                                                    </form>
                                                    </div>
												</div>
                                            <!-- End single service -->
                                            
											<!-- Start single service -->
											<div class="col-sm-6">
                                            <div class="form">
                                            <h2 align="center">Sign Up Now</h2>
                                                    <form>
                                                    
                                                        <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                         
                                                         <input type="fname" class="form-control" id="firstname" placeholder="First Name">
                                                         </div>
                                                         <div class="form-group col-md-12">
                                                
                                                         <input type="lname" class="form-control" id="lastname" placeholder="Last Name">
                                                         </div>
                                                            <div class="form-group col-md-12">
                                                         
                                                            <input type="email" class="form-control" id="email" placeholder="Email">
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                   
                                                            <input type="password" class="form-control" id="password" placeholder="Password">
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                   
                                                            <input type="cpassword" class="form-control" id="confirmpassword" placeholder="Confirm Password">
                                                            </div>
                                                        </div>
                                                                                             
                                                        <button type="submit" class="btn btn-primary">Get Started</button>
                                                    </form>
                                                </div>
                                                </div>
                                              
											</div>
											<!-- End single service -->
										</div>
									</div>
								</div>
							</div>
							<!-- End Service Content -->
						</div>
					</div>
				</div>
			</div>
        </section>
        <br><br> <br><br>
@endsection