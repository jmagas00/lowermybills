@extends('layout.app')

@section('main-content')
<main>
    <section id="mu-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-footer-area">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mu-footer-title">
									<h2>Our Privacy Notice</h2>
									<p>May 20, 2018</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
						<div class="col-md-6">
								<div class="mu-footer-left">
									<h3>We Respect Your Privacy</h3>
									<p>In PaymentFixer.com®, owned and run by PF Mortgage Services, Inc. (together, "PaymentFixer.com", "we", "us", or "our"), your privacy is a top priority for us and we're dedicated to safeguarding your privacy online. This Privacy Notice summarizes the measures we have taken to make sure that you get a fantastic experience at PaymentFixer.com, and provides information on how we handle any personal information you will submit.</p>
									<p>This Privacy Notice applies to the site, PaymentFixer.com. Please be aware that this Notice applies only to the data collected through this site and not to sites maintained by other organizations or companies to which we connect. Additionally, this Notice applies only to data gathered online, not offline, and also how they're used, procured, and shared by PaymentFixer.com. Additionally, it describes the options available to you concerning our usage of your private information and ways to get and update this info.<br />
									<img class="" src="{{asset('img/seal.png')}}"></p>
									<p>When you've got an open source privacy or information utilize concern that we've not addressed satisfactorily, please contact our U.S.-based third party dispute resolution provider (free of cost) in https://feedback-form.truste.com/watchdog/request.</p>
									
									<h3>Information Collection and Use</h3>
									<p>We can collect and use private information regarding you to respond to your request for goods and services provided through this site with the intention of fitting you to suppliers that have paid advertising charges to get queries. We'll also collect information so as to inform you about goods, services, and other opportunities we believe will be of interest for you. The information we gather could be from the following sources:</p>
									<ul>
										<li>Information given by you in connection with getting quotes from suppliers for house purchase loans, home refinance loans, or home equity loans. As an instance, you might be requested to supply your entire name, address, phone number, Social Security Number, or other financial information based upon your request.</li>
										<li>Information given by you in connection with getting a charge card. As an instance, you might be asked to submit your entire name and address. Sometimes, you can also be requested to submit additional personal information after you're related to the credit card issuer's web site.</li>
										<li>Info given by you in connection with getting quotes from suppliers for different goods or services like long distance service, internet access, home improvement, and insurance.</li>
										<li>Information given by you in connection with acquiring a specific product or service provided by our network suppliers.</li>
										<li>Information that you provide us when you enroll to get communications from us (including the email address).</li>
										<li>Information you provide us when you enter promotions or sweepstakes offered by PaymentFixer.com (including your entire name, physical address and email address).</li>
										<li>Information you supply us via phone and web based polls, customer support correspondence, and general opinions.</li>
										<li>Information you provide us when you apply for employment with PaymentFixer.com.</li>
									</ul>
									<br />
									<p>We also gather demographic information. Demographic information is other details like gender, zip code, or some other info that's not connected to your private info. Additionally, we might get information about you from other online or offline resources, such as third parties from whom we affirm customer self-reported info, and confirm information we have about you. This assists us to upgrade, expand and examine our documents and supply services and products which could be of interest to you.</p>
									<p>Examples of those kinds of private information which might be obtained from third parties to confirm information we have about you might comprise Credit band.</p>

									<h3>Information Sharing and Disclosure</h3>
										<p>We discuss your personal information with other businesses so they can advertise their goods or services to you. If You Don't want us to discuss your personal information with those businesses, contact us in support@paymentfixer.com</p>
										<p>We Also share personal Information Regarding you under the following Conditions:</p>
										<p><strong>Loan Products or Service Requests</strong> – Should you submit a request for advice about financing product or service provided through this site, we'll discuss the personal information found in your petition form (including your entire name, address, phone number, property location, self-assessed price, or Social Security Number) with third parties, including banks, mortgage lenders and agents, and direct aggregators (together, "Lender" or "Lenders") in our customer community to process and fulfill your request. In certain conditions, we may get additional information regarding you and/or discuss that info with Lenders within our community. By way of instance, we might use your Social Security Number, your Year of Birth, or even your own private information to confirm your self-reported info and assist us better suit you with Lenders.</p>
										<p>All of Lenders in our community have entered into arrangements with PaymentFixer, and therefore are needed to follow state and federal privacy regulations. By submitting your request to PaymentFixer.com, you know that Lenders may contact you about a listed line by phone or cellular apparatus, such as SMS and MMS, via automated or prerecorded methods, email, or regular email depending on the information which you supplied to us to be able to process and fulfill your request, even in the event that you have opted into the National Do Not Call List administered by the Federal Trade Commission, any state equivalent don't Call List, or Don't Call List of an inner firm. Your consent to be contacted isn't required as a requirement to buy a service or good. If among these Lenders contacts you and you need to not be contacted by that Bank connected to your query, then you need to specifically make a request to this Bank to not contact you. Please be aware that the Lenders can keep the information which you supplied whether you decide to utilize their services.</p>
										
										<p>For a complete list of licenses and disclosures, please click here</p>
										
										<p><strong>Credit Card Requests</strong> – Should you want to match into a credit card supplier, you'll be asked to supply your personal information (for example, your entire name and residence address).</p>
										<p><strong>Other Products or Service Requests</strong> – Should you submit a request for advice about a product or service aside from a loan such as home improvement, realtor services, Internet access, insurance or extended distance service, then you'll be asked to supply your personal information (for example, your entire name, address, phone number and email address). The personal information you submit is accumulated by PaymentFixer.com and shared with all the specific company provider working the service or supplying the product that you asked. Where the agency is managed by a number of our company suppliers, the collection, use and disclosure of your private data will be subject to that company suppliers' privacy note. Our company providers can contact you by phone, email, or regular email depending on the information which you provided to us even in the event that you have opted into the National Do Not Call List administered by the Federal Trade Commission, any state equivalent don't Call List, or Don't Call List of an inner firm. If a number of our suppliers contacts you by phone and you wish to not be contacted by that company provider related to your query, then you need to specifically make a request to this supplier to not contact you.</p>
										<p><strong>Vendors</strong> – We occasionally disclose the information that we collect to companies that run various services for us like advertising distribution, advertisements, processing manuals, or specific product functionalities. These sellers simply have access to personal information required to carry out their purposes, and may not discuss it with other people or use it for another purpose.</p>
										<p><strong>Job Postings and Affiliate Providers</strong> – We allow for the online submission of resumes and employment related advice. Please be aware that the data (including personal information) gathered through the online job application procedure on the Career section of our site isn't subject to the Privacy Notice. But we would like to guarantee you that advice obtained from our occupation website is going to be utilized only for the purpose of accepting and evaluating your submission for potential employment and the associated functions described on our Career page.</p>
										<p><strong>Affiliate Providers</strong> – We can also collect personal and company information submitted via the "Affiliate Program" section of our site. This information filed through the "Affiliate Program" isn't subject to the Privacy Notice. However, the data obtained via the Affiliate Program will be utilized only for the purpose of accepting and evaluating your submission as a potential Affiliate Provider.</p>
										<p><strong>Subsidiaries, Parent or Sister Companies</strong> – We can share demographic and personal information regarding you with our subsidiaries, parent company, or even sister organizations to provide you more choices to your client requests and supply you better service. By way of instance, we might share your data with ClassesUSA.com. If You Don't want your information shared in this way, please email us in support@paymentfixer.com</p>
										<p><strong>Aggregate, Non-Directly Identifying Information.</strong> We may share aggregated information (i.e., information about you and other clients jointly, but not directly and specifically identifiable to you) along with other pseudonymized private information we collect with third parties, such as affiliates to develop and provide targeted advertisements on the website and on the sites of third parties. If you'd prefer that we don't use cookies with your site encounter, or would rather limit the usage of cookies using network advertisements suppliers, please visit the "Offering You Choices" section below.</p>
										<p><strong>As Permitted by Law</strong> – We might be asked to share your individual information with law enforcement or government agencies in response to subpoenas, court orders, federal/state audits or comparable legal procedure. We think it's vital to share information so as to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any individual, violations of PaymentFixer.com's Conditions of use, or as otherwise required by law enforcement.</p>
										<p><strong>Business Transfer</strong> – Just like with any other company, it's likely that PaymentFixer.com from the future could merge with or be acquired by another corporation. If such an acquisition occurs, the successor company would have access to the data saved by PaymentFixer.com, such as your own personal information, client and company account information, but would continue to be bound by this Notice unless you agree otherwise. You'll be notified via a prominent notice on the site of any change in ownership or material modifications to the applications of your personal info, in addition to any alterations to options you might have regarding your personal info.</p>
										<p><strong>Non-Affiliated Companies:</strong> From time to time, we will discuss your individual data with non-affiliated third parties. By way of instance, we may share personal information to deliver a product or service which we feel could be of interest to you. We may also share or sell your private data to a third party advertiser which will use your private information for their marketing purposes.</p>
									</div>
								</div>
                            <div class="col-md-6">
                                <div class="mu-footer-right">
																	<h3>Advertisements and Third Party Ad Servers</h3>
																	<p>We participate with a third party to display advertisements on the website or to handle our advertisements on other websites. For example, when seeing a third party site, you might be given a banner ad, a "pop up" window advertisements PaymentFixer.com, or even a exhibited a text link to the next party website. Please be aware that such ads are served with the third party site which you've seen and aren't commanded by PaymentFixer.com. Additionally, such advertisements could be placed by third party ad servers or ad networks. Our third party suppliers can use technology such as cookies and action tags to collect information regarding your activities on this website and other websites so as to supply you advertisements depending upon your surfing activities and pursuits. Should you would like to get this information used for the purpose of helping you interest-based advertisements, you might opt-out by simply clicking here. Please be aware that this doesn't opt you from being served advertisements. You may continue to get generic advertisements.</p>
																	
																	<h3>Tracking Technologies</h3>
																	<p>Technologies like biscuits or similar technology are utilized by PaymentFixer.com and our advertising suppliers, display advertisements network suppliers, affiliates, and other campaign performance monitoring and analytics service suppliers, including Lead Intelligence, Inc. (d/b/a Jornaya), whose privacy policy may be seen here: https://www.jornaya.com/consumer-privacy-policy. These technologies are employed in assessing trends, restarting the website, monitoring users' movements around the site (such as the record of visual snapshots of your action on the website) and to collect demographic information regarding our user base as a whole. We might receive reports based on using these technologies by those firms on an individual in addition to aggregated basis.</p>
																	<p>We normally use cookies to decrease the time it takes for pages to load in your pc, to help with client tracking, to keep the visitors source or advertising campaign which explains how you reached our website and to provide targeted advertisements on the site, sites owned by our sister companies, or sites of third parties. To be able to help save effort and time, we might also use cookies in many areas on our site to pre-populate or automatically fill out web forms for you once you revisit our site. Users may control the use of cookies in the browser degree. If you refuse cookies, you may still use our website, however, your ability to utilize several features or areas of the website might be restricted.</p>
																	<p>As is true of all web websites, we collect certain information automatically and store it in log files. The data we monitor may consist of such info as browser type, Internet service provider (ISP), referring/exit pages, platform type, date/time postage, IP address and number of clicks. We can combine information we obtain through using cookies and third party web analytics solutions with private information, additional aggregate or pseudonymized information, including a exceptional ID We might assign you.</p>
																	
																	<h3>Offering You Choices</h3>
																	<p><strong>Marketing Email Options</strong> – You will register to receive communications from us right on our site or via a third party. Upon registering, we'll occasionally send you e-mails with promotions and offers. In case you no longer want to receive these messages, please tell us by sending an email to support@paymentfixer.com (you might also opt-out by clicking the unsubscribe link in our emails).</p>
																	<p><strong>Third Party Advertisers</strong> – Please be aware that PaymentFixer.com can only control its own mailing list and coverages. Third party advertisers who maintain their own mailing lists can send communications which market their services, and you might have to speak to these parties directly so as to stop receiving their email communications.</p>

																	<h3>Confidentiality and Security</h3>
																	<p>The safety of your personal information is extremely significant at PaymentFixer.com and we have lots of measures to protect it. By way of instance, we restrict access to private information to those employees or agents who help us in supplying requested services and products for you and maintain physical, electronic, and procedural safeguards that comply with federal standards to guard personal information regarding you.</p>
																	<p>When you enter sensitive information (for example, a Social Security number) on our order forms, we encrypt the transmission of the information using secure socket layer technology (SSL).</p>
																	<p>We follow generally accepted criteria to guard the private information submitted to us, both during transmission and once we get it. No method of transmission over the world wide web, or method of electronic storage, is 100% secure, however. Thus, we can't guarantee its absolute security. In case you have any questions about security on the Web site, you are able to contact us in support@paymentfixer.com.</p>

																	<h3>Surveys</h3>
																	<p>We might give you the chance to take part in surveys on our website. If you take part, we'll request certain personal information from you. Participation in these surveys is totally voluntary and you therefore have a choice whether to disclose this info. We utilize this information for market research purposes and to enhance our services. We'll sometimes offer pseudonymized aggregated data about our customers in reports to third parties. These reports won't identify individual users.</p>
																	<p>We might use a third party service provider to conduct these polls; this corporation will be banned from using our customers' private information for any other purpose.</p>

																	<h3>Third Party Links</h3>
																	<p>This Privacy Notice applies to all information which you supply to PaymentFixer.com. But when you visit sites to which PaymentFixer.com hyperlinks, bear in mind you'll be interacting with a third party that operates under its own privacy and safety policy. Should you choose to acquire certain services via a third party website, the information gathered by the third party is governed by that third party′s solitude Notice. A third party website will even have its own policy concerning the usage of cookies and clear gifs. We invite you to review the privacy notices of another service supplier from whom you request services.</p>

																	<h3>Social Media Features</h3>
																	<p>Our Website includes Social Media Characteristics, like the Facebook Similar button.</p>
																	<p>This Attribute could collect your IP address, which page you're seeing on our website, and might set a cookie to permit the Characteristic to operate properly. Social Media Features and Widgets are hosted by a third party or hosted right on our website. Your interactions with these Attributes are regulated by the privacy note of the firm supplying it</p>

																	<h3>Framing</h3> 
																	<p>In some instances, we can use third parties to run parts of their PaymentFixer.com site. Though the specific section of this site might have a similar look as the PaymentFixer.com website, data submitted through these external third parties are subject to this firm′s privacy notice unless otherwise said.</p>
																	
																	<h3>A Special Note about Children’s Privacy</h3>
																	<p>You have to be at least 18 years old to use this site. We don't knowingly collect, use or disclose personal information regarding visitors under 18 decades old. If you're under 18 decades old, you may use the services provided on our site just in conjunction with your parents or guardians. Please see our Conditions of Use to learn more.</p>

																	<h3>Removing Your Information</h3>
																	<p>We'll keep your data for as long as your account remains active or as required to supply you solutions. Should you would like to ask that we no longer utilize your data to provide you services or upgrades on other goods, contact us in Customer Care. Additionally, we'll keep and use your data as required to comply with our regulatory or legal duties, solve disputes, and enforce our agreements.</p>
																	<p>Upon request, we'll supply you with advice about if we hold any of your private info. If your personal information changes, or if you no longer desire our service, you might update, amend, or request to have it removed from our mailing lists or manufacturing directory by emailing Client Maintenance, or by calling us by phone or postal mail at the contact information given below. We'll respond to your petition within 30 calendar days.</p>

																	<h3>Questions and Suggestions</h3>
																	<p>If you have any Queries, Suggestions or Concerns Regarding our Privacy Notice, You Might contact us</p>
																	<p>By sending an email to: <mailto:>support@paymentfixer.com</mailto:></p>
																	<p>By sending a letter to:</p>
																	<p><strong>PaymentFixer.com</strong><br />
																	Attn: Customer Care<br />
																	222 S. Main Street, 5th Floor,<br />
																	Salt Lake City, Utah, 8410<br />
																	<!-- Los Angeles, CA 90056<br /> -->
																	<!-- Phone No.: 855-901-7220 -->
																	Email: <a href="mailto:info@paymentfixer.com">INFO@PaymentFixer.com</a>
																	</p>

																	<h3>Changes to this Privacy Notice</h3>
																	<p>PaymentFixer.com can upgrade or alter this Privacy Notice from time to time. If there are material changes to this Privacy Notice or at how PaymentFixer.com can use your private information, we'll post such changes. You'll be advised by email if some of these material changes that affect using your personal information before it happens and requested to opt-in into the new usage of your private info. If you don't agree with all the adjustments to the way your data will be utilized, please don't continue to utilize this site.</p>
																	<p>If you don't agree for this note, please don't utilize any of this PaymentFixer.com website. When you ask rate quotations or other information via PaymentFixer.com, you're authorizing us to discuss info with our network suppliers, that will contact you via phone, email, or mail. If you don't want additional communication from them, please notify the supplier right. We reserve the right to make changes within this note at any moment. Please check the note every time you use our site to make certain you're aware of some changes in our privacy practices. Our Privacy Notice will indicate the date that it was last updated. Your continued use of the website will indicate your approval of these modifications to our Privacy Notice.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection