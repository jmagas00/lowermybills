@extends('layout.app')

@section('main-content')

<section id="mu-bills">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="">
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
                                <br><br><br>
									<h1>Mortgage Advisor</h1>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="card">
									<div class="card-body">
										<strong class="card-title">Recomendation:</strong>
										<h1 class="card-text">Take advantage of HARP</h1>
									</div>
								</div>
							</div>
								<div class="col-sm-6">
									<div class="card bg-success text-white">
										<div class="card-body">
											<strong class="card-title">Monthly Payment Saving:</strong>
											<h1 class="card-text">${{ app('request')->segment(4) }}</h1>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="mu-title">
										<br><br><br>
										<h3>Here's Why:</h3>
										<br><br><br>
										<h3><strong>HARP can help you qualify to refinance and reduce your monthly mortgage payment by as much as ${{ app('request')->segment(4) }}.</strong></h3>
										<p align="justify">HARP (Home Affordable Refinance Program) is a government program aimed at helping homeowners like you who have little or no equity qualify to refinance. Homeowners who use HARP reduce their monthly payments, get into shorter-term loans so that they can pay off their homes faster, or get out of ARMs and into safer, fixed rate mortgages.</p>
										<p align="justify"><a href="#">Take Advantage Of The Benefits Of HARP By Using This Easy Online Tool To Get Matched To Lenders</a></p>
										<br><br><br>
										<h3 class="display-4"> Here's why we think HARP could be a good fit for you:</h3>
										<br><br><br>
										<h3><strong> HARP can help you get a lower monthly payment</strong></h3>
										<p>By refinancing at today's rates you could reduce your monthly mortgage payment by as much as ${{ app('request')->segment(4) }}.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>

@endsection