@extends('layout.app')

@section('main-content')

<section id="mu-bills">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="">
						<!-- Title -->
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
                                <br><br><br>
                        
									<h1>Mortgage Advisor</h1>
                                    <br><br>
                                    <p align="center">Congratulations, we’ve looked at all the major types of mortgages out there and at today’s rates and you look to be in a great mortgage. Well done!</p>
								<span align="center"><i class="fas fa-check" style="font-size: 100px; color:green"></i></span>
                              
								</div>
							</div>
						</div>
						<!-- Start Feature Content -->
						
						<!-- End Feature Content -->
					</div>
				</div>
			</div>
		</div>
       ><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	</section>

@endsection