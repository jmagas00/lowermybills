@extends('layout.app')

@section('main-content')
<!-- Lower My Bills -->
<section id="mu-bills">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-bills-area">
						<!-- Title -->
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
									<h1>Mortgage Comparison</h1>
                                    
								</div>
							</div>
						</div>
						<!-- Start Feature Content -->
						<div class="row">
							<div class="col-md-6">
								<div class="mu-bills-right">
                                <p align="justify"><b>Our Goal:</b> Assist you to compare the advantages and disadvantages of various mortgages so that you may make an educated choice on which one you need to choose!
                                </p>
                                <h4 align="left">How it Works</h4>
									<ol>
										<li>
										We'll show you the Various mortgage options that are available to choose from
										</li>
										<li>
                                        You select which ones you find most intriguing and wish to find out more about.  

										</li>
										<li>
                                        We'll show them for you at a digestible format which will make it possible for you to weigh the advantages and disadvantages!  
                                        </li>
                                        
									</ol>
								</div>
							</div>
							<div class="col-md-6">
								<div class="mu-bills-left">
									<img class="" src="{{asset('img/pexels-photo-669615.jpeg')}}" alt="img')}}">
								</div>
							</div>
						</div>
						<!-- End Feature Content -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Bills -->
	

	
	<section id="para-center">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="para-center-area">
						<div class="row checkboxes">
							<div class="col-md-3">							
								<Label><input type="checkbox" class="big-checkbox" id="30-year-fixed" value="mortgage-comparison-30-year-fixed"><span>30 Year Fix</span></Label>
							</div>
							<div class="col-md-3">
								<Label><input type="checkbox" class="big-checkbox" id="15-year-fixed" value="mortgage-comparison-30-year-fixed"><span>15 Year Fix</span></Label>
							</div>
							<div class="col-md-3">
								<Label><input type="checkbox" class="big-checkbox" id="cash-out-refinance" value="mortgage-comparison-30-year-fixed"><span>Cash Out Refinance</span></Label>
							</div>
							<div class="col-md-3">
								<Label><input type="checkbox" class="big-checkbox" id="5-1-year-adjustable" value="mortgage-comparison-30-year-fixed"><span>5/1 Year Adjustable</span></Label>
							</div>
							<div class="col-md-12">
								<div class="para-center-area">
									<div class="row">
										<div class="col-md-3">
											<Label><input type="checkbox" class="big-checkbox" id="fha-loan" value="mortgage-comparison-30-year-fixed"><span>FHA Loan</span></Label>
										</div>
										<div class="col-md-3">
											<Label><input type="checkbox" class="big-checkbox" id="va-loan" value="mortgage-comparison-30-year-fixed"><span>VA Loan</span></Label>
										</div>
										<div class="col-md-3">
											<Label><input type="checkbox" class="big-checkbox" id="harp-refinance" value="mortgage-comparison-30-year-fixed"><span>HARP Refinance</span></Label>
										</div>
									</div>
								</div>
							</div>	
						</div>
						<input type="button" id="comparison" name="submit" class="submit btn btn-lg btn-success col-md-4" onClick="updateCompare(); window.location='#comparison';" href="#comparison" value="Update Comparison"><br>
						@include('layout.partials.mortgage-comparison-tool')
					</div>
				</div>
			</div>
		</div>
	</section>

<section id="mu-newsletter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-newsletter-area comparison" style="display:none;"> 
			  <div style="overflow: auto;">
                <table class="table table-bordered table-responsive-xl details mu-simplefilte table-striped" style="background-color: white !important; color: black;">
                  <thead>
                    <tr>
                      <th class="mu-simplefilter table-success">LOAN TYPE</th>
                      <th class="mu-simplefilter 30yf">30 YR FIXED</th>
                      <th class="mu-simplefilter 15yf">15 YR FIXED</th>
                      <th class="mu-simplefilter cashout">CASH OUT</th>
					  <th class="mu-simplefilter arm">5/1 ARM</th>
					  <th class="mu-simplefilter fha">FHA*</th>
					  <th class="mu-simplefilter va">VA*</th>
					  <th class="mu-simplefilter harp">HARP*</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
						<th class="mu-simplefilter table-success">GET CASH</th>
						<td class="30yf"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="15yf"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="cashout"><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
						<td class="arm"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="fha"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="va"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="harp"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
                    </tr>
                    <tr>
						<th class="mu-simplefilter table-success">LOWER<br>PAYMENT</th>
						<td class="30yf"><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
						<td class="15yf"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="cashout"><strong>DEPENDS ON TERM<strong></td>
						<td class="arm"><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
						<td class="fha"><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
						<td class="va"><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
						<td class="harp"><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
                    </tr>
                    <tr>
						<th class="mu-simplefilter table-success">LOWER Rate</th>
						<td class="30yf"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="15yf"><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
						<td class="cashout"><strong>DEPENDS ON TERM<strong></td>
						<td class="arm"><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
						<td class="fha"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="va"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="harp"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
                    </tr>
                    <tr>
						<th class="mu-simplefilter table-success">PAY OFF FASTER</th>
						<td class="30yf"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="15yf"><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
						<td class="cashout"><strong>DEPENDS ON TERM<strong></td>
						<td class="arm"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="fha"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="va"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="harp"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
                    </tr>
                    <tr>
						<th class="mu-simplefilter table-success">PAY LESS INTEREST</th>
						<td class="30yf"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="15yf"><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
						<td class="cashout"><strong>DEPENDS ON TERM<strong></td>
						<td class="arm"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="fha"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="va"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
						<td class="harp"><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
                    </tr>
                  </tbody>
				</table>     
</div>
				<h1 align="center">Mortgage Requirements</h1><br><br>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-3 30yf">
									<h5 align="center"><strong>30 Year Fixed</strong></h5>
									<ol>
										<li>Good to excellent credit, likely needs to be 600+</li>
										<li>Steady income</li>
										<li>At least 3% down payment</li>
									</ol> 
								</div>
								<div class="col-md-3 15yf">
									<h5 align="center"><strong>15 Year Fixed</strong></h5>
									<ol>
										<li>Low to no debt</li>
										<li>Excellent credit, likely needs to be 700+</li>
										<li>Steady income</li>
									</ol> 
								</div>
								<div class="col-md-3 cashout">
									<h5 align="center"><strong>Cash Out</strong></h5>
									<ol>
										<li>Must have owned current home for at least a year</li>
										<li>Must have a minimum credit score-usually higher than a normal/previous refinance requirement</li>
										<li>Current loan to value ratio must be around 85%</li>
									</ol> 
								</div>
								<div class="col-md-3 arm">
									<h5 align="center"><strong>5/1 ARM</strong></h5>
									<ol>
										<li>Excellent credit, likely needs to be 700+</li>
										<li>At least 5% down payment</li>
										<li>Steady income</li>
									</ol> 
								</div>
								<div class="col-md-3 fha">
									<h5 align="center"><strong>FHA</strong></h5>
									<ol>
										<li>Steady employment over the past 2 years</li>
										<li>Valid SSN</li>
										<li>Must be for primary residence</li>
										<li>Loan must be from an approved FHA lender</li>
										<li>Have to pay mortgage insurance premiums</li>
										<li>Allows lower credit, minimum of 500</li>
										<li>Low down payment with a minimum of 3.5%</li>
									</ol> 
								</div>
								<div class="col-md-3 va">
									<h5 align="center"><strong>VA</strong></h5>
									<ol>
										<li>Served 90 consecutive days of active service during wartime</li>
										<li>Served 181 days of active service during peacetime</li>
										<li>Have more than 6 years of service in the National Guard or Reserves</li>
										<li>You are the spouse of a service member who has died in the line of duty</li>
										<li>Must maintain a certain amount of income left over each month</li>
										<li>Satisfactory credit of around 600</li>
									</ol> 
								</div>
								<div class="col-md-3 harp">
									<h5 align="center"><strong>HARP</strong></h5>
									<ol>
										<li>Current on your mortgage</li>
										<li>No 30 day+ late payments in last six months</li>
										<li>No more than one in the past 12 months</li>
										<li>Must be for primary residence</li>
										<li>Loan is owned by Freddie Mac or Fannie Mae</li>
										<li>Loan was originated on or before May 31, 2009</li>
										<li>Current loan to value ratio must be greater than 80%</li>
										<li>Credit score requirement varies by lender</li>
									</ol> 
								</div>
							</div>	
						</div>
					</div>
				<div>			
         	</div>
        </div>
      </div>
    </div>
</section>

 <section id="mu-skills">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-skills-area">
							<!-- Title -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-title">
										<h2>OTHER POPULAR TOOLS</h2>
									</div>
								</div>
							</div>
							<!-- Start Skills Content -->
							<div class="row">
							<div class="col-md-12">
								<div class="mu-skills-content">
									<div class="row">
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a href="{{url('/mortgagecalculator')}}" target="_blank"><img src="{{asset('img/icons/calculator.svg')}}" alt=""></a>
												</div>
												<h3><a href="{{url('/mortgagecalculator')}}" target="_blank">MORTGAGE CALCULATOR</a></h3>
												<p>Compute your monthly payment, interest, APR, and overall interest paid with our easy and fixed-rate mortgage calculator.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a  href="{{url('/refinancecalculator')}}" target="_blank"><img src="{{asset('img/icons/money.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/refinancecalculator')}}" target="_blank">REFINANCE CALCULATOR</a></h3>
												<p>Use our Refinance Calculator to determine different monetary opportunities that might help you reduce your monthly payment or spend less.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a href="{{url('/mortgagecomparisontool')}}" target="_blank"><img src="{{asset('img/icons/comparision.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/mortgagecomparisontool')}}" target="_blank">MORTGAGE COMPARISON</a></h3>
												<p>Select distinct mortgages and see all the various advantages that every mortgage supplies in a single easy-to-read infographic.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a  href="{{url('/mortgageanalysis')}}" target="_blank"><img src="{{asset('img/icons/analysis.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/mortgageanalysis')}}" target="_blank">MORTGAGE ANALYSIS</a></h3>
												<p>By the supplied advice, we examine your existing mortgage and let you know if it's either helping or hurting you.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<!-- End Skills Content -->
						</div>
					</div>
				</div>
			</div>
		</section>

<section id="mu-bills">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-bills-area">
						<!-- Title -->
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
									<h1>More About the Different Types of Home Loan</h1>
                                    
								</div>
							</div>
						</div>
						<!-- Start Feature Content -->
						<div class="row">
							<div class="col-md-6">
								<div class="mu-bills-left">
                            
                                <h4 align="left">30 Year Fixed</h4>
								<p align="justify">The 30-year fixed mortgage is the most common mortgage and is the one that almost 90% of Americans have. The main benefits that a 30 year mortgage offers is the low monthly payments and the fixed interest rate. If you want to lower your current mortgage payment, you can also refinance into a 30-year fixed. By refinancing into a 30 year fixed at today’s still historically low rates you get to lock in a low rate, and low monthly payment that will never rise. Refinancing to a 30 Year Fixed Rate mortgage at today’s rates could reduce your monthly mortgage payment.
								</p>

								<h4 align="left">5/1 ARM</h4>
								<p align="justify">If you’re only planning on living in your house for the next 5 years, there’s no reason to pay the higher rates to get a 30 year fixed if you aren’t going to be there for the duration of the loan. Rates are lower on 5 Year ARMs than longer term fixed rate mortgages. Because the rates are lower, this means you get to have 5 years with lower payments. After these 5 years, your rate would adjust once a year and your payment could go up or down, but if saving money for the next 5 years is important to you, a 5 Year ARM could be a great option. You’re probably better off getting a 5-Year ARM at a lower rate, and by the time the loan would adjust you won’t be carrying the loan anyways so it won’t matter.
								</p>

								<h4 align="left">5/1 ARM</h4>
								<p align="justify">In case you are just planning on living in your home for another five decades, there is no reason to cover the higher prices for a 30 year fixed if you're not planning to be there for the duration of your loan.  Prices are reduced on 5 Year ARMs than longer term fixed rate mortgages.  Since the prices are reduced, this usually means that you get to get 5 years together with lower payments.  Following these 5 decades, your rate would adjust annually along with your payment could move down or up, but when saving money for another five years is significant to you, a 5 Year ARM might be a terrific alternative.  You are likely better off getting a 5-Year ARM in a lower speed, and at that time the loan will adapt you will not be taking the loan so it will not matter.  
								</p>
								<p align="justify">In the event you're only planning on living in your house for another five years, there's absolutely no reason to pay the higher costs for a 30 year fixed if you are not likely to be there for the length of your loan.   Rates are decreased on 5 Year ARMs than longer term fixed rate mortgages.   Considering that the rates are not reduced, this typically implies you get to receive 5 decades with reduced payments.   Observing these 5 years, your rate would adjust annually with your payment may move up or down, but if saving cash for the following five years is important to youpersonally, a 5 Year ARM may be a terrific option.   You're probably better off obtaining a 5-Year ARM at a decrease rate, and in the period the loan will accommodate you won't be accepting the loan so that it won't matter.    
								</p>

								<h4 align="left">Veteran Loans</h4>
								<p align="justify">Should you or your partner have served in the army, we'd love to thank you.  Reduced mortgage rates are offered for service members and their spouses since the Department of Veteran Affairs backs the loans which are created for army personnel, which makes it possible for lenders to offer far greater rates.  The prices on a VA 30 Year Fixed vs a Conventional 30 Year fixed are reduced, meaning your monthly payments are reduced.  							</p>
								<p align="justify">If you wanted even lower monthly payments you could take advantage of the VA 5/1 ARM. With a VA 5/1 ARM your monthly payment would be lower each month for the 5 years you were in the ARM. After those 5 years, the interest rate would adjust and your payments could go up or down, but during the 5 years you could build up your savings. Rates are lower on VA 5/1 ARMs than longer term fixed rate mortgages. This means you get to have 5 years with lower payments. After these 5 years your rate and thus your payment could go up or down, but if saving money for the next 5 years is important to you, a VA 5/1 ARM could be a great option.
								</p>
								<p align="justify">In case you are just planning on living in your home for another five decades, there is no reason to cover the higher prices for a 30 year fixed if you're not planning to be there for the duration of your loan.  You are probably better off obtaining a VA 5/1 ARM in a lower speed, and from the time that the loan will adapt, you will have sold your home and paid back the remaining balance of this loan and transferred elsewhere.  
								</p>
								<p align="justify">You might also select a VA 5/1 ARM in case you really feel as though your earning capacity increases in five decades.  You could make the most of the reduced payments throughout the 5 decades and when the rate adjusts after 5 decades, you'll be making enough to pay for the payment.  You might also refinance into another mortgage prior to the modification interval, if you would like to keep on paying a reduced monthly payment.  
								</p>
								<p align="justify">While many want a low monthly payment, there are different priorities for different people. Some people want to save money, take cash out of their home, buy a home for less than 20% down, or refinance because they fell victim to the housing crisis and are underwater on their mortgages.
								</p>

								<h4 align="left">HARP (Home Affordable Refinance Program)</h4>
								<p align="justify">HARP (property cost-effective re finance plan) can be really a government system targeted toward supporting homeowners enjoy those that have minor if any equity be eligible for re finance. Its principal goal is always to assist homeowners that are sufferers of this home catastrophe and also are adhered using a unjust loan because of inadequate timing and terrible fortune. That really is quite debatable due to the fact homeowners have been submerged in their mortgage, so which means lots of homeowners invest far more compared to residence is well worth and all of equity they'd assembled is now gone. Homeowners using HARP lessen their month-to-month obligations, invest in to shorter-term financial loans they are able to repay their domiciles more rapidly, or move outside of ARMs and to safer, more adjusted fee home loans. HARP has given loan aid to tens of thousands of Americans simply helping re finance their own loans.
								</p>
								<p align="justify">If you should be interested in buying a house, be aware you might have several choices and perhaps not simply one single. No matter your purpose is, there's something which is able to allow you to attain this objective. If you should be now in a home finance loan then and comprehended there surely is a mortgage loan that's just a superior suit for you personally, see whether you may re finance inside that home finance loan and begin living a high standard of living. Mortgages are sometimes a significant issue for a lot of nevertheless they also usually do not need to become always a stress in your own financing.
								</p>

									
								</div>
							</div>
							<div class="col-md-6">
								<div class="mu-bills-left">
									
                                <h4 align="left">15-Year Fixed</h4>
								<p align="justify">In case your primary purpose is really to spend less in your own mortgage afterward the 15-year mended could be the optimal/optimally choice. Having a 15-year adjusted, the month-to-month payments are somewhat marginally higher compared to this of the 30-year adjusted as you possess half of an opportunity for you to pay off the financial loan, nevertheless, you obtain yourself a decrease speed along using a reduce speed across the whole period of 1-5 decades, the personal savings are so large. Perhaps not only are you going to might have saved plenty of income around the total value of the loan, but nevertheless, you're going to also provide paid in full your property solely in mere 1-5 decades, that might accelerate the travel into retirement as you may not have month-to-month cost.
								</p>
								<p align="justify">For some property owners, it isn't preferable to carry on needing to earn mortgage repayments from retirement for just two different reasons. To begin with, the sum you get per calendar month will probably be reduced whenever you are retired, meaning that your mortgage may function as much greater percentage of one's entire money. Secondly, as you have worked hard and also you'd rather get that funds readily available to accomplish all of the enjoyable stuff that you wish to complete at retirement. For paying out only a tad bit more per month, then you have to cover off the main strategy faster so consuming to 1-5 decades of obligations also substantially minimizing the quantity of attention you wind up paying out off your own lender.
								</p>

								<h4 align="left">Cash Out Refinance</h4>
								<p align="justify">A Money Out Refinance can be really a amazing approach to tap in the ownership of your house. Fundamentally you will re finance to get a period over your overall mortgage equilibrium and also return into pocket which gap in funds. It's possible for you to take advantage of this cash for such a thing you desire. Fantastic utilizes for income outside mortgages would be to pay for off higher rate of interest debt such as credit cards and student education loans and improve your credit score history, or even to make use of the additional cash for dwelling renovations to improve the significance of one's house.
								</p>
								<p align="justify">In a place at which equity is rising, this may possibly be a excellent approach to tap in the riches that your dwelling is really helping build. For those who have personal debt you simply are paying attention, it almost certainly is sensible to invest out some dough away from one's home and cover down this credit card debt. There is not any motive to maintain paying a astronomical rate of interest on charge card debt or university student education loans once you are able to make use of a Money Out as being a amazing remedy to expel debt.
								</p>
								<p align="justify">If matters really are a bit tight per month, this really could be a good approach to have a little additional cash onhand. Possessing equity in your home is vital, however also you understand that your own life situation exactly the ideal. Some times rather than equity you prefer to get that funds as money hands to aid cover a few costs, do house upgrades or repairs, and also invest on your own.

  
								</p>
								
								
								<h4 align="left">FHA (Federal Housing Administration) Loans</h4>
								<p align="justify">In the event you wish to get a house, but do not fairly have 20 percent to put like a down payment, then FHA loans are a amazing selection and are supporting individuals become homeowners since 1934. FHA loans are excellent since they don't really take a sizable advance payment they also will have lesser closure outlays, plus they've got stricter qualifying fico ratings. To get the FHA mortgage, your advance payment is frequently only 3.5 percent! As the debtor is currently putting greater than 20 percent of their house value because of being a down payment, then they have been also necessary to buy house loan to defend the lending company in the event the borrower defaults on the financial loan.
								</p>
								<p align="justify">The other reasons folks buy ARMs is they also predict they possess greater monetary flexibility in five decades. In the event you believe that'll possess greater financial liberty 5 decades from today, it may sound right to benefit from this lesser monthly obligations per 5 yr ARM may sell today, after which re finance in to a predetermined home finance loan or utilize the greater making possibility to pay for the greater monthly obligations.
								</p>
								</div>
							</div>
						</div>
						<!-- End Feature Content -->
					</div>
				</div>
			</div>
		</div>
	</section>
    <!-- End Bills -->

@endsection