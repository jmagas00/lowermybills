@extends('layout.app')

@section('main-content')
<section id="mu-bills">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-bills-area">
						<!-- Title -->
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
									<!-- <h1>Mortgage Analysis</h1>
                    <h5 align="center">We Boost Your Present Mortgage Against Your Financial Goals</h5> -->
                    <h1>Mortgage Calculator</h1>
                    <h5>Find the Ideal Mortgage For You</h5>
								</div>
							</div>
						</div>
						<!-- Start Feature Content -->
						<div class="row">
							<div class="col-md-6">
								<div class="mu-bills-right">
                  <p align="justify">Compute your monthly payment, rate of interest, APR, and complete interest with our mortgage calculator.
                  </p>
                  <h4 align="left">How it Works</h4>
                  <ol>
										<li>Input your loan amount so that we understand how much you want for the mortgage.</li>
                    <li>Input your zip code and current credit standing so we can recover the hottest Rates and APR.</li>
                    <li>According to those, we will provide you with different mortgage options, the monthly obligations of each, and also the entire price of this loan.</li>
                    <li>We will also supply pros and cons for each mortgage type that will assist you choose which is most appropriate for your circumstances.</li>
									</ol>
								</div>
							</div>
							<div class="col-md-6">
								<div class="mu-bills-left">
									<img class="" src="{{asset('img/pexels-photo-990818.jpeg')}}" alt="img')}}">
								</div>
							</div>
						</div>
						<!-- End Feature Content -->
					</div>
				</div>
			</div>
		</div>
	</section>
  <section id="para-center">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="para-center-area">
							<div class="row">
								<div class="col-md-12">
                  <form id="msform" name=mortgagecalc method=POST class="form-group">
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="mcselect" style="color:#fff;">Loan Amount</label>
                          <select onkeypress="return validNumber(event)" type="text" class="form-control mcselect" name="mcselect" onChange="mcvalidate()">                        
                            <option value="" disabled="disabled" selected>Please Home Value</option>
														<option value="1">$1-$5,000</option>
														<option value="5001">$5,001-$10,000</option>
														<option value="10001">$10,001-$15,000</option>
														<option value="15001">$15,001-$20,000</option>
                          </select>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="mcselect1" style="color:#fff;">Credit Profile</label>
                          <select onkeypress="return validNumber(event)" type="text" class="form-control mcselect1" name="mcselect1" onChange="mcvalidate()" >
                            <option value="" disabled="disabled" selected>Please Enter Credit Profile</option>
                            <option value="1">Excellent (720 or above)</option>
                            <option value="2">Good (620-719)</option>
                            <option value="3">Fair (580-619)</option>
                            <option value="4">Poor (529 or lower)</option>
                          </select>
                      </div>
                      <div class="form-group col-md-2">
                        <label for="inputZip" style="color:#fff;">Zip Code</label><br>
                        <input type="text" onkeydown="mcvalidate()" class="mczip" name="mczip">
                      </div>
                    </div>
                    <input type="button" name="submit" class="submit btn btn-lg btn-success col-md-4" onClick="$('.details').show();" value="Calculate"><br>
                    <!-- <input name="next" type="button" class="btn btn-success active" onClick="$('.details').show();" value="Calculate"> -->
                  </form>
                  @include('layout.partials.mortgage-calc')
                </div>
						  </div>
				    </div>
				  </div>
			  </div>
	    </div>
	</section>
		<!-- End Newsletter -->

  <!-- Start Newsletter -->
  <section id="mu-newsletter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mu-newsletter-area" style="overflow: auto;"> 
                <table class="table table-bordered table-responsive-xl details mu-simplefilter table-striped" style="background-color: white !important; color: black; display: none;">
                  <thead>
                    <tr>
                      <th class="mu-simplefilter table-success">LOAN TYPE</th>
                      <th class="mu-simplefilter">30 YR FIXED</th>
                      <th class="mu-simplefilter">15 YR FIXED</th>
                      <th class="mu-simplefilter">5/1 YR ARM</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                    <th class="mu-simplefilter table-success">MONTHLY PAYMENT</th>
                      <td>M$1,054</td>
                      <td>$1,534</td>
                      <td>$1,009</td>
                    </tr>
                    <tr>
                    <th class="mu-simplefilter table-success">APR<br>(RATE)</th>
                      <td>5.09%<br>(4.88%)</td>
                      <td>4.67%<br>(4.25%)</td>
                      <td>4.95%<br>(4.50%)</td>
                    </tr>
                    <tr>
                    <th class="mu-simplefilter table-success">TOTAL INTEREST</th>
                      <td>$174,440</td>
                      <td>$71,120</td>
                      <td>???</td>
                    </tr>
                    <tr>
                    <th class="mu-simplefilter table-success">TERM</th>
                      <td>30 Years</td>
                      <td>15 Years</td>
                      <td>30 Years</td>
                    </tr>
                    <tr>
                    <th class="mu-simplefilter table-success">LOWER PAYMENT</th>
                      <td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
                      <td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
                      <td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
                    </tr>
                    <tr>
                    <th class="mu-simplefilter table-success">LOWER RATE</th>
                    <td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
                    <td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>

                      <td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
                    </tr>
                    <tr>
                    <th class="mu-simplefilter table-success">PAY OFF FASTER</th>
                      <td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
                      <td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
                      <td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
                      
                    </tr>
                    <tr>
                    <th class="mu-simplefilter table-success">PAY LESS INTEREST</th>
                      <td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
                      <td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
                      <td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
                      
                    </tr>
                    <tr>
                    <th class="mu-simplefilter table-success"></th>
                      <td><a href="{{url('/read-and-learn/30-year-fixed-rate-mortgage')}}" target="_blank" class="btn btn-success btn-md active" role="button" aria-pressed="true">More Info</a></td>
                      <td><a href="{{url('/read-and-learn/15-year-fixed-rate-mortgage')}}" target="_blank" class="btn btn-success btn-md active" role="button" aria-pressed="true">More Info</a></td>
                      <td><a href="{{url('/read-and-learn/fixed-vs-adjustable')}}" target="_blank" class="btn btn-success btn-md active" role="button" aria-pressed="true">More Info</a></td>
                      
                    </tr>
                  </tbody>
                </table>     
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Newsletter -->

    
  <!-- Start Services -->
  <section id="mu-skills">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-skills-area">
							<!-- Title -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-title">
										<h2>OTHER POPULAR TOOLS</h2>
									</div>
								</div>
							</div>
							<!-- Start Skills Content -->
							<div class="row">
              <div class="col-md-12">
                <div class="mu-skills-content">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="mu-single-skills">
                        <div class="mu-skills-circle">
                          <a href="{{url('/mortgagecalculator')}}" target="_blank"><img src="{{asset('img/icons/calculator.svg')}}" alt=""></a>
                        </div>
                        <h3><a href="{{url('/mortgagecalculator')}}" target="_blank">MORTGAGE CALCULATOR</a></h3>
                        <p>Compute your monthly payment, interest, APR, and overall interest paid with our easy and fixed-rate mortgage calculator.</p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="mu-single-skills">
                        <div class="mu-skills-circle">
                          <a  href="{{url('/refinancecalculator')}}" target="_blank"><img src="{{asset('img/icons/money.svg')}}" alt=""></a>
                        </div>
                        <h3><a  href="{{url('/refinancecalculator')}}">REFINANCE CALCULATOR</a></h3>
                        <p>Use our Refinance Calculator to determine different monetary opportunities that might help you reduce your monthly payment or spend less.</p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="mu-single-skills">
                        <div class="mu-skills-circle">
                          <a href="{{url('/mortgagecomparisontool')}}" target="_blank"><img src="{{asset('img/icons/comparision.svg')}}" alt=""></a>
                        </div>
                        <h3><a  href="{{url('/mortgagecomparisontool')}}" target="_blank">MORTGAGE COMPARISON</a></h3>
                        <p>Select distinct mortgages and see all the various advantages that every mortgage supplies in a single easy-to-read infographic.</p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="mu-single-skills">
                        <div class="mu-skills-circle">
                          <a  href="{{url('/mortgageanalysis')}}" target="_blank"><img src="{{asset('img/icons/analysis.svg')}}" alt=""></a>
                        </div>
                        <h3><a  href="{{url('/mortgageanalysis')}}" target="_blank">MORTGAGE ANALYSIS</a></h3>
                        <p>By the supplied advice, we examine your existing mortgage and let you know if it's either helping or hurting you.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
							<!-- End Skills Content -->
						</div>
					</div>
				</div>
			</div>
		</section>
    <!-- End Skills -->

   <!-- Start Team -->
  <section id="mu-team">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-team-area">
							<!-- Title -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-title">
										<h3>Pros and Cons of the Different Loan Types</h3>
									</div>
								</div>
							</div>
							<!-- Start Team Content -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-team-content">
										<div class="row">
											<!-- start single item -->
											<div class="col-md-4">
                        <!-- <div class="mu-title">
                          <h4>15 Year Fixed Rate</h4>
                        </div> -->
												<!-- <div class="mu-single-team">
													<div class="mu-single-team-content">
                            <h3>Pro</h3>
                            <ul>
                              <li style="text-align:left !important;">Reduced interest</li>
                              <li style="text-align:left !important;">Reduced amount to Repay</li>
                              <li style="text-align:left !important;">House paid off in Just 15 years</li>
                            </ul>
                            <br />
                            <h3>Con</h3>           
                            <ul>
                              <li style="text-align:left !important;">Higher monthly payment</li>
                            </ul>
													</div>
                        </div> -->
                        <div class="card border-info">
                          <h5 class="card-header">15 Year Fixed Rate</h5>
                          <div class="card-body">
                            <h4>Pro</h4>
                            <ul>
                              <li style="text-align:left !important;">Reduced interest</li>
                              <li style="text-align:left !important;">Reduced amount to Repay</li>
                              <li style="text-align:left !important;">House paid off in Just 15 years</li>
                            </ul>
                            <br />
                            <h4>Con</h4>
                            <ul>
                              <li style="text-align:left !important;">Higher monthly payment</li>
                            </ul>
                          </div>
                        </div>
											</div>
											<!-- End single item -->
											<!-- start single item -->
											<div class="col-md-4">
                        <!-- <div class="mu-title">
                        <h4>30 Year Fixed Rate</h4>
                        </div> -->
												<!-- <div class="mu-single-team">
													<div class="mu-single-team-content">
                          <h3>Pro</h3>               
                            <ul>
                              <li style="text-align:left !important;">Reduced monthly payment</li>
                            </ul>
                            <br /><br />
                            <h3>Con</h3>
                            <ul>
                              <li style="text-align:left !important;">Higher interest</li>
                              <li style="text-align:left !important;">In debt to Get 15 more years</li>
                              <li style="text-align:left !important;">Greater amount to Repay</li>
                            </ul>
													</div>
												</div>
                      </div> -->
                      <div class="card border-info">
                          <h5 class="card-header">30 Year Fixed Rate</h5>
                          <div class="card-body">
                            <h4>Pro</h4>
                            <ul>
                              <li style="text-align:left !important;">Lower monthly payment</li>
                            </ul>
                            <br />
                            <h4>Con</h4>
                            <ul>
                              <li style="text-align:left !important;">Higher interest rate</li>
                              <li style="text-align:left !important;">In debt for 15 more years</li>
                              <li style="text-align:left !important;">Higher amount to pay back</li>
                            </ul>
                          </div>
                        </div>
											</div>
											<!-- End single item -->
											<!-- start single item -->
											<div class="col-md-4">
                        <!-- <div class="mu-title">
                        <h4>5 Year Adjustable Rate</h4>
                        </div> -->
                        <div class="card border-info">
                          <h5 class="card-header">5 Year Fixed Rate</h5>
                          <div class="card-body">
                            <h4>Pro</h4>
                            <ul>
                              <li style="text-align:left !important;">Lower monthly payment for the first five years</li>
                              <li style="text-align:left !important;">Perfect if your living situation isn’t permanent</li>
                            </ul>
                            <br />
                            <h4>Con</h4>
                            <ul>
                              <li style="text-align:left !important;">Monthly payments after first 5 years will be unpredictable (no fixed interest rate)</li>
                            </ul>
                          </div>
                        </div>
											</div>
												<!-- <div class="mu-single-team">
													<div class="mu-single-team-content">
                          <h3>Pro</h3>
                            <ul>
                              <li style="text-align:left !important;">Reduced monthly payment for Your first five Decades</li>
                              <li style="text-align:left !important;">Perfect if your living situation is Not permanent</li>
                            </ul>
                            <br />
                            <h3>Con</h3>                 
                          
                            <ul>
                              <li style="text-align:left !important;">Monthly Obligations Following 5 years will be Inconsistent (no Given interest rate)</li>
                            </ul>
													</div>
												</div> -->
											</div>
											<!-- End single item -->
										</div>
									</div>
								</div>
							</div>
							<!-- End Team Content -->
						</div>
					</div>
				</div>
      </div>
</section>

    <section id="faq">
			<div class="container-fluid">
				<h2 style="text-align:center;">Mortgage Calculator FAQ’s</h2>
				<div id="accordion">
					<div class="card">
						<div class="card-header">
							<a class="card-link" data-toggle="collapse" href="#collapseOne">
								SHow much can I borrow? How much am I eligible for?
							</a>
						</div>
						<div id="collapseOne" class="collapse show" data-parent="#accordion">
							<div class="card-body">
								<p>How much you can borrow/are eligible for is dependent on several factors such as your credit score, your DTI ratio, and your income. These three components show the lender how well you manage your debt, how much debt you have, and if you can afford to make your mortgage payments</p>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header">
							<a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
              How much interest will I pay?
							</a>
						</div>
						<div id="collapseTwo" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<p>The amount of time and money you save will depend on the type of mortgage you choose to refinance into. If you are refinancing into a shorter term mortgage such as a 15-Year Fixed Interest Rate Mortgage from a 30-Year Fixed Interest Rate Mortgage, you will save many years and much more money in interest.</p>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header">
							<a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
							  What is PMI and why do I need it?
							</a>
						</div>
						<div id="collapseThree" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<p>PMI stands for Private Mortgage Insurance and is required for homeowners who put down less than 20% as a down payment. This protects the lender in case the home goes into foreclosure. PMI is not a permanent addition to your mortgage and you may ask your lender to cancel your PMI once the loan balance reaches 80% of the home’s original value.</p>
							</div>
						</div>

					<div class="card">
						<div class="card-header">
							<a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
							How much would my mortgage payment be?
							</a>
						</div>
						<div id="collapseFour" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<p>Your mortgage payment would be dependent on the type of you loan you choose. Different terms will yield different payment amounts because there are more or less payments to make. Different mortgages also provide different interest rates which would directly influence your mortgage payment amount.</p>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header">
							<a class="collapsed card-link" data-toggle="collapse" href="#collapseFive">
							  How do you calculate your mortgage payment?
							</a>
						</div>
						<div id="collapseFive" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<p>A mortgage payment consists of 4 parts: Principal, Interest, Property Taxes, and Homeowner’s Insurance. The principal is the amount borrowed, interest is the “fee” you pay to use the borrowed amount, property taxes go to your local government, and homeowner’s insurance is required to protect your home.</p>
								<p>Your mortgage payment technically is only comprised of your interest and principal, but if you decide to open an escrow account, your lender will calculate how much your property taxes and homeowner’s insurance will cost and charge you per month so that it’s taken care of. Homeowners insurance and property taxes are paid twice a year so many homeowners have a tendency to forget. With an escrow account, your lender will make sure that you’re saving for these every month so that when it’s time to pay, you won’t be shocked by the large amount.</p>
							</div>
						</div>
					</div>
				</div>		
			</div>
    </section>
    <br /><br />


@endsection