@extends('layout.app')

@section('main-content')

<section id="mu-bills">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="">
						<!-- Title -->
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
                                <br><br><br>
                        
									<h1>Licenses and Disclosures</h1>
								
                                    
								</div>
							</div>
						</div>
						<!-- Start Feature Content -->
						<div class="row">
							<div class="col-md-12">
								<div class="">
                                <p align="justify">PaymentFixer.com is not a lender or a broker. PaymentFixer.com performs automatic and marketing loan matching services. The information given by you to PaymentFixer.com isn't an application for a mortgage loan, nor can it be utilized to notify you with any broker or lender. If you're contacted by a lender or agent that advertises with PaymentFixer.com and you also submit a loan program to such lender or broker, your information remains between you and that lender or broker. We do not accept applications or collect other files from potential borrowers, make loans accept or ease loan payments. As a promotion lead generator, PaymentFixer.com is needed to become a licensed mortgage broker.
                                </p>
								<p align="justify">PaymentFixer.com does not endorse, warrant or guarantee service or products of any lender or broker and doesn't guarantee and makes no representations of any rates, points and loan programs posted by advertisements lenders and brokers. All information is subject to change without notice. This is not an advertisement for credit as defined by paragraph 226.24 of regulation Z. PaymentFixer.com won't be liable or responsible for any products, services, information or other materials displayed, purchased, or acquired as a consequence of any information or offer in or results of any kind obtained in connection with this web site, including, without limitation, any agent referrals, loan recommendations, application, acceptance, pre-qualification, loan or interest rate analysis.
                                </p>
								<p align="justify">In no event shall PaymentFixer.com be liable to anyone for any delays, inaccuracies, errors or omissions with respect to the information or the transmission or delivery of all or any part thereof, for any damage arising therefrom or occasioned thereby, or for the results obtained from using the info. The entire risk as to the accuracy, adequacy, completeness, timeliness, validity and quality of any information is with the user.
                                </p>  <br><br> <br><br><br> <br><br><br>
                            </div>
                        </div>        
                                
                       <br><br><br>
						<!-- End Feature Content -->
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection