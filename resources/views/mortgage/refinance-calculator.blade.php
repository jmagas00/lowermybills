@extends('layout.app')

@section('main-content')
	<section id="mu-bills">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-bills-area">
							<!-- Title -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-title">
										<!-- <h1>Mortgage Analysis</h1>
											<h5 align="center">We Boost Your Present Mortgage Against Your Financial Goals</h5> -->
											<h1>Refinance Calculator</h1>
											<h3>Locate the Ideal Refinance Choice</h3>
									</div>
								</div>
							</div>
							<!-- Start Feature Content -->
							<div class="row">
								<div class="col-md-6">
									<div class="mu-bills-right">
										<p align="justify">Compute your new monthly payment, rate of interest, and possible savings across a lot of different loan types.
										</p>
										<h4 align="left">How it Works</h4>
										<ol>
											<li>Input your current loan amount, home value, and Many Years left in your current mortgage</li>
											<li>Next, Input your current Speed so we can Predict what Kinds of refinance loans make sense</li>
											<li>According to your inputs, We'll Reveal if and how much you Can save by refinancing your current loan</li>
										</ol>
									</div>
								</div>
								<div class="col-md-6">
									<div class="mu-bills-left">
										<img class="" src="{{asset('img/adult-3327336_960_720.jpg')}}" alt="img')}}">
									</div>
								</div>
							</div>
							<!-- End Feature Content -->
						</div>
					</div>
				</div>
			</div>
		</section>

    <section id="para-center">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="para-center-area">
						<div class="row">
							<div class="col-md-12">
								<form id="msform" name=mortgagecalc method=POST class="form-group">
									<fieldset class="panel-default">
										<span for="inputState" style="color:#fff;">Select Your Current Mortgage Balance</span><br>
										<p> OK to estimate</p>
												<span class="smpc-error" id="loanError"></span>
												<select onkeypress="return validNumber(event)" type="text" class="custom-select select" onChange="validate()" data-width="10%">
														<option value="" disabled="disabled" selected>Please Home Value</option>
														<option value="1">$1-$5,000</option>
														<option value="5001">$5,001-$10,000</option>
														<option value="10001">$10,001-$15,000</option>
														<option value="15001">$15,001-$20,000</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-sm-3" value="Next"/>
									</fieldset>
									<fieldset  class="panel-default">
										<span for="inputState" style="color:#fff;">Select Your Current Home Value</span><br>
										<p> OK to estimate</p>
												<span class="smpc-error" id="loanError"></span>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" name="loan" onChange="validate()">
														<option value="" disabled="disabled" selected>Please Mortgage Balance</option>
														<option value="1">$1-$5,000</option>
														<option value="5001">$5,001-$10,000</option>
														<option value="10001">$10,001-$15,000</option>
														<option value="15001">$15,001-$20,000</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-3" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
									</fieldset>
									<fieldset class="panel-default">
										<span for="inputState" style="color:#fff;">Select Your Credit Profile:</span><br>
										<p> OK to estimate</p>
												<span class="smpc-error" id="loanError"></span>
														<select name="" id="" type="text" class="custom-select selectpicker select" onChange="validate()">
														<option value="" disabled="disabled" selected>Select Property Use</option>
														<option value="1">Primary Residence</option>
														<option value="2">Secondary Home</option>
														<option value="3">Investment Property</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-3" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
									</fieldset>
									<fieldset  class="panel-default">
										<span for="inputState" style="color:#fff;">Select Your Current Interest Rate</span><br>
										<p> OK to estimate</p>
												<span class="smpc-error" id="rateError"></span>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" name="rate" onChange="validate()"> 
														<option value="" disabled="disabled" selected>Select Rate</option>
														<option value="2.00">2.00%</option>
														<option value="2.25">2.25%</option>
														<option value="2.50">2.50%</option>
														<option value="2.75">2.75%</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-3" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
									</fieldset>
									<fieldset  class="panel-default">
										<span for="inputState" style="color:#fff;">Select your current monthly payment:</span><br>
										<p> OK to estimate</p>
												<span class="smpc-error" id="rateError"></span>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" name="rate" onChange="validate()"> 
														<option value="" disabled="disabled" selected>Select Rate</option>
														<option value="2.00">2.00%</option>
														<option value="2.25">2.25%</option>
														<option value="2.50">2.50%</option>
														<option value="2.75">2.75%</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-3" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
									</fieldset>
									<fieldset  class="panel-default">
										<span for="inputState" style="color:#fff;">Enter Your ZIP Code:</span><br>
										<p> OK to estimate</p>
												<span class="smpc-error" id="yearsError"></span>
												<input type="input" name="zip" id="zip" class="zip" onkeypress="validate()"/>
												<!-- <input type=button class="btn btn-lg btn-success col-md-4" onClick="return myPayment()" value=Calculate>   -->
												<!-- <a type="submit" class="btn btn-md btn-success col-md-4" onClick="$('.details').show();">Calculate</a><br> -->
												<input type="button" name="submit" class="submit btn btn-md btn-success col-md-3 js-scroll-trigger" onClick="$('.details').show();" value="Calculate"><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
									</fieldset>			
								</form>
								<p class="smpc-monthlypayment" id="monthlyPayment"> </p>
								@include('layout.partials.mortgage-calc')
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="mu-newsletter">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-newsletter-area" style="overflow: auto;">
						<table class="table table-bordered table-responsive-lg details mu-simplefilter table-striped" style="background-color: white !important; color: black; display: none;">
							<thead>
								<tr>
									<th class="mu-simplefilter table-success">LOAN TYPE</th>
									<th class="mu-simplefilter">30 YR FIXED</th>
									<th class="mu-simplefilter">15 YR FIXED</th>
									<th class="mu-simplefilter">5/1 YR ARM</th>
									<th class="mu-simplefilter">CASH OUT</th>						
								</tr>
							</thead>
							<tbody>
								<tr>
									<th class="mu-simplefilter table-success">NEW MONTHLY PAYMENT</th>
									<td>$810</td>
									<td>$1,178</td>
									<td>$1,009</td>
									<td>$1,666</td>
								</tr>
								<tr>
									<th class="mu-simplefilter table-success">MONTHLY SAVINGS</th>
									<td>$640</td>
									<td>$272</td>
									<td>$675</td>
									<td style="color:red;">-$216</td>
								</tr>
								<tr>
									<th class="mu-simplefilter table-success">INTEREST SAVINGS</th>
									<td style="color:red;">-$77,000</td>
									<td>$2,560</td>
									<td>???</td>
									<td style="color:red;">-$385,160</td>
								</tr>
								<tr>
									<th class="mu-simplefilter table-success">CASH OUT</th>
									<td>$0</td>
									<td>$0</td>
									<td>$0</td>
									<td>$166,500</td>
								</tr>
								<tr>
									<th class="mu-simplefilter table-success">APR<br>(RATE)</th>
									<td>4.63%<br>(4.63%)</td>
									<td>4.13%<br>(4.13%)</td>
									<td>4.74%<br>(4.25%)</td>
									<td>4.63%<br>(4.63%)</td>
								</tr>
								<tr>
									<th class="mu-simplefilter table-success">TERM</th>
									<td><strong>30 Years</strong></td>
									<td><strong>15 Years</strong></td>
									<td><strong>30 Years</strong></td>
									<td><strong>30 Years</strong></td>
								</tr>
								<tr>
									<th class="mu-simplefilter table-success">LOWER PAYMENT</th>
									<td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
									<td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
									<td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
									<td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
								</tr>
								<tr>
									<th class="mu-simplefilter table-success">LOWER RATE</th>
									<td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
									<td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
									<td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
									<td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
								</tr>
								<tr>
									<th class="mu-simplefilter table-success">PAY OFF FASTER</th>
									<td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
									<td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
									<td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
									<td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
								</tr>
								<tr>
									<th class="mu-simplefilter table-success">PAY LESS INTEREST</th>
									<td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
									<td><i class="fa fa-check" aria-hidden="true" style="color:green; font-size: 30px;"></i></td>
									<td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
									<td><i class="fa fa-times" aria-hidden="true" style="font-size: 30px; color:red;"></i></td>
								</tr>
								<tr>
									<th class="mu-simplefilter table-success"></th>
									<td><a href="{{url('/read-and-learn/30-year-fixed-rate-mortgage')}}"  target="_blank" class="btn btn-success btn-md active" role="button" aria-pressed="true">More Info</a></td>
									<td><a href="{{url('/read-and-learn/15-year-fixed-rate-mortgage')}}" target="_blank" class="btn btn-success btn-md active" role="button" aria-pressed="true">More Info</a></td>
									<td><a href="{{url('/read-and-learn/fixed-vs-adjustable')}}" target="_blank" class="btn btn-success btn-md active" role="button" aria-pressed="true">More Info</a></td>
									<td><a href="{{url('/read-and-learn/cash-out')}}" target="_blank" class="btn btn-success btn-md active" role="button" aria-pressed="true">More Info</a></td>
								</tr>
							</tbody>
						</table>					
					</div>
				</div>
			</div>
		</div>
	</section>
		<!-- Start Services -->
		<section id="mu-skills">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-skills-area">
							<!-- Title -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-title">
										<h2>OTHER POPULAR TOOLS</h2>
									</div>
								</div>
							</div>
							<!-- Start Skills Content -->
							<div class="row">
							<div class="col-md-12">
								<div class="mu-skills-content">
									<div class="row">
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a href="{{url('/mortgagecalculator')}}" target="_blank"><img src="{{asset('img/icons/calculator.svg')}}" alt=""></a>
												</div>
												<h3><a href="{{url('/mortgagecalculator')}}" target="_blank">MORTGAGE CALCULATOR</a></h3>
												<p>Compute your monthly payment, interest, APR, and overall interest paid with our easy and fixed-rate mortgage calculator.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a  href="{{url('/refinancecalculator')}}" target="_blank"><img src="{{asset('img/icons/money.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/refinancecalculator')}}" target="_blank">REFINANCE CALCULATOR</a></h3>
												<p>Use our Refinance Calculator to determine different monetary opportunities that might help you reduce your monthly payment or spend less.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a href="{{url('/mortgagecomparisontool')}}" target="_blank"><img src="{{asset('img/icons/comparision.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/mortgagecomparisontool')}}" target="_blank">MORTGAGE COMPARISON</a></h3>
												<p>Select distinct mortgages and see all the various advantages that every mortgage supplies in a single easy-to-read infographic.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a  href="{{url('/mortgageanalysis')}}" target="_blank"><img src="{{asset('img/icons/analysis.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/mortgageanalysis')}}" target="_blank">MORTGAGE ANALYSIS</a></h3>
												<p>By the supplied advice, we examine your existing mortgage and let you know if it's either helping or hurting you.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<!-- End Skills Content -->
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Skills -->
		<section id="mu-team">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-team-area">
							<!-- Title -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-title">
										<h3>Pros and Cons of the Different Loan Types</h3>
									</div>
								</div>
							</div>
							<!-- Start Team Content -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-team-content">
										<div class="row">
											<!-- start single item -->
											<div class="col-md-4">
                        <!-- <div class="mu-title">
                          <h4>15 Year Fixed Rate</h4>
                        </div> -->
												<!-- <div class="mu-single-team">
													<div class="mu-single-team-content">
                            <h3>Pro</h3>
                            <ul>
                              <li style="text-align:left !important;">Reduced interest</li>
                              <li style="text-align:left !important;">Reduced amount to Repay</li>
                              <li style="text-align:left !important;">House paid off in Just 15 years</li>
                            </ul>
                            <br />
                            <h3>Con</h3>           
                            <ul>
                              <li style="text-align:left !important;">Higher monthly payment</li>
                            </ul>
													</div>
                        </div> -->
                        <div class="card border-info">
                          <h5 class="card-header">15 Year Fixed Rate</h5>
                          <div class="card-body">
                            <h4>Pro</h4>
                            <ul>
                              <li style="text-align:left !important;">Lower interest rate</li>
                              <li style="text-align:left !important;">Lower amount to pay back</li>
                              <li style="text-align:left !important;">Home paid off in only 15 years</li>
                            </ul>
                            <br />
                            <h4>Con</h4>
                            <ul>
                              <li style="text-align:left !important;">Higher monthly payment</li>
                            </ul>
                          </div>
                        </div>
											</div>
											<!-- End single item -->
											<!-- start single item -->
											<div class="col-md-4">
                        <!-- <div class="mu-title">
                        <h4>30 Year Fixed Rate</h4>
                        </div> -->
												<!-- <div class="mu-single-team">
													<div class="mu-single-team-content">
                          <h3>Pro</h3>               
                            <ul>
                              <li style="text-align:left !important;">Reduced monthly payment</li>
                            </ul>
                            <br /><br />
                            <h3>Con</h3>
                            <ul>
                              <li style="text-align:left !important;">Higher interest</li>
                              <li style="text-align:left !important;">In debt to Get 15 more years</li>
                              <li style="text-align:left !important;">Greater amount to Repay</li>
                            </ul>
													</div>
												</div>
                      </div> -->
                      <div class="card border-info">
                          <h5 class="card-header">30 Year Fixed Rate</h5>
                          <div class="card-body">
                            <h4>Pro</h4>
                            <ul>
                              <li style="text-align:left !important;">Lower monthly payment</li>
                            </ul>
                            <br />
                            <h4>Con</h4>
                            <ul>
                              <li style="text-align:left !important;">Higher interest rate</li>
                              <li style="text-align:left !important;">In debt for 15 more years</li>
                              <li style="text-align:left !important;">Higher amount to pay back</li>
                            </ul>
                          </div>
                        </div>
											</div>
											<!-- End single item -->
											<!-- start single item -->
											<div class="col-md-4">
                        <!-- <div class="mu-title">
                        <h4>5 Year Adjustable Rate</h4>
                        </div> -->
                        <div class="card border-info">
                          <h5 class="card-header">5 Year Fixed Rate</h5>
                          <div class="card-body">
                            <h4>Pro</h4>
                            <ul>
                              <li style="text-align:left !important;">Lower monthly payment for the first five years</li></li>
                              <li style="text-align:left !important;">Perfect if your living situation isn’t permanent</li>
                            </ul>
                            <br />
                            <h4>Con</h4>
                            <ul>
                              <li style="text-align:left !important;">Monthly payments after first 5 years will be unpredictable (no fixed interest rate)</li>
                            </ul>
                          </div>
                        </div>
											</div>
												<!-- <div class="mu-single-team">
													<div class="mu-single-team-content">
                          <h3>Pro</h3>
                            <ul>
                              <li style="text-align:left !important;">Reduced monthly payment for Your first five Decades</li>
                              <li style="text-align:left !important;">Perfect if your living situation is Not permanent</li>
                            </ul>
                            <br />
                            <h3>Con</h3>                 
                          
                            <ul>
                              <li style="text-align:left !important;">Monthly Obligations Following 5 years will be Inconsistent (no Given interest rate)</li>
                            </ul>
													</div>
												</div> -->
											</div>
											<!-- End single item -->
										</div>
									</div>
								</div>
							</div>
							<!-- End Team Content -->
						</div>
					</div>
				</div>
      </div>
</section>


			<br />

		<section id="faq">
			<div class="container-fluid">
				<h2 style="text-align:center;">Refinance Calculator FAQ’s</h2>
				<div id="accordion">
					<div class="card">
						<div class="card-header">
							<a class="card-link" data-toggle="collapse" href="#collapseOne">
								Should I refinance my mortgage?
							</a>
						</div>
						<div id="collapseOne" class="collapse show" data-parent="#accordion">
							<div class="card-body">
								<p>There are many reasons why homeowners refinance their mortgages, but the main reason is that their mortgage was no longer providing them with benefits that coincided with their priorities. Priorities change and qualified homeowners can also change their mortgages to better assist them with their new goals.</p>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header">
							<a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
								How much time and money can I save by refinancing?
							</a>
						</div>
						<div id="collapseTwo" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<p>The amount of time and money you save will depend on the type of mortgage you choose to refinance into. If you are refinancing into a shorter term mortgage such as a 15-Year Fixed Interest Rate Mortgage from a 30-Year Fixed Interest Rate Mortgage, you will save many years and much more money in interest.</p>
								<p>It is important to note that refinancing your mortgage will not change the principal balance of your loan, it will only change the interest rate or length of term.</p>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header">
							<a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
							How much will my new payment be?
							</a>
						</div>
						<div id="collapseThree" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<p>This will depend on the type of loan you choose and how much your interest rate will be. If you choose to refinance into a loan with a shorter term, your new payment amount will be larger because you are choosing to pay more to get out of debt faster. Similarly, if you choose to refinance into a longer term mortgage, your new payment amount will be less.</p>
							</div>
						</div>

					<div class="card">
						<div class="card-header">
							<a class="collapsed card-link" data-toggle="collapse" href="#collapseFour">
							Can I refinance to get rid of PMI?
							</a>
						</div>
						<div id="collapseFour" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<p>Normally, no. PMI is required on all mortgages if less than 20% is put down for a down payment. So long as the loan balance is more than 80% of the original home value, you will be required to pay for PMI. The only exception to this rule is the VA Loan as veterans and active duty military members are not required to get mortgage insurance, no matter how little the down payment is.</p>
							</div>
						</div>
					</div>

					<div class="card">
						<div class="card-header">
							<a class="collapsed card-link" data-toggle="collapse" href="#collapseFive">
							How do you calculate your new mortgage payment and refinance rate?
							</a>
						</div>
						<div id="collapseFive" class="collapse" data-parent="#accordion">
							<div class="card-body">
								<p>Like your previous mortgage payment, your payment will consist of your principal, interest, property taxes, and homeowner’s insurance.  Depending on the mortgage you choose and your new interest rate, your payment amount will either increase or decrease.</p>
								<p>For refinancing, a home appraisal is very important. An appraisal is done by a 3rd party and the appraiser will inspect your home to see if the home’s value has gone up or down. They will look at the value of neighboring homes and check to see if repairs and other features have been added to the home. If the home value has increased since the purchase, you have gained equity. If the value has gone down so low that you are underwater, you won’t be able to refinance.</p>
								<p>Your Loan to Value ratio (LTV) affects the rate and type of loan you may qualify for. It shows what you owe on your mortgage against the home value. Having a low LTV is best because it can get you a better interest rate. If you have a high LTV, your refinance may require PMI.</p>
							</div>
						</div>
					</div>
				</div>		
			</div>
		</section>

			<br /><br />



		
				
@endsection