@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">What To Expect During the Mortgage Process</h1>
                                        
                                        <br>
                                       <p align="justify">Obtaining a mortgage is a major step for anybody. It usually means you'll get a better sense of fiscal responsibility and with a good deal more duty could be daunting. There Are Lots of measures that You Have to take so as to make this transaction go as smoothly as you can:</p>
                                        <ul>
                                            <li>Choosing the best mortgage for you</li>
                                            <li>Closing your mortgage</li>
                                            <li>Own your home</li>
                                        </ul>
                                        <br />
                                       <p align="justify">By educating yourself on those issues, not only can you be ready, but you also won't have any openings and you will have the ability to shut your mortgage quicker.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Selecting the Best Mortgage For You</h1>
                                       <p align="justify">The initial and most significant step in acquiring a mortgage is to decide on the mortgage that suits you the best. You may readily read up on many kinds of mortgages and determine which one seems the best on paper. Occasionally, what's best on paper is not the right for you. You will need to ask to specify the term "cheap", because it means different things for everybody. Can a mortgage payment take priority over your other financial objectives? Is a bigger monthly payment with half of the years in debt or is fiscal breathing space more significant? Ever since your lender just looks to see whether you're able to cover your mortgage obligations, it's completely your responsibility to determine how comfortable you'll be financially.</p>
                                        <img class="" src="{{asset('img/OCRSWY0.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Preparing Your Finances</h1>
                                       <p align="justify">Organizing your financing is a huge portion of the mortgage application process as your creditor will probably be exploring to see just how fiscally ready you are for house ownership. If you're thinking about buying a house with your partner or a spouse, they'll also need to give documentation of the history. Lenders are searching for a solid credit rating to find out whether you can keep on making payments on your mortgage. Obtaining credit history is significant as it shows the creditor that you can make payments in a timely way. In case you don't have any credit history, then you won't have the ability to have financing since supplying a credit history is necessary. A score of 800 (almost perfect) is not required, but a score of 620 is deemed strong. FHA and VA Loans have significantly more lenient qualifying credit scores as they're government insured. If your credit rating may use some progress, keep paying your debts on time for 6 weeks and your score increases. While credit rating is essential for your mortgage, your credit rating is not the deciding factor in whether you get approved for the loan.</p>
                                        
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">IMPORTANT! Debt to Income Ratio</h1>
                                       <p align="justify">The most significant part being approved for a mortgage is the debt to income ratio (DTI). Your debt to income ratio is calculated by adding up all your monthly payments and dividing that by your gross monthly income (your yearly earnings BEFORE taxes are deducted). This reveals the creditor your debt from your earnings. Possessing a DTI ratio as low as possible will be your best, but 43 percent is the maximum ratio that a debtor could have while still having the ability to find financing. This can be a more precise method of showing your creditor if you're able to really make the payments in your mortgage. Someone could maintain a pile of debt, however since they're paying their bills on time that their credit rating is quite high and they seem fantastic on paper. In this kind of scenario, obtaining a high credit rating is deceiving because this individual could appear to be a fantastic candidate, but in fact paying off their current debt will not be simple. If this individual were to receive a mortgage along with their debt, then it might make it even harder for them to pay their debt off and it might look too risky to the lender.</p>
                                        <img class="" src="{{asset('img/accounting-blur-business-1028726.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">WARNING! Mortgage Fraud</h1>
                                       <p align="justify">When searching for your mortgage it's extremely important that you are 100 percent fair on your loan program. Lying in your program to be able to receive approved or to find a bigger loan amount can be regarded as mortgage fraud. It's a serious crime that may lead to fines up to $1 million dollars and 30 years. If your creditor persuades one to lie on your program, this is a significant red flag and you want to discover a new lender when possible. Even if your creditor is advising you to do that, it's still regarded mortgage fraud and because your name is going to be on the mortgage program, you're likely to be the person who's liable. Long story short, regardless of what anybody tells you... don't lie on your mortgage program or boost any replies. The consequences are not worth the short-term outcomes.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What does “In Escrow” Mean?</h1>
                                       <p align="justify">"In escrow" officially means moving a parcel of property, for example personal valuables or property, to another individual using a third party. This merely means that you're presently in the legal portion of getting your house, which might require as few as 3 months and up to many weeks. As soon as you've entered the authorized part of your mortgage you're currently "in escrow." When you've reached this component, you have to choose when you need to close. Think carefully about once you need this to occur and make sure you plan around large trips and significant meetings and vacations. If you are paying money for your house, closing ought to be quicker but if you are refinancing, give yourself a bit more time and breathing space. 30-45 days is the suggested time period if you're funding your residence. Though you would need this component to go by as soon as possible, you'll have to sign and present documents and they need to occur in person. Not consulting with your calendar and picking a final date as you're on holiday or on a business trip may lose you your property. An appraisal happens to guarantee the creditor your buy is well worth the money they'll be lending you. The appraiser assesses to observe that the area and the value of neighboring houses while a house inspection checks into the view the true construction and quality of the house.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Your Rights as a Buyer</h1>
                                       <p align="justify">During the time you're handling the creditor as well as the vendor, you also need to maintain your best interests in mind. You need to be certain that you're not getting ripped off and purchasing a house for far more than it's worth. The appraisal and the house inspection will safeguard you out of this. If through the appraisal along with the house inspection there are a number of red flags, then you've got every right to back out of this deal. The home and the house inspector will let you know in fantastic detail why those are issues, which means you'll have complete transparency if those problems are going to be a long-term issue if not cared for. If you are put on the home, you may even utilize the vendor and come to an arrangement about the best way best to find these problems fixed. As you are currently in escrow and this really is the legal procedure, your arrangement with the vendor is going to be on the listing. Just because you have made it this far to the house buying process doesn't mean that you're bound to go through with it. You do not need to get stuck with a home with faulty gear in a locality where the house value is not increasing every year. In case you've got a valid reason to back out of this purchase, you can without any consequences and you'll also receive your cash back. If you back out of the deal with no legal reason, you'll find a partial refund as you'll need to cover the appraisal and home inspection services along with the services involved until the deciding stage.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Own Your Home</h1>
                                       <p align="justify">In case you decide that you are still set on obtaining your loan, the bank will then get everything for underwriting. The mortgage underwriter is fundamentally the gate keeper for your loan. They'll look over each the files which you have supplied and appraise them. When the underwriter approves your loan, all you've got to do is cover your closing expenses (that were summarized on your GFE), examine the conditions of your loan, and also do a last walk through of the house. During the final meeting that you just scheduled, you are going to need to review a good deal of files and review them carefully prior to signing. After everything is signed, the loan isn't yours!</p>
                                    </div>
                                </article>
                                                                                                                                
                            </div>
                        </div>
                        <div class="col-md-4">
                             @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog -->
@endsection