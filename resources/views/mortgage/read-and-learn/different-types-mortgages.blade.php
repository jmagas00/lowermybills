@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">Different Kinds of Mortgages</h1>
                                       
                                        <br>
                                        <h1 class="mu-blog-item-title">There Is More Than 1 Kind Of Mortgage</h1>
                                       <p align="justify">Were you aware that nearly 90 percent of Americans chose the exact same kind of mortgage? Everybody differs. Most of us have different priorities, different jobs, and unique lifestyles so why does it look like there is just 1 form of mortgage to suit everybody's needs? Regardless of what your financial goals are, there's something out there for everyone.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What’s Your Priority?</h1>
                                       <p align="justify">There's many different different mortgage forms that caters to everybody's different requirements. Whether your focus is to locate a mortgage which has a low monthly payment, saves you money on interest, works with continuous relocation on account of your profession, makes it possible to obtain a house with less than 20 percent down, thanks you for yours or your partner's support in the army, etc., it is. If your priority at a mortgage would be to have a low monthly payment, then There Are Numerous choices:</p>
                                        <ul>
                                            <li>30-year fixed mortgage</li>
                                            <li>5/1 ARM</li>
                                            <li>10/1 ARM</li>
                                            <li>VA Loans (Veterans only)</li>
                                        </ul>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">30-Year Fixed</h1>
                                       <p align="justify">The 30-year fixed mortgage is the most frequent mortgage and will be the one which nearly 90 percent of Americans have. The principal advantages that a 30 year mortgage provides is that the low monthly payments along with the fixed rate of interest. If you would like to decrease your existing mortgage payment, then you may even refinance to some 30-year fixed. By refinancing to a 30 year fixed in today's nonetheless historically lower prices you have to lock in a low speed, and low monthly payment which won't ever rise. Refinancing into a 30 Year Fixed Rate mortgage in the current rates can lower your monthly payment.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">5/1 ARM</h1>
                                       <p align="justify">In case you are just planning on living in your home for another five decades, there is no reason to cover the higher prices for a 30 year fixed if you're not planning to be there for the duration of your loan. Prices are reduced on 5 Year ARMs (Adjustable Rate Mortgages) than longer term fixed rate mortgages. Since the prices are reduced, this usually means that you get to get 5 years together with lower payments. Following these 5 decades, your rate would adjust annually along with your payment could move down or up, but when saving money for another five years is significant to you, a 5 Year ARM might be a terrific alternative. You are likely better off getting a 5-Year ARM in a lower speed, and at that time the loan will adapt you will not be taking the loan anyway so it will not matter.</p>
                                        <img class="" src="{{asset('img/370.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">With a Low Monthly Payment Is Important To Me</h1>
                                       <p align="justify">Still another reason folks buy ARMs is since they predict they will have more fiscal flexibility in five decades. If you feel you are going to have more fiscal freedom 5 decades from today, it may make sense to make the most of the reduced monthly payments per 5 Year ARM can offer currently, then refinance into a fixed mortgage or utilize your higher earning potential to manage the monthly payments.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">10/1 ARM</h1>
                                       <p align="justify">Using a 10/1 ARM, the idea is precisely the same as a 5/1 ARM, but you get to make the most of this reduced monthly payment for another five decades. Provided that you are not planning on living in your home for over 10 decades, there is no reason to cover the higher speed of a 30 year fixed. A 10-Year ARM will lower your mortgage rate during the next 10 years as a result of reduced rate, and so you will not maintain the loan once the adjustment interval comes, that usually means you get to pocket all of the savings. By refinancing to some 10-Year ARM (Adjustable Rate Mortgage) you can lower your mortgage payments for another 10 decades.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">VA Loans</h1>
                                       <p align="justify">Should you or your partner have served in the army, we'd love to thank you. Reduced mortgage rates are offered for service members and their spouses since the Department of Veteran Affairs backs the loans which are created for army personnel, which makes it possible for lenders to offer far greater rates. The prices on a VA 30 Year Fixed vs a Conventional 30 Year fixed are reduced, meaning your monthly payments are reduced.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What If That's a Temporary Living Proof?</h1>
                                       <p align="justify">In case you are just planning on living in your home for another five decades, there is no reason to cover the higher prices for a 30 year fixed if you're not planning to be there for the duration of your loan. You are probably better off obtaining a VA 5/1 ARM in a lower speed, and from the time that the loan will adapt, you will have sold your home and paid back the remaining balance of this loan and transferred elsewhere.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Financial Flexibility for Veterans</h1>
                                       <p align="justify">You might also select a VA 5/1 ARM in case you really feel as though your earning capacity increases in five decades. You could make the most of the reduced payments throughout the 5 decades and when the rate adjusts after 5 decades, you'll be making enough to pay for the payment. You might also refinance into another mortgage prior to the modification interval, if you would like to keep on paying a reduced monthly payment. Even though many need a low monthly payment, there are different priorities for different men and women. Many people today wish to spend less, take cash from the house, purchase a house for less than 20 percent, or refinance since they fell prey to the housing crisis and so are submerged on their own mortgages.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">15-Year Fixed</h1>
                                       <p align="justify">If your prime purpose would be to spend less on your mortgage, then a 15-year mended is the most suitable choice. Having a 15-year fixed, the monthly payments are marginally higher than that of a 30-year adjusted since you've got half of the time to repay the loan, but you receive a lower price and using a lower speed within the length of 15 decades, the savings are considerable. Not only will you have saved a great deal of cash on the general price of your loan, however you'll also have repaid your house entirely in just 15 decades, which might accelerate the travel to retirement as you won't have monthly payment.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Why You Should Pay Off Your Mortgage Sooner Than Later</h1>
                                       <p align="justify">For most homeowners, it is not preferred to keep needing to make mortgage payments in retirement for 2 reasons. To begin with, the amount you make every month will probably be lower when you are retired, meaning that your mortgage might be a greater percentage of your overall income. Secondly, as you have worked hard and you would rather have that money available to perform all of the fun stuff that you would like to do in retirement. For paying a bit more every month, you have to repay your main way quicker so eliminating around 15 decades of obligations and significantly minimizing the quantity of interest you find yourself paying your creditor.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Cash Out Refinance</h1>
                                       <p align="justify">A Cash Out Refinance is a terrific way to tap into the equity in your dwelling. Basically you will refinance for a sum more than your current mortgage balance and also get to pocket which gap in money. You may use this cash for whatever you desire. Fantastic uses for money out mortgages would be to repay high rate of interest debt such as credit cards and student loans and boost your credit score, or even to utilize the excess cash for house renovations to boost the value of your property.</p>
                                        <img class="" src="{{asset('img/632.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Pay Off Your Money</h1>
                                       <p align="justify">Within a environment in which equity is rising, this might be a excellent way to tap into the riches that your house is really helping create. In case you have debt that you are paying high interest, it likely makes sense to take out some money out of your home and repay that debt. There is no reason to keep paying a astronomical rate of interest on credit card debt or student loans when you are able to use a Cash Out Refinance as a fantastic remedy to eliminate debt.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Have Some Fiscal Breathing Room</h1>
                                       <p align="justify">If matters are somewhat tight month, this may be a terrific way to find a little excess money on hand. Having equity in your property is essential, but you understand your life conditions the ideal. Occasionally rather than equity you would rather get that money as money in hand to help pay a few bills, do home repairs or updates, and invest on your own.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">FHA Loans</h1>
                                       <p align="justify">If you would like to buy a house, but do not really have 20 percent to put as a down payment, FHA loans are a fantastic choice and have been helping people become homeowners since 1934. FHA loans are great because they do not take a large down payment they have reduced closing costs, and they've easier qualifying credit ratings. For an FHA mortgage, your deposit can be as low as 3.5%! Since the debtor is putting less than 20 percent of the house value as a deposit, they're also required to buy mortgage to protect the lender in the event the borrower defaults on the loan.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Added Advantages of a FHA Loan: Refinancing</h1>
                                       <p align="justify">In case you are now in an FHA loan, then this is fantastic news. This means that you are able to benefit from this FHA Streamline refinance application. Here is the simplest way to be eligible for refinance since you will not require an assessment, credit rating requirements are reduced (homeowners with scores as low as 580 can qualify), and there is less documentation required. You could even refinance around 97.5 percent of your principal home's worth. 30, 25, 20, and 15 year fixed rate mortgages and a 5-year flexible rate mortgage are available for you.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">HARP</h1>
                                       <p align="justify">HARP (Home Affordable Refinance Program) is a government program aimed at assisting homeowners that have little if any equity to qualify for refinancing. Its principal objective is to aid homeowners who are victims of this housing emergency and are stuck using an unfair mortgage as a result of bad timing and bad fortune. This is quite problematic since homeowners are submerged in their mortgage, so many homeowners invest considerably more than the house is worth and each of the equity that they had formerly assembled is gone. Homeowners using HARP decrease their monthly payments, enter shorter-term loans so they can repay their houses quicker, or get out of ARMs and to safer, more fixed rate mortgages. HARP has supplied mortgage relief to tens of thousands of Americans by assisting them refinance their loans.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Select the Best Alternative for Yourself</h1>
                                       <p align="justify">In case you are searching to buy a house, be aware that you have many choices rather than only one. Whatever your objective is, there's a item which could help you attain this objective. If you are now in a mortgage and recognized there is a mortgage that's a better match for you, see if you may refinance in that mortgage and begin living a high quality of life. Mortgages can be a significant issue for quite a few, but they do not need to become a strain on your finances.</p>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="col-md-4">
                             @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 
@endsection