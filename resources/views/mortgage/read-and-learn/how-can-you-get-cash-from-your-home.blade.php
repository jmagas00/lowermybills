@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">How Can You Get Cash From Your Home?</h1>
                                       
                                        <br>
                                       <p align="justify">A Cash Out Refinance along with a Home Equity Line Of Credit (HELOC) along with a Home Equity Loan are a Few of the ways that homeowners can tap into their equity to pull Money.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What is Equity?</h1>
                                       <p align="justify">Home equity is the part of the house that homeowners really own. Home equity may go down or up based on the value of the house. If your house worth goes up, so you have greater equity. Even when you did not pay that much for your house, your equity has improved because if you should market the house, you would find that excess price. Regrettably, the identical thing applies if house values were to reduce. In the event the value of the property were to return, you've now lost equity even in the event that you've paid off a specific amount of your principal (the amount you borrowed from the creditor without the extra interest and insurance).</p>
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/44jltfNsOKk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Cash Out Refinance</h1>
                                       <p align="justify">Home equity is the part of the house that homeowners really own. Home equity may go down or up based on the value of the house. If your house worth goes up, so you have greater equity. Even when you did not pay that much for your house, your equity has improved because if you should market the house, you would find that excess price. Regrettably, the identical thing applies if house values were to reduce. In the event the value of your property were to return, you've lost equity even in the event that you've paid off a specific amount of your principal (the amount you borrowed from the creditor without the extra interest and insurance).</p>
                                       <p align="justify">You may get this as a lump sum and because this continues to be accounted for on your new mortgage, you'll be paying it back together with your new monthly payments. There's a limitation to just how much you can borrow. For many money out refinances, the limitation is generally 85% of your equity. Cash out refinances generally have a fixed rate of interest, and therefore you will have no surprises. You'll have the exact same monthly mortgage repayment amount every month before the conclusion of the term irrespective of inflation or prices skyrocketing.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Things You Need to Know About a Cash Out Refinance</h1>
                                       <p align="justify">There's a limitation on how much of your property equity you are able to tap into to get a cash payout. As a result of this limitation, and the closing costs that are connected with any new loan or refinance, it's better that you've built up sufficient equity prior to tapping into it for cash. Closing costs are typically 2%-5% of your loan, and that may cost you a few thousand dollars. If you do not have a lot of equity built up in the first place, a cash out refinance likely is not in your very best interests right now.</p>
                                       <p align="justify">In the event that you only have $10,000 in equity, then you're restricted to carrying about $8,500. Based on how much your loan number is, you might be receiving very little if any cash back whatsoever after paying the closing prices for this particular refinance. This is a good alternative when you've been making payments for several years and your house worth has gradually been increasing too.</p>
                                       <p align="justify">Another factor to remember is that because you're refinancing your current mortgage into a different mortgage, even if you should wind up with less than 20% equity in your house, you'll need to pay a FHA Mortgage Insurance Premium (MIP). This is exactly the identical notion as a FHA Loan, which makes it possible for homeowners that do not have 20 percent as a deposit to buy a house. As they're putting down less than 20 percent and also have less than 20% equity in their home, they must buy MIP. MIP, however, isn't permanent and is no longer needed when the loan was paid down to 80 percent or less of the initial value of the house.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Home Equity Line of Credit (HELOC)</h1>
                                       <p align="justify">A Home Equity Line of Credit is quite like a cash out refinance, but also allowing the homeowners tap into their home equity for capital. But, unlike the money out refinance, a HELOC doesn't provide the homeowner a lump sum. A HELOC functions quite similarly to the likes of a charge card, providing you a line of charge as its name says.</p>
                                       <p align="justify">Considering that a HELOC doesn't provide the homeowner a lump sum of money, the homeowner may utilize their home equity via writing checks or despite a designated credit card. An advantage to your HELOC is that the borrower only has to repay the money borrowed. This implies that in case the debtor doesn't draw the maximum the HELOC permits, then the borrower may receive lower monthly payments!</p>
                                       <p align="justify">A HELOC includes a "draw period" meaning there's a time limit to just how long that line of credit is readily available for use. The draw period to get a HELOC is generally 10-15 decades, so there's loads of time to use it if desired. Additionally distinct from a cash out refinance, the interest rate with the majority of HELOCs are flexible that the monthly payments to get a HELOC will not be exactly the same until it's paid.</p>
                                        <img class="" src="{{asset('img/250668-P419K8-497.jpg')}}">    
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Things You Should Know About a Home Equity Line of Credit</h1>
                                       <p align="justify">During the draw period, the borrower is only required to repay the interest. When the draw period is finished, the debtor will finally need to repay the principal and the interest and because the rate of interest is flexible, your payments will most likely change. Additionally, with the majority of creditors there's a minimal to just how much the borrower may withdraw, therefore even if they don't want much in the moment, they must draw more cash than they require.</p>
                                       <p align="justify">Considering that a HELOC is a credit line that acts as a charge card, you may be demanded similar fees generally associated with a charge card along with the final costs of the loan. Examples of them include an yearly fee, membership fee, or transaction fees.</p>
                                       <p align="justify">Using a HELOC, there's also a limitation as to how much equity you may use. It's generally 80%-90%, and it is a bit greater than you can get if you're contemplating a cash out refinance. Additionally, because a HELOC is another loan, you'll also need to address two monthly payments whereas using a cash out refinance, you've refinanced your current loan so that you would just have one monthly payment to manage.</p>  
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Home Equity Loan</h1>
                                       <p align="justify">A Home Equity Loan is the most like Your cash out refinance.</p>
                                       <p align="justify">Just like a cash out refinance, a home equity loan provides the borrower a lump sum to pay any large purchases or cover a fantastic part of any high interest debt that may have been gradually accruing. There's also a limitation to how much equity you may tap to money, but with a home equity loan several creditors will allow you to borrow around 90-95 percent. Just like a cash out refinance, your interest rate and monthly payments are fixed, which means you'll be paying the exact same amount every month, unlike a HELOC.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What's the Difference Between a House Equity Loan and a Cash Out Refinance?</h1>
                                       <p align="justify">A home equity loan and also a cash out refinance will be the most similar goods since they both enable the homeowner to tap into their equity to pull out a lump sum of money. They have fixed interest rates and the monthly payments for the two are fixed. Even though they look like the exact same solution, there are numerous differences.</p>
                                       <p align="justify">A cash out refinance requires the homeowner to refinance their current mortgage. Considering that the new quantity of the refinance includes the main amount which the homeowner has not completed paying, this may influence the closing costs associated with that. Closing prices are generally 2%-5% of their loan and because the sum of the new mortgage is greater than a home equity loan, the closing prices using a cash out refinance will probably be greater. In addition, because a cash out refinance is just refinancing their mortgage for a bigger sum, they can write off the interest. A home equity loan and HELOC are different loans which are taken out along with the homeowner's existing mortgage, therefore closing prices will be more economical for those.</p>
                                       <p align="justify">As a home equity loan is another loan that's taken out along with the homeowner's existing mortgage, the homeowner now has to manage an additional monthly payment every month. This is what's normally called "another mortgage" Having a cash out refinance, the whole loan was refinanced to adapt the equity that's been cashed out, therefore there is just one monthly payment to take care of.</p>
                                    </div>
                                </article>                
                                                                                                
                            </div>
                        </div>
                        <div class="col-md-4">
                             @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog -->
@endsection