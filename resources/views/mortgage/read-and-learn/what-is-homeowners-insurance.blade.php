@extends('layout.app')


@section('main-content')
<main>
<!-- Start Blog -->
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">What is Homeowners Insurance?</h1>
                                      
                               
                                       
                                       <p align="justify">Home insurance can be a vital section of one's property. As soon as it's not compulsory, it's vital to possess dwelling insurance plan to secure your investment decision. Some creditors may demand householders insurance along with some home finance loan to guard their financial commitment. They may likewise necessitate the house owner to start an entry accounts to guarantee the house owner is not overdue on any obligations.</p>
                                        
                                    

                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Exactly What Exactly Does Homeowners Insurance Policies Consist of?</h1>
                                       <p align="justify">Homeowners insurance plan is just what it resembles. It's an insurance policy plan that house owners choose outside to guard their house. When there's a harm to your house along with also your insurance policy plan covers it, then you might ben't accountable for spending of pocket to get all. It's advisable to possess insurance that in the case of an emergency, then you may not need to worry. After all, the restore costs could possibly become very pricey.</p>
                                       <p align="justify">While house owners insurance appears just like it simply addresses damages into a house because of natural disasters, so it addresses more. Based upon your design, many homeowners insurance plan may insure possessions. As an instance, if furnishings has been damaged by an flame, your insurance policy provider may aid with that. Homeowners insurance policies may also insure personal things which traveling together with you personally. If you should be on a break and you also left your high priced opinion at a resort space, insurance policies helps pay for the expense to displace it.</p>
                                       <p align="justify">Homeowners insurance policies may also protect accountability problems which happen at residence. By way of instance, if some one should happen to drop at house and simply take legal actions, house owners insurance may help pay for the authorized expenses and harms. That is not some thing which a lot of homeowners consider, however nevertheless, it might materialize.</p>
                                        
                                        <img class="" src="{{asset('img/NewHome.jpg')}}">
                      
                                    </div>
                                </article>
                               
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Several Varieties of Householders Insurance Plan</h1>
                                       <p align="justify">Agents are usually suggested by your real estate agent or with way of a close friend. You are able to even find a single online that works into your neighborhood area or the area that you want to dwell in. If you should be searching for you personally, you'll find plenty of mortgage broker overviews online that will help you on your hunt.</p>
                                       <p align="justify">Even the HO3 policy can be a "open-peril" coverage, this means it is going to pay many pitfalls. An HO3 policy may insure each the dangers recorded in a HO 2 policy and much also more. It is not only going to pay greater risks therefore there is more policy, however nevertheless, it is also going to cover personalized items in addition to private obligations which happen in your own premises. HO3 policies additionally provide greater financial security.</p>
                                      
                                        
                                      
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Just How Can I Pick the Ideal Insurance Carrier?</h1>
                                       <p align="justify">Distinct insurance businesses provide various prices for his or her coverages, therefore it's critical to investigate and determine what type best meets your priorities. In the event you really don't possess the opportunity to sit down and investigate different businesses and exactly what they insure, tons of internet sites will offer a break down of their most useful householders insurance plan choices.</p>

                                        
                                        <img class="" src="{{asset('img/cropped-property.jpg')}}">
                                      
                                    </div>
                                </article>

                                    <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Exactly What Does Homeowners Insurance Policies Consist of?</h1>
                                       <p align="justify">Even though householders' insurance coverage businesses include policy for a sort of threats, you will find a few who most insurance businesses exclude away from their coverages. Flooding, earthquakes, landslides, mould, phobias, regular tear and wear are a few of the dangers which produce the checklist. For these pitfalls, you're able to get insurance coverages guarding you against those threats independently. You tend to be far more than welcome to speak with your insurance firm concerning risks which are not recorded in your own coverage and determine when adding info could be potential. Endorsements will definitely cost added, nevertheless they may give you greater security.</p>

                                        
                                      
                                    </div>
                                </article>
                              
                                <!-- End single item -->
                            </div>
                        </div>
                        <div class="col-md-4">
                        @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 
@endsection

