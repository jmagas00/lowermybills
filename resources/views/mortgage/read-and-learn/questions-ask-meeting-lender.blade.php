@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">What Questions Should You Ask Your Lender?</h1>
                                       
                                        <br>
                                       <p align="justify">Once it is time to meeting a creditor, it's necessary you have questions ready rather than entering your mortgage process blindly. Collars are complex topic which are comprised of numerous components as much as you possibly attempt to educate yourself about the topic, your lender remains a human being. When they were to inadvertently leave out some vital details, which might cause you a great deal of unnecessary strain if overlooked. To prepare yourself for your mortgage process, we've put together a listing of questions which you ought to ask your creditor.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">1. What Mortgage Can You Recommend For Me Why?</h1>
                                       <p align="justify">Many creditors will directly off the bat urge a 30-year fixed mortgage to the majority of homeowners, but a lot of them do not know that there's even more than 1 mortgage type. There's that the Adjustable Rate Mortgage (ARM), 15-Year Fixed, also a FHA Loan, and also a VA Loan, which matches a large number of character and lifestyle forms. Some mortgages have different qualifying rules, which means that your creditor will have the ability to let you know just what loans you qualify for. Automatically assuming a 30-year fixed mortgage is the best product for you reveals that the creditor is not considering your financial situation and inquiring about your long-term program and consequently doesn't have your best interests in mind.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">2. What Is The Interest Rate?</h1>
                                       <p align="justify">Request for a Complete break from your mortgage.</p>  
                                       <p align="justify">Consult your lender to the rate of interest and don't be scared to ask why this is the rate of interest. Ask if you're going to be receiving a fixed rate or an adjustable speed. Even in case you've got zero comprehension of what type of speed you're supposed to be receiving, by getting your lender explain to you why (you have a great student debt, your credit rating is excellent although not outstanding, etc.) it usually means your lender is getting the opportunity to really determine what they can do for you.</p>
                                        <img class="" src="{{asset('img/152530-OUMN2K-225.png')}}">    
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">3. What Interest Rate Could I Be Getting?</h1>
                                       <p align="justify">The traveling from start to finish together with your mortgage might take a little while. It may also take up to several months to close your mortgage, but during the period of several months, a great deal may occur to interest rates. If you are using a lender that would like the right for you, then they probably want you to locate the cheapest interest rate possible so you can save the more of your hard earned money. Were you aware that you are able to "lock" a rate? In the event you truly feel like costs are low and you would like to fix it before anything else happens, you can elect to lock in the speed in order to prevent ending up with a higher interest rate when it is time to close. You may want to pay fees to guarantee this ideal rate for your loan, but it is not mandatory.</p> 
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">4. What Are Discount Points and Origination Fees?</h1>
                                       <p align="justify">Lenders may or might not charge discount points or origination fees. A discount point is a charge which the borrower pays upfront on the lender at closing in exchange for a lower rate of interest. For reduction points, 1 point is equal to a percent of the amount of the loan. The very best way to describe a reduction point would be to call it "prepaid attention" As an instance, if you should take a mortgage of $300,000 and wish to cover two discount points, then your fee will complete to $6,000 (2% of $300,000). In case you're saving for quite a while and have a cozy cushion finance and realize that using a lower interest rate for the length of your loan is something that's a massive priority for you, paying discount points might be a fantastic idea. Even though some people today choose to be concerned about details later down the street, you will find many others who wish to play it safe and make sure that they're setting themselves up for success later on. There's not any appropriate way to do so, it depends on what your priorities are.</p> 
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">5. Which Are The Final Prices?</h1>
                                       <p align="justify">Final costs/fees is exactly what borrowers pay to the creditor along with the other parties which were involved in getting your mortgage. Within three business days of receiving a loan program, a creditor must supply a written breakdown of all of the fees and expenses. When speaking about preparing for house ownership, many will not understand there are extra costs along with the deposit. Closing costs are generally between 2%-5% of the loan value, which may cost a few thousand dollars. You may choose to get a no-closing-cost mortgage that essentially means your final price is absorbed to a mortgage. Your monthly payment will probably be greater since you've opted to include it to your general mortgage, however you'll not need to think about needing to save additional cash along with the down payment you've worked so tough to save.</p> 
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">7. Can There Be A Prepayment Penalty Associated With This Loan?</h1>
                                       <p align="justify">Believe it or not, you can get slapped with a penalty fee for paying your loan back too fast. A prepayment penalty fee is prohibited in certain countries, therefore it's very important to ask if this is something which accompanies your loan. This information is revealed and available on your arrangement, but you need to make certain of this information prior to signing. Lenders have prepayment penalty fees since when the loan has been paid back too fast, they're losing money from the dearth of interest. Ensure that you ask your lender when there's a penalty for paying premature and when there's, you can pay to get the penalty fee taken out of the contract or just request a distinct loan.</p> 
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">8. What Records Can I Have To Provide?</h1>
                                       <p align="justify">There are 3 chief files which you want and it may be a great deal of paperwork. You'll have to supply your proof of earnings and resources, personal identification and data regarding your credit history. It's absolutely crucial that this region of the procedure is done properly since in the event that you don't supply a number of the mandatory mortgage records, you might be turned down. Possessing a creditor who's organized and put-together can also decrease the strain associated with finding all the vital files by keeping tabs on what's been achieved and moving ahead with the procedure.</p> 
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">9. How Much Time Does It Take To Process Financing Program?</h1>
                                       <p align="justify">This response is totally determined by how fast you may provide the necessary documents on your own lender and how large his workload is in addition to the other parties which are involved. It might take as little as a couple of months to as long as 60 days, but should you speak with your creditor and work together it'll be as easy as it could be. To avoid slowing down the process, there are a number of things you can do. You can do your part to fully complete each the necessary documents, be ready with a justification for any previous credit difficulties, do not make a decision to change jobs or choose another career path in this time, and go on your credit report only if there are any mistakes.</p> 
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Overall Thoughts</h1>
                                       <p align="justify">Purchasing a home will probably be among the largest purchases of your life and getting someone dig out of your background and credit history can make you feel vulnerable. A fantastic lender must make you feel as if there's a solution for you, regardless of what your circumstance is. Sitting with a creditor to proceed ahead on your journey to buy a house can be intimidating and daunting, but provided that you're well equipped with the ideal questions, you shouldn't be blind-sided. These concerns should help to get rid of the anxiety that you aren't receiving the best price on your own or your lender is not considering your well-being.</p> 
                                    </div>
                                </article>
                                                                                                
                            </div>
                        </div>
                        <div class="col-md-4">
                             @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog -->
@endsection