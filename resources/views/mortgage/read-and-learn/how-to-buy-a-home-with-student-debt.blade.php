@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">How to Purchase a House with Student Debt</h1>
                                       
                                        <br>
                                       <p align="justify">Attempting to purchase a house with student loan debt may appear daunting and perhaps even impossible, but it's absolutely possible. In the following guide, we'll discuss some strategies and techniques that'll be great for people who have student debt, but also wish to be a homeowner.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Understand Your Student Debt</h1>
                                       <p align="justify">Student debt may undoubtedly be crippling to get a few. Most are postponing home possession due to their student debt. They think that since they've student debt they are not able to buy a house. Having a lot of debt gathered and not yet repaid, how can it be feasible to get qualified for a mortgage and take on more debt? Well, it is not impossible. There are numerous men and women who have houses and have student debt and aren't delinquent on their obligations to loans.</p>
                                        <ol>
                                            <li>Your credit Rating</li>
                                            <li>Your Deposit Number</li>
                                            <li>Your Earnings Compared to your Own Debt</li>
                                        </ol>
                                       <p align="justify">There are different variables connected with getting qualified for a mortgage like citizenship, but these are the three main ones.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Why Credit Score is So Important</h1>
                                       <p align="justify">Even in the event that you have debt, then it's still possible to have great credit. Your credit rating reveals lenders if you're always late or in the event that you consistently make your payments in time. It's crucial to understand that obtaining your credit assessed will decrease your credit rating. It's supposed to be just like a savings account; it is something you ought to keep and enhance, but maybe not touch regularly. In case you've got a sense your credit score requires development, keep making your payments in time. Paying your bills on time and in total will help to improve your credit rating and your financial standing. There are a number of advantages to getting a higher credit rating, therefore if yours can use some improvement, it's in your very best interest to do this prior to applying for a mortgage. Possessing a higher credit rating will qualify you to get more kinds of mortgages. It follows that when it comes to purchasing property, you'll have more choices about the best way best to fund it. Possessing a higher credit rating will also provide you the advantage of having a lower rate of interest, meaning you will be able to save your hard earned cash rather than using it to cover attention.</p>
                                        <img class="" src="{{asset('img/288693-P6NYH1-966.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Why Your Down Payment Is Crucial</h1>
                                       <p align="justify">Much like your credit rating, your deposit amount could accommodate you to get different kinds of mortgages. Most mortgages require a deposit of 20 percent of the house's selling price, but you will find different mortgages which need significantly less than that. By way of instance, a FHA Loan expects that the borrower set down a minimum deposit of 3.5 percent, but since it's significantly less compared to traditional 20% down payment, the borrower must buy FHA Mortgage Insurance. Traditional mortgages need 20 percent down, have reduced rates of interest, and do not require the borrower to buy mortgage insurance. Possessing a bigger down payment provides you the freedom of having more options and because different mortgage options also have different rates of interest, it's better that you qualify for as many as possible. Along with raising your options, acquiring a bigger down payment will just assist with your mortgage since the more you put down, the less you are going to need to pay every month. This may also help enhance your DTI (debt to earnings) ratio.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Debt to Income Ratio (DTI)</h1>
                                       <p align="justify">While credit rating is very important, your DTI ratio is most likely even more significant. This reveals the creditor if you're in fact able to manage buying a house. A credit rating essentially shows the creditor when you've got a credit history and it demonstrates how well you're in managing your credit card. You may take a mountain of debt, but also have a fantastic credit rating. As a result of this, your credit rating is not the one thing that lenders consider as a way to assess your fiscal situation. Even though this might make people that have student debt worried, the DTI ratio measures how well you'd have the ability to afford your loan, no matter whether you have student loan debt. To compute your DTI ratio, then add up all your monthly statements. Include your student loan repayment, rent, power and gas and water bills, credit card payments, etc.. Take that amount and divide it by your gross monthly income (the amount you make BEFORE the taxes). The outcome is the DTI ratio and the lower the number, the greater your position is going to be.</p>
                                        <img class="" src="{{asset('img/1133.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">How to Decrease Your DTI</h1>
                                       <p align="justify">Your DTI ratio needs to be 43 percent or less to be able to become qualified for a mortgage since that is the greatest a DTI ratio could be to get a creditor to issue financing. It's crucial to keep in mind that if your outcome was greater than 43%, it isn't the end of the planet. The DTI ratio is figured in your monthly financial commitments. In case you've got various kinds of student loans which have different rates of interest that need different payment levels, then you can refinance them and find a lower rate of interest. Since the curiosity about student loans are amortized, your monthly repayment amount will decrease thus providing you with a lesser DTI ratio. In case you've got high interest credit cards, it's ideal to cover off those. A lot of individuals don't appear to understand that even when they have a comparatively low equilibrium, just paying the minimum payment amount each month may maintain them for ages. Earning debt with the maximum interest rates are greatest because after the balance is repaid, it is going to free up more cash to go towards other debt and cover down those down quicker. Reducing card accounts will lower the monthly payment amount, which is going to create a lower DTI percentage.</p>
                                        
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Getting Pre-Qualified to Find a Mortgage</h1>
                                       <p align="justify">If you truly feel like you're in good shape to find a home, you can see if you pre-qualify to acquire a mortgage. Getting pre-qualified is the very first step into the mortgage process and can allow you to focus on how much you're able. There is no dedication involved with pre-qualification. A good deal of women and men get pre-approval and pre-qualification mixed upward, but they are not precisely the specific same and there is a gap between the two. Pre-qualification determines how much space you can manage and does not request that you supply each the files connected to your finances (these might be self-reported). Pre-qualification also does not require a credit score, nor does it require you to apply for financing. These might also be online using mortgage pre-qualification calculators for those that do not need to talk with a lender. While pre-qualification is a great instrument utilized to estimate how much your loan amount can be, it's not ensured. These are all estimates and approximations based on the provided information and since it does not call for a charge report, the result may vary.</p>
                                        
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Obtaining Pre-Approved to Get a Mortgage</h1>
                                       <p align="justify">After going through pre-qualification, now you can secure pre-approved for a mortgage. Pre-approval does need you to apply for a loan and will want a credit check along with all of the documentation necessary to establish your financial status like pay stubs, bank statements, two years' worth of tax types, etc.. If you wind up becoming qualified for a mortgage, then you're not required to commit. You're still able to become multiple pre-approval letters from various lenders before making a selection. It's essential to be aware that if you're pre-approved to get a mortgage, it's not guaranteed you'll find the loan. This generally only occurs in case the real estate agreement drops, but the majority of the time it's because of changes in the fiscal situation which were created after the actuality. Some of those changes involve change in job, changes made from the loan conditions, and taking on extra debt. When applying for a loan, it's necessary not to use for any credit cards, apply for a car loan to get a new card, or even change jobs. Your acceptance is dependent on what has been sent in and analyzed so it must remain the exact same or improve before you have the loan.</p>
                                        
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Overall Thoughts</h1>
                                       <p align="justify">Hoping to take on more money once you have debt does not sound like a scenario that creditors would always approve of, but mercifully the fantasy of owning a house is accessible to everybody. Improving your credit rating and lowering your own DTI ratio are a few of the ways it is possible to show that you're capable of taking on much greater financial obligation. Provided that you're accountable and can handle your debt, home ownership is definitely feasible for you.</p>
                                    </div>
                                </article>                                                                
                            </div>
                        </div>
                        <div class="col-md-4">
                             @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 
@endsection