@extends('layout.app')



@section('main-content')
<main>
<!-- Start Blog -->
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">The Hidden Costs That Come With a Mortgage</h1>
                                       
                               
                               
                                       
                                       <p align="justify">When talking about genuine estate, the word "mortgage loan agent" is used quite often, but what precisely is a mortgage broker? A mortgage agent is somebody that you employ to be the a game maker. They are the ones that match up you having a financial institution or creditor to trouble together with your home loan.</p>
                                       <img class="" src="{{asset('img/mortgage-710x473.jpg')}}">
                                        
                                      
                                    

                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What Exactly Does a Large Financial Company Do?</h1>
                                       <p align="justify">Since the large financial company will undoubtedly be picking out some thing very important in your benefit, they'll first have to gather all of your important financial details. This would incorporate pay stubs, tax statements, bank account statements, a credit score report, etc.. Basically, you could supply them with anything that is mandatory in order to obtain a house loan. The greatest benefit and benefit of a mortgage broker will be that they can compare numerous mortgage loan costs from several lenders and find the best premiums or programs with the lowest charges. As ordinary men and women, we're not licensed nor would we all have the links which agents need to see what's available at one time. Mortgages brokers assist all sorts of home loans. They work with FHA financial loans, conventional loans, jumbo loans, mortgage payments, etc...</p>
                                        
                                      
                      
                                    </div>
                                </article>
                               
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">How Can I Look for a Mortgage Broker?</h1>
                                       <p align="justify">Agents are usually suggested by your real estate agent or with way of a close friend. You are able to even find a single online that works into your neighborhood area or the area that you want to dwell in. If you should be searching for you personally, you'll find plenty of mortgage broker overviews online that will help you on your hunt.</p>

                                          <img class="" src="{{asset('img/Mortgage-Broker-Vancouver-Services.jpg')}}">
                                      
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Do You Definitely Require a Home Loan Broker?</h1>
                                       <p align="justify">The answer to that issue is not any. No, you do not have to have a large financial company in order to find a mortgage. You're entirely free to really go about searching for yourself and do your search. A amazing candidate to get a mortgage broker is somebody who comes with an incredibly busy schedule or someone that is looking to purchase investment land at a specific location. In the event you are thinking of buying real estate at a separate country, mortgage brokers are great due to the fact they do all the work with you while you're remote. Somebody that doesn't possess time only because they have so much moving on will probably also substantially benefit out of your mortgage broker. Since mortgage brokers are commission based, it may be sensible to execute a small amount of your analysis and examine the results. In this manner, you are going to know whether the mortgage broker gets your best interest in mind. In addition, it is important to determine whether your broker is commission-based or should they cost a fee upfront.</p>

                                        
                                      
                                    </div>
                                </article>
                              
                                <!-- End single item -->
                            </div>
                        </div>
                        <div class="col-md-4">
                        @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 
@endsection

