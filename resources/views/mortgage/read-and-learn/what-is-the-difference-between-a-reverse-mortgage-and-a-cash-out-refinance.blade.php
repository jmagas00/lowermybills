@extends('layout.app')

@section('main-content')
<main>
<!-- Start Blog -->
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">What is the Difference Between a Reverse Mortgage and a Cash Out Refinance?</h1>
                                       
                                        <br>
                                        <p align="justify">There are lots of differences between a reverse mortgage and a cash out refinance. While the two applications use home equity to get money back, the reverse mortgage is especially for elderly citizens.</p>
                                        <img class="" src="{{asset('img/480.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What is a Reverse Mortgage?</h1>
                                       	<p align="justify">Home Equity Conversion Mortgage is the Appropriate Title for Your reverse mortgage.</p>
																				<p align="justify">The reverse mortgage is exclusive to older citizens that are at least 62 years of age. Elderly citizens that qualify for a reverse mortgage may utilize their home equity as extra income whilst still residing in their property.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What is a Cash Out Refinance?</h1>
                                       <p align="justify">A Cash Out Refinance permits homeowners to refinance their mortgage to get a greater amount than that which they owe. The homeowner chooses the difference in money and is totally free to use it to anything they need. The key reasons to get a cash out refinance would be to cover expensive repairs, to pay for a house remodel, to repay high interest debt, etc..</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What are the Biggest Differences Between a Reverse Mortgage and a Cash Out Refinance?</h1>
                                        <ol>
                                            <li>A Reverse Mortgage is only available to senior citizens aged 62 or older.</li>
                                            <li>A Reverse Mortgage is an extra mortgage that's removed. It doesn't replace the present mortgage.</li>
                                            <li>Reverse Mortgages do not need monthly payments. On the contrary, it's reimbursed at the end of the expression.</li>
                                        </ol>
                                        <img class="" src="{{asset('img/2158.jpg')}}"> 
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                      <h1 class="mu-blog-item-title">What are the Benefits to a Reverse Mortgage?</h1>
                                      <p align="justify">A reverse mortgage doesn't change your mortgage. It's another loan along with your mortgage. This usually means the homeowner will keep on making payments on their mortgage but using a reverse mortgage they might pay their mortgage off with the money obtained through a reverse mortgage.</p>
																			<p align="justify">A lot of men and women aim to retire to live a life. Unfortunately, many seniors discover they haven't saved up enough to survive comfortably. Using a reverse mortgage, seniors will boost their cash flow and have money set aside for emergencies, pay for home improvements, etc..</p>
																			<p align="justify">Also, it's a frequent misconception that using a reverse mortgage, the possession of the house is given up, but this isn't correct. Reverse Mortgages may benefit seniors by providing them with extra funds through their house while they're still living inside. They still have the home unless they or their heirs opt to sell it.</p>
																		</div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                      <h1 class="mu-blog-item-title">Overall Thoughts</h1>
                                      <p align="justify">A reverse mortgage is a fantastic solution for seniors that wish to be certain they are in good financial position. Maintaining away additional cash is never a bad idea. It is far better to prepare for a costly home repair or health emergency than not in all. By not needing to make monthly payments, seniors up more of the income to put towards other vital things.</p>
																		</div>
                                </article>
                            </div>
                        </div>
                        <div class="col-md-4">
                             @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection