@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">Why are Interest Rates So Important?</h1>
                                      
                                        <br>
                                       <p align="justify">Interest levels are incredibly significant to your mortgage. They dictate just how much additional money one pays along with their own amount of the loan. Interest is generally added when borrowing cash to make a huge buy, or borrowing money generally.</p>
                                       <img class="" src="{{asset('img/pexels-photo-69760.jpeg')}}"> 
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                       <h1 class="mu-blog-item-title">Crucial Things to Know about Interest Rates</h1>
                                       <p align="justify">Just how do you calculate interest? Could it be computed by accepting the interest rate and multiplying it in the entire quantity of the loan? It makes sense as houses aren't cheap and the interest will cost you thousands of dollars. Many first-time buyers don't know that the interest rate on your mortgage is actually a monthly interest rate.</p>
                                       <p align="justify">To be able have the capability to figure how much the rate of interest impacts your mortgage, take into consideration the rate of interest and divide it by 12. That can be sometimes actually for the 12 months annual. Should you do the mathematics, 3 percent of $100,000 is just three million bucks. Unfortunately, you wouldn't only be paying three million dollars.</p>
                                       <p align="justify">The resulting amount is how much attention you would pay for this particular month. In this particular scenario, the interest because of the month is now $250. Depending upon the length you've chosen,this typically means that curiosity will likely cost well over the $3,000 you initially thought you are paying.</p>
                                    <br />                                        
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                       <h1 class="mu-blog-item-title">Why Are You Two Interest Prices?</h1>
                                       <p align="justify">When looking at several rates the creditors have to offer, you may always see two places of interest rates side by side. An individual will be tagged as the "interest rate" and the other will probably be tagged as the "APR." Occasionally, both rates will probably be equal to one another and sometimes, there's a small gap. The expression "APR" stands for Annual Percentage Rate and gives the mortgagee a clearer notion about exactly what the loan will really cost them in the initial year.</p>
                                       <p align="justify">The APR doesn't have any impact in your monthly payment. Should you see two distinct sets of numbers, then the APR will probably be greater. This is normally because the loan will need reduction points and origination fees upfront.</p>
                                       <img class="" src="{{asset('img/OH5IPD0.jpg')}}"> 
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                       <h1 class="mu-blog-item-title">Overall Thoughts</h1>
                                       <p align="justify">It's essential to know about these little details before buying a house so you understand what you are getting yourself into. That little difference between monthly rate of interest and general interest rate may better prepare buyers to their mortgage.</p>
                                        
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="col-md-4">
                             @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 



@endsection