@extends('layout.app')

@section('main-content')
<main>
<!-- Start Blog -->
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">The Way Trump's Tax Cuts and Job Act May Impact the Marketplace</h1>
                                        
                                        <br>
                                       <p align="justify">Trump's Tax Acts and Job Cuts was released on November 2, 2017 and at that 1,000+ page bill, there was a department that could influence the housing industry. President Trump has diminished the quantity that homeowners are permitted to write off in interest payments. Ahead of that, homeowners were permitted to write off around $1 million in interest payments, that was a fantastic incentive for homeowners and a massive incentive for men and women that were seeking to buy a house. With less of the edge related to being a homeowner, the motivation to purchase a residence is predicted to drop by the wayside and slowdown much more so.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Millennials Can Not Afford To Purchase A Home</h1>
                                       <p align="justify">It's no secret that millennials are not buying houses. They just can not manage it and much more millennials have been reported to lease with minimal intention to purchase or continue living in your home so they can work towards saving for a deposit. Although it appears small today, this may actually become a larger issue. Millennials are now anticipated to sustain the home market, however it is not happening. Millennials are now in the age when their families purchased their first house and had kids, but these kinds of plans are becoming delayed. The probability of present homeowners purchasing several extra properties is to the lower side. They aren't likely to buy a second house unless they are a real estate professional that purchases investment properties for gain, or should they possess the monetary means to do so. We can't rely on the professionals to keep on buying houses and maintain our marketplace afloat.</p>
                                        <img class="" src="{{asset('img/OH5IPD0.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What Trump's Tax Cuts and Job Act Means For Millennials</h1>
                                       <p align="justify">In case millennials are already slowing down the pace at which homes are being purchased, then that new alteration in tax advantages will make things go along more gradually than it was. Additionally, a growing number of millennials are reported to be bypassing the "starter house" and appear to be moving straight to purchasing the dollar "forever home" since they're waiting longer to save up to the bigger down payment. Together with millennials already setting off the purchase of the first home because of it not as much cheap and Trump's brand new Tax Cuts and Job Act, you can surely expect an even larger decrease in the speed of residence purchases. President Trump diminished the quantity that homeowners can compose in interest payments. If millennials were buying starter houses for lower costs, then this change would not have an impending influence in the marketplace. But since millennials are deciding to reside with their parents so as to save up to the bigger down payment, the motivation to continue with their programs will weaken. The capability to compose interest payments is not any more the benefit it was.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What Will Happen Should Millennials Continue Not Purchasing Homes?</h1>
                                       <p align="justify">In case millennials continue not to buy houses, then house values will gradually return. This is due to the supply/demand idea. In case the demand for houses are large, then the costs of houses will be higher. In case the demand for houses decrease, then the purchase price of houses will even begin to shrink. The value of houses is dependent on the dollar amount houses in your area are selling for, and house values can vary. Even though this may be perfect for those that wish to purchase a house but believed it was too pricey, this might be bad news for those that are currently homeowners. As stated earlier, house values can go down and up and this includes those who have already been bought. Just because your house has been assessed for a particular amount once you bought it, it does not automatically indicate it is going to remain like that. The appraiser provides you the house worth based on if you bought the house, maybe not if it is going to keep on holding that worth. Even in the event that you've made house renovations in hopes of raising the value of your house, value of your house will return in case the costs of houses in your town return. If the house worth goes down that your mortgage is much more than what your house is currently worth, that usually means you're submerged or upside down in your mortgage.</p>
                                        <img class="" src="{{asset('img/2936.jpg')}}">    
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What Can I Do To Prevent Being Underwater On My Mortgage?</h1>
                                       <p align="justify">Regrettably, there is not much you can perform. You have signed up your mortgage and you are dedicated to paying your loan off if your house increases or declines in value. There are some refinance choices that could be useful. If you do not already have one, then you can refinance into a 30-year fixed mortgage so you will have exactly the identical payment for the whole duration for a very low quantity. You might even make the most of your present home worth and tap into your home's equity for money.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">The Best Way To Use A Cash Out Refinance For Your Edge</h1>
                                       <p align="justify">Your equity may also fluctuate based upon your home's worth. If home values are steadily increasing, then so you are building equity up by doing this. Though you have not officially paid off this dollar number on your loan, you finally have that equity since if you should sell your house, that is how much you'd get after paying off the remainder of your mortgage. If home values plummet along with your mortgage is greater than what your house is worth, you finally don't have any equity regardless of all of the payments you have been making through time. That is because if you should sell the home for its low price, you would not have the ability to pay back the whole loan. Together with Trump's brand new Tax Cuts and Jobs Act and the constant decrease of millennials not purchasing houses, we could expect rates to fall. The best case scenario is to take a cash out refinance loan while your house value remains high and receive your equity in money before it disappears into thin air. Yes, you will eventually need to pay this sum back, but you'll have tapped to your equity as you had it. If you do not, you may have zero equity and you might be submerged in your mortgage. There are a few limitations to some cash out refinance. You can't take out 100 percent of your equity in money, but it is possible to extract 80%. Even though it is not the complete amount, it's a way better choice to have 80 percent of your equity easily accessible for you to use than nothing in any way.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Overall Thoughts</h1>
                                       <p align="justify">Tapping into your equity has turned out to be helpful when you had a significant investment, but given the present scenario, cashing in on your equity could be the ideal thing for this circumstance. You're already committed to paying off the main, if you've got a 30 or 15-year mended or an Adjustable Rate Mortgage. In the event the value of your house has become since you bought it, then cashing in on this extra equity could be the ideal approach to really use it for your benefit while it exists.</p>
                                    </div>
                                </article>
                                <!-- End single item -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mu-blog-sidebar">
                                <!-- start Single Widget -->
                                <div class="mu-sidebar-widget">
                                    <h2 class="mu-sidebar-widget-title">Other Types of Mortgages</h2>
                                    <ul class="mu-sidebar-widget-nav">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Veteran Loans</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>FHA Loans</a>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>30 Year Fixed Rate Mortgage</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>What is the Difference Between a Fixed Rate Mortgage and an Adjustable Rate Mortgage?</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Cash Out Refinancing</a></li>
                                    </ul>
                                    
                                </div>
                                <div class="mu-sidebar-widget">
                                    <h2 class="mu-sidebar-widget-title">Mortgage Basics and How to Guides</h2>
                                    <ul class="mu-sidebar-widget-nav">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>What To Expect During the Mortgage Process</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>The Hidden Costs That Come With a Mortgage)</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>What Questions Should You Ask Your Lender?</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Why Should I Refinance My Home?</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>So You've Decided To Buy A Home, What Happens Now?</a></li>
                                    </ul>
                                </div>
                                <div class="mu-sidebar-widget">
                                    <h2 class="mu-sidebar-widget-title">Links To Other Tools</h2>
                                    <ul class="mu-sidebar-widget-nav">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/calculator.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Calculator</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/money.svg')}}" alt="" style="max-width:15%;"> &nbsp; Refinance Calculator</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/comparision.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Comparison</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/analysis.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Analysis</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/bulb.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Advisor</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 

@endsection