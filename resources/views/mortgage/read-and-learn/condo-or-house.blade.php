@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">Can I Buy a Condo or a Home?</h1>
                                       
                                        <br>
                                       <p align="justify">For all, there'll be a come a time when they'll determine that leasing is no more for them and they've opted to take the measures prior to purchasing their very first article of property. If you're on the fence about buying since the idea of staying in 1 place for a very long duration of time bothers you, you will find mortgages that may accommodate you, but in case you're contemplating this for quite a while and are prepared... then congratulations!</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Things to Consider</h1>
                                       <p align="justify">For a first time house buyer, I am convinced the question of if purchasing a condominium or purchasing a home has arisen in your head before. "Should I purchase a unit from several in a construction? Or if I purchase a single family dwelling?" There are lots of things to consider like the obvious way of life and location preferences of their potential home buyer however there's also the expense of upkeep, rules of possession, and the monthly charges which might be the decision maker. 1 choice isn't always better than another and isn't a dimension of the house buyer's financial achievement. They simply have to make the best selection for themselves.</p>
                                        <img class="" src="{{asset('img/img_city_house.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Benefits Which Include Owning a Condo: Price</h1>
                                       <p align="justify">There are lots of advantages that come with buying a condominium, the most important one being the cost. Condos normally have a more appealing price tag, but what exactly are you actually buying? The clearest distinction is that a condominium is smaller compared to a single family dwelling. Condos are generally more compact than homes and they feel quite much like a flat. Even though there's a gap in the magnitude of the two kinds of homes, it does not need to be a con.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Benefits Which Include Owning a Condo: Size</h1>
                                       <p align="justify">As a lot people know the longer you occupy a distance and the more space it's, a growing number of objects have a tendency to develop and gather. This notion appears to be related to any sort of scenario like your desk place on the job, your car, your luggage, etc.. The identical thing goes to your property. While homes can provide additional storage space, there's also the probability of clutter and also the group of unnecessary items and since condos have less space, there is less chance for the buildup. Additionally, because the distance of a condominium is smaller, the expense of living is significantly less.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Extra Costs You Ought to Know About (Condo)</h1>
                                       <p align="justify">Apart from the purchase price of the condominium itself, condos normally have a HOA (Homeowner's Association) fee which you will need to pay in the kind of a monthly fee as well as a monthly mortgage payment. Saving cash might have been among the very best reasons to get a condominium, so it's clear that the extra fees might be a drawback, however these prices are mainly used towards building upkeep and also to sustain the beachfront conveniences which are also included along with your unit for the pleasure.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What is a Homeowner’s Association?</h1>
                                       <p align="justify">The Homeowner's Association is a volunteer group that's constituted of the owners of their units and consequently, they determine what areas of the complicated need attention to be able to conserve the real estate value. Since multiple parties and families reside in precisely the exact same complex, everyone is accountable for the upkeep of communal areas like the lifts, health club, swimming pool, landscaping, roofing, safety, etc.. While the intention behind the HOA would be to keep up the standard of living for the own community, they could sometimes be a hassle.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Benefits of a HOA Fee (Condo)</h1>
                                       <p align="justify">Having a home, the homeowner is 100% accountable for the upkeep of both the exterior and the interior, for example, lawn and backyard. A whole lot of the instances, a specialist support is hired to assist with the maintenance or costly tools are bought to make the task simpler. With a condominium, the owner is mostly responsible for taking good care of the interior of the unit. Paying HOA fees may remove the hassle of having to determine what has to be repaired and it may be financially advantageous to you because each the owners are expected to cover the fees and the general cost of repairs will be split between each one the residents.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Cons of a HOA Fee</h1>
                                       <p align="justify">There are principles which may be put in place and imposed by the HOA, which can be similar to the principles an apartment tenant must abide by. The HOA can pick if you're permitted to own pets and what type of pet you're permitted to own, if you may have a satellite dish, even if you may have a little backyard, etc..</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Added Things You Need to Know About the HOA</h1>
                                       <p align="justify">HOAs are not only exclusive to condos, but in addition they exist in gated communities and subdivisions with different houses and they are able to dictate what colour you would like to paint your home, the sort of cars which are permitted to park on the road, and even when they're permitted to be parked on the road. When considering your choices, it's very important to think about if abiding by those rules can affect the quality of your own life and also to establish if spending the excess cash to get a home is well worth it.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Benefits of Owning a House: More Freedom</h1>
                                       <p align="justify">Total, homes do need much more effort, but the payoff could be well worth it. You have to have full charge of your premises and are not needed to seek the acceptance of an HOA. You're able to truly express yourself, decorate your own house to reflect your personality and preferences, and reap the benefits that come with owning your own house.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Bigger Isn’t Always Better</h1>
                                       <p align="justify">The price of living in a home versus a condominium is much more expensive. Since the land itself costs more, you'll need to find a larger loan, which implies that the monthly payment will probably be longer. Additionally, it usually means the insurance and the taxes will be greater as when the house value is greater, the real estate tax will be expensive. Since the land itself will probably have more square footage, there's currently more to preserve, which will lead to a marginally more costly utility invoice. It will need more warmth to heat a bigger place and in the event the home has a lawn or a garden, it is going to have to be watered that will be reflected on your water bill.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Bigger Isn’t Always Better</h1>
                                       <p align="justify">The price of living in a home versus a condominium is much more expensive. Since the land itself costs more, you'll need to find a larger loan, so the monthly payment will probably be. Additionally, it usually means the insurance and the taxes will be greater because when the house value is greater, the real estate tax will be expensive. Since the land itself will probably have more square footage, there's more to preserve, which will lead to a somewhat more expensive utility bill. It will need more warmth to warm up a bigger area and in the event the home has a lawn or a garden, it is going to have to be watered that will be reflected on your water bill.</p>
                                        <img class="" src="{{asset('img/img_smartphone_mobility.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Location, Location, Location</h1>
                                       <p align="justify">In which you want to live could also have a huge influence in your choice between choosing a condo or a house. If you'd like to remain near a lengthier city like setting, a condo is much more suitable. They are typically assembled around urban areas and are in close proximity and walking distances to coffee shops, bars, restaurants, and other fascinating locations. Since numerous attractions may be available, a car isn't necessarily crucial.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Pros of Living in a More Urban Area</h1>
                                       <p align="justify">Dwelling at a metropolitan area may seem attractive to many mainly because everyone is truly close to you, but in addition, this is a con. Living in a condo is quite much like living in a flat unit, however you are the person who owns the home and are the neighbors in your complex. Having neighbors in close proximity can make a sense of community, but together with your own personal privacy may also be important. Sometimes you're not in the mood to get small discussion whilst accessing your email address.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What Should I Do Not Have 20&#37; Travels?</h1>
                                       <p align="justify">In case you are hesitant on purchasing a house because you've got less than 20 percent of the house value to get a deposit, there's the FHA Loan that allows qualified home buyers to buy houses and place down as small as 3.5 percent. If later in life you end up in a better fiscal situation or your own priorities have shifted, you can always refinance into another kind of mortgage to fit your requirements. The choice to refinance and alter your mortgage is there, therefore it's ideal to consider your long-term pleasure from the very start.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Neighbors</h1>
                                       <p align="justify">Additionally, because your neighbor will most probably be a fellow condominium owner, they most likely don't have plans to go out anytime soon. If they're the type of neighbors who like to sponsor karaoke contests late at night and throughout the week, that's likely to become part of your life also. Having a home, you will find lawns and drives which serve as a divider between you and your neighbors that may supply you with a bit more solitude that may go a very long way.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">In the Long Run, Your Conclusion is the Most Essential</h1>
                                       <p align="justify">The most significant person that you want to consult when determining which kind of property that you would like to purchase is yourself. You should not need to compromise while purchasing a house. You will be the one to reside there, therefore it's very important to set your pleasure and well-being first. There are pros and cons to every kind of home and while it might be better to other people that's a much better alternative, that does not automatically indicate it's going to be the ideal selection for you. If your finances are swaying your decision, please remember there are an assortment of mortgages on the market to suit nearly every kind of lifestyle and condition.</p>
                                    </div>
                                </article>
                                
                            </div>
                        </div>
                        <div class="col-md-4">
                             @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 
@endsection