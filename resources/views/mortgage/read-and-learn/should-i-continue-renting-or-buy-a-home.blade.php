@extends('layout.app')

@section('main-content')
<main>
<!-- Start Blog -->
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">Should I Continue Renting or Buy a Home?</h1>
                                       
                                        <br>
                                       
                                       <p align="justify">It's become a subject of debate for quite a while although there are legitimate disagreements to either side, it's however a challenging turn to if buying or leasing would be a much far better financial movement.</p>

                                        
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Renting is Less Costly</h1>
                                       <p align="justify">First of all, it is pretty clear to mention that leasing is substantially less expensive compared to just buying. That you don't need to save as much money to make use of as a down payment to get a household. While there is actually a safety deposit that is ordinarily required just before registering for up a lease to your different living situation, the cost of the collateral deposit is much less than 20 percent on the selling cost of a house. Leasing can also be more economical because the renter, you aren't expected to look after any significant repairs, so long as they are maybe not your own fault. When there's a flow in the roof or a burst pipe, then the money that goes into fixing this really does not come from your own wallet. In addition to lacking to pay for expensive repairs, you're additionally exempt from needing to cover the real estate taxes as well as the home loan insurance policy.</p>
                                        
                      
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Renting is Loss Costly Upfront, But Over Time….</h1>
                                       <p align="justify">Renting is an equally attractive option since that you need not spare up a sizable amount of money to utilize over a down payment or on high priced repairs, property taxes, or even insurance. While renting is more economical upfront, it might accumulate over time. In the event you chose to exude the American dream to be a property owner and chose to lease for your remainder of your own life, you might have compensated enough in hire to obtain your own personal item of home. If your spouse charges £1,500 a month, then not raises your rent, and you rented the exact device for 1-5 years, you will have paid $270,000 during that moment; point. Most say that using renting, you're throwing your money away ever since your hard earned money will not move towards anything, which isn't the circumstance. With the rent you're spending, you have the capacity to to have a place to live. It actually is up to you to decide if paying so substantially more period will be worth it. By leasing, you can find instances which occur more frequently than not when a renter is basically evicted because the landlord chose to triple the leasing cost. The leasing that you are paying out is contingent upon the landlord and so they can change that should they want. If you have a great relationship by means of your spouse and also are confident that your rent will stay inside of the purchase price range, then there is actuallyn't a motivation to buy your own home.</p>
                                        <img class="" src="{{asset('img/House_rent_sign.jpg')}}">
                                        
                                      
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Location and Surrounding Property Values Could Play a Big Part in Your Rent</h1>
                                       <p align="justify">If you're in a location where your home worth are steadily rising year like cities that are big, then leasing could be the best alternative. If home values are steadily rising, you could be playing grab up to store 20% for a deposit. However far you save your self 20 percent on a dollar sum which is always changing means that you will always need to truly save even more. There is even the flip side of the argument since dwelling worth are continuing to move upward, you are at the will of your own landlord and also you may get yourself a surprise surge in your rent. If your landlord is still ongoing to improve the rent as a way to match the home worthiness of that area, then perhaps placing aside cash to purchase property can be advisable.</p>

                                        
                                      
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">You Don’t Have To Commit To One Location</h1>
                                       <p align="justify">Another benefit to renting is that you're not required to remain in your rental as you will find leases that are per month monthly. If you are inbetween professions or are thinking of moving someplace else for a reversal of pace, you can certainly do that without the impacts and hardly any work. The duration for the majority of mortgages have been 30 decades, so if you're contemplating purchasing property, you'll probably be spending the mortgage off for approximately 30 years.</p>

                                        
                                      
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">The Benefits of Home Ownership: Different Mortgage Types</h1>
                                           <p align="justify">It's costly to buy a home since not many men and women have thousands and tens of thousands of dollars sitting in home expecting to be invested. While home-ownership could be more expensive than leasing, you can find various mortgage types out there to make home ownership more affordable, based on what your intentions are. Although there exists a significant price label in your the property, mortgages can create purchasing a property simpler, with regards to usually the main one which you choose.</p>
                                            
                                        
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">FHA Loan</h1>

                                      <p align="justify">Saving up 20 percent of the sale cost of a home may be a lot of cash, based on just how much the home is buying for. If homeownership is something which you want to your own but you are nowhere close using 20% saved to make use of as a down payment, then you also can decide on a FHA (Federal Housing Administration) bank loan. A FHA Loan requires the prospective house owner to place down a minimum of 3.5% because the down cost. 3.5% as a deposit can still be a lot, but it is much more affordable than 20 percent also that it is something that is achievable. In addition, if you are thinking about purchasing home with somebody else, protecting up 3.5 percent will happen a lot more quickly. Since 20% is usually required for some home loans, Private Mortgage Insurance (PMI) is demanded for FHA Loans since it is a hazard for lenders to loan income together with less than 20% down. Luckily, PMI isn't a lasting accession to a house loan. Once you've paid away everything will be 20 percent, then you are able to terminate your PMI payments. PMI might be a bit pricy, nearly costing you too far as your mortgage payment, but in the event that you are purchasing property by means of your spouse or intending on locating a home, their section of the rent will offset this price.</p>
                                       
                            
                                    </div>
                                </article>




                                  <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Adjustable Rate Mortgage (ARM)</h1>
                                       <p align="justify">In the event you happen to own 20% saved upward, however, you're focused on being unable to pay for the monthly obligations right now, you could also opt to get a Flexible Rate Mortgage (ARM). An ARM is considered a hybrid mortgage, meaning it has a set and an adjustable interest rate. An ARM contains two pieces: the initial period and the period following the first phase is over. During the first time, the rate of interest is adjusted and non refundable, which makes the mortgage payments more affordable during this time. Right after the original stage ends, the rate of interest will subsequently adjust towards the industry so your month-to-month payment can either go up or down. If you are on the lookout for an reasonably priced mortgage in the mean time since you're expecting a greater income in the future (presume a pending large advertising or some health student on their way to become a physician), an ARM can be just a fantastic alternative since the future cover raise should be able to offset the larger expenses.</p>
                                        
                                        <img class="" src="{{asset('img/2.jpg')}}">
                                    </div>
                                </article>




                                
                             
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">These Options Are Not Permanent</h1>
                                       <p align="justify">If you'd opt to go for a FHA bank loan or a ARM once you turned into a house owner and also determined the huge benefits that these house loans made available are nolonger beneficial for your requirementspersonally, you always have the option to refinance into a greater house loan. The best thing about re financing is you may switch mortgages out according to what period of existence you are searching for a far better bank loan that fits with your fresh group of priorities. In the event you have an ARM to benefit from those low month-to-month obligations, however are not a lover of this flexible rate of interest, you're able to refinance to some 30-Year fixedrate mortgage that'll provide you a marginally higher rate of interest, however, also the adjusted interest will probably guarantee you for exactly the exact same month-to-month mortgage repayment and soon you've paid your loan off. You'll have additional hours for the own mortgage, however as you're dividing the remaining of one's amount of the loan to a brand new collection of thirty decades, your mortgage payment will probably always be fairly lower.</p>
                                       
                                        
                                    </div>
                                </article>


                                 <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Overall Thoughts</h1>
                                       <p align="justify">Leasing and purchasing both possess their particular advantages, however, in addition, there are drawbacks for the two. This will depend upon which you find just as valuable and also what exactly you may pay for. Luckily, you'll find various house loan types which may help anybody get going within their course into homeownership. If homeownership is some thing which you have ever wanted to your self, however, felt as though this is a dream, it is maybe not. If you need a minimal month-to-month charge along with perhaps a reduced cost to start, you need selections. You simply have to determine if you'd like to shoot them.</p>
                                       
                                        
                                    </div>
                                </article>
                               
                                                               
                                <!-- End single item -->
                            </div>
                        </div>
                        <div class="col-md-4">
                        @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 



@endsection