@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">Should I Refinance My House?</h1>
                                       
                                        <br>
                                       <p align="justify">Along with all the ads urging homeowners to refinance their houses, it makes people wonder whether they need to and how would it benefit them. To refinance your mortgage ensures that you're paying off the rest of your current loan and replacing it with a new person, or altering the conditions of your current mortgage. Any homeowner who now has a mortgage and is looking for change is a fantastic candidate for refinancing their mortgage. They can refinance and get a new type of mortgage or else they could correct their mortgage so it's better rewards for them.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Common Reasons to Refinance</h1>
                                       <p align="justify">There Are Lots of reasons as to why homeowners refinance their Houses for Example:</p>
                                        <ul>
                                            <li>Use a lower interest rate</li>
                                            <li>Lowering their monthly payment, shortening the Duration of the loan</li>
                                            <li>Cashing in on their Own Mortgage to get a Lot of Money</li>
                                        </ul>
                                        <br />
                                       <p align="justify">Each one of these motives hold different advantages based upon the homeowner along with their priorities, therefore keep reading in the event that you would like to find out whether you are able to reap the benefits of refinancing your mortgage.</p>
                                        <img class="" src="{{asset('img/WhyRefinance_infographic-02.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">You Wish To Decrease Your Interest Rate</h1>
                                       <p align="justify">Among the most typical and greatest reasons to refinance is to decrease the rate of interest on your current mortgage. The typical homeowner could save around $3,000 annually by simply assessing their mortgage. By cutting the rate of interest, you can't just save a good deal of cash over the life span of your loan, but you also can repay your principal quicker. What a lot of don't see is that using a 30 year fixed rate mortgage, the majority of the payments during the first 12 years are mostly to pay back the interest. This implies that after making monthly mortgage payments for 12 decades, you're now starting to really repay your loan.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Added Benefits Which Include a Lower Interest Rate</h1>
                                       <p align="justify">Having a lower rate of interest, you should begin paying off your main earlier because in the event you've got a lower rate of interest, there is less attention to repay. You don't need to wait till the market has reduced rates of interest so as to refinance and benefit from a lower rate of interest. You might also simply work in your credit rating and wait patiently for it to improve. With a higher credit rating compared to the one which you already needed if you signed your mortgage, then you can be eligible for a greater rate than the one that you presently possess. Though lowering the rate of interest might get you a lower monthly mortgage payment, then you might even decrease your monthly payment by refinancing into another mortgage.</p>
                                        <img class="" src="{{asset('img/OC1OZ70.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">You Wish To Decrease Your Monthly Payment Number</h1>
                                       <p align="justify">When lowering your monthly mortgage repayment is of the highest priority, the most certain way of doing this would be to refinance your existing loan for another loan, or refinance it for a period longer than that which is left on your mortgage. Perhaps you want to have disposable income or much more breathing space to your finances every month, or perhaps you need to increase your quality of life and have more money each month to experience new things and travel.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Refinancing Into a Longer Term Mortgage</h1>
                                       <p align="justify">There are a large number of reasons as to why folks wish to reduce their monthly payment amount and at times calling and awaiting the prices to fall might look like an eternity, that may make it seem as though the remedy is in your grasp, yet so far off. If you're at present at a 15 year fixed rate mortgage, and you choose to refinance into a 30 year fixed, you'll get a reduced monthly payment. In the same way, if you're at a 30 year fixed rate mortgage and you've been making payments for a decade, by assessing your present mortgage back to some 30 year fixed, you may now have reduced monthly payments as your remaining balance is currently divided by a greater amount, which finally yields a reduce amount.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Pay Off Your Home ASAP</h1>
                                       <p align="justify">When repaying your mortgage and getting out of debt as rapidly as you can is your primary reason for refinancing, then you may also accomplish this by refinancing into a mortgage with a shorter duration. In case you've recently found yourself eventually getting that advertising and earning more money than you did in the time you signed your mortgage and could afford a higher monthly payment, then refinancing to your 15 year fixed rate mortgage is a excellent idea.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Refinancing Into a Shorter Term Mortgage</h1>
                                       <p align="justify">By refinancing to a 15 year fixed, you aren't merely likely to end paying off your house in just 15 years as you'll be paying off the principal in a significantly quicker rate, but since you're drastically shortening the duration of your loan you might also be receiving a lower rate of interest. By also obtaining a lower rate of interest, you're saving money overall throughout the life span of your loan by paying less money in interest, therefore although the monthly payments are somewhat higher, the entire amount which you paid will be. Even though the monthly payments of a 15 year fixed are much greater compared to the monthly payments of a 30 year fixed, you'll have completely paid off your house in only 15 decades and may currently pocket the money which you would have been paying in mortgage payments for another 15 decades.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Refinancing For Stability</h1>
                                       <p align="justify">While most homeowners refinance their mortgages so as to benefit from a lower rate of interest or to reduce their monthly payment or decrease it to some shorter duration, you will find homeowners that can do the reverse. Homeowners with a flexible rate mortgage (ARM) have refinanced to a fixed rate mortgage and ended up having a higher rate of interest and a higher monthly fee, but obtained a feeling of security. An ARM is a hybrid significance during the first period of 5 years that the rate of interest is fixed, but when the initial phase is over, the rate of interest will adjust once a year in line with the marketplace.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Refinancing For a Higher Interest Rate</h1>
                                       <p align="justify">During the first period, the monthly payment is generally lower than the monthly payment of a fixed rate mortgage, but when the first phase is over and the rate of interest adjusts, which could change. There are techniques to get prepared for the shift and also to compute it via a banking pro or on your own, but it needs a little study on your part if that is the path that you would like to take. By refinancing with an ARM into a 30 year fixed, you'll almost certainly receive a slightly higher rate of interest than the rate of interest you obtained during the first interval of an ARM along with the monthly payments will probably be marginally higher, but you won't need to think about your monthly obligations changing. A fixed rate mortgage means the rate of interest will stay the same during the whole duration of the loan, so monthly that your monthly payment will be the same. It's an investment which yields the benefit of equilibrium and relief from uncertainty related to not understanding how much your monthly payment will price.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Cash Out Refinance</h1>
                                       <p align="justify">Another manner refinancing could be regarded as an investment is via a cash out refinance. Having a cash out refinance, you refinance your present loan for a period more than what you owe then you are going to choose the difference in money. With this huge amount of money, it's advised that it's to be utilized as an investment for a long-term purpose. Examples include remodeling your home so the home worth is raised and paying off credit card debt to improve your credit rating. You might even use it as a deposit for another home to rent out to other people and have a different stream of revenue.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Be Mindful With Your Cash</h1>
                                       <p align="justify">Finally it's up to the homeowner just how the cash is invested, but since the amount of the money is included in your mortgage obligations you're paying interest on it consequently, it isn't free. Going on a shopping spree or choosing a lavish vacation is not the smartest approach to generate use of those funds. Before you take a cash out refinance, you wish to ensure there's a particular purpose for this and the final result will financially help you in the long run.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Nothing Is Set In Stone</h1>
                                       <p align="justify">In the time of registering your mortgage, then you may have had another set of priorities and the conditions have changed. The excellent news is that you aren't chained and anticipated to stick with your mortgage for the rest of its duration. Together with the choice to refinance homeowners have the opportunity to correct their existing mortgages to reflect on their present point in life and also the changes which came with it. If you're now pleased with your current mortgage, then you're more than welcome to keep on paying off it. Just know that you're ready to tweak it to meet your requirements. Whether your purpose is to repay your house as quickly as possiblereduce your monthly payment, then invest in yourself, or possess a feeling of safety, refinancing your mortgage could help you optimize your wellbeing.</p>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="col-md-4">
                             @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 
@endsection