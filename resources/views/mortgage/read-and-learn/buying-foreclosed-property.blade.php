@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">Buying Foreclosed Property</h1>
                                       
                                        <br>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Why Purchase Foreclosed Property?</h1>
                                       	<p align="justify">property differs since they're possessed by the lender. As stated earlier, buying foreclosed home generally means that you are getting a killer deal. They're generally sold fairly low under market value and you can most likely get more square footage per buck when you're purchasing a foreclosed home. Normally foreclosed houses are bought for "flipping", so purchasing a house at wholesale price and renovating the house to sell for profit.</p>
                                        <p align="justify">Because they're bank-owned and more affordable, these houses require adjusting and maintenance since they've been failed, but with all the renovations that the entire price of the house could be more economical than that which is available on the marketplace.</p>
                                        <img class="" src="{{asset('img/foreclosure_house.jpg')}}"> 
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Buying Foreclosed Property</h1>
                                        <p align="justify">Purchasing foreclosed property may be an attractive alternative for some because if you hear the term "foreclosed", you automatically presume that the real estate value is more economical. That's extremely nicely true as most foreclosures occur, on account of the homeowner being not able to keep their monthly obligations. The most popular example of this is that the home crisis of 2007-2011 if the mortgage sector dropped and several homeowners in America became submerged on their mortgage.</p>
                                        <p align="justify">When individuals were put off and no longer making cash, they could not afford their mortgage obligations. Feeling hopeless, homeowners chose to foreclose on their house and walk off and begin. When these kinds of properties are less costly than homes which are available on the current market, they do require the extra effort to buy.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Foreclosed Property Warning</h1>
                                        <p align="justify">There are a number of things to remember in regards to purchasing foreclosed houses. More frequently than not, the house will not be in the very best dwelling state. It may be more "inhabitable", which means you may technically reside there however there will probably be some harms and it will not be cozy. Occasionally these harms are intentional and willful, brought on by the last owner from frustration of losing their dwelling. There is also the possibility that these destructions were brought on from the house being made to be vandalized by other people.</p>
                                        <p align="justify">Considering that the prior homeowners were not able to afford their mortgage payments, you can bet they were not able to keep the home also. It is important to perform a thorough house inspection assessing for even the simplest things like termite damage, broken sockets, busted garbage disposal, etc.. There will probably be water damage because the house was failed. Check for mold growth everywhere or burst plumbing or roof leaks because the home probably has not been unlocked for weeks, and many can happen in a few months.</p>
                                        <p align="justify">In addition, it is important to remember that foreclosure is not something which happens unexpectedly. After being unable to cover their mortgages to get an elongated quantity of time, a few homeowners are not granted the luxury of a notice of flooding. There have been instances where the homeowners are locked out of the property before they have had the opportunity to pack their possessions. It's also highly likely that the homeowners only determined they did not wish to take care of it since transferring costs could be an extra cost they can not manage. Should you just happen to get a great deal of pre-owned things in your residence, they've become your entire duty. The lender technically possessed it, but once you purchased the house, everything in it also becomes your home.</p>
                                        <img class="" src="{{asset('img/OIHR970.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">The Buying Process of Foreclosed Homes</h1>
                                        <p align="justify">Purchasing a foreclosed home raises a great deal of red flags for creditors. They understand that the house is in horrible shape and is in dire need of costly remodeling and maintenance. As a result of this, lenders are not very inclined to give you the money to get the property. These two determine the worth of your house and also the construction of the house and the lending institution will determine whether the home is worth committing cash for buy. Even without having an appraisal and a home inspection, the creditor understands the property value will not be very high and also the home inspection is going to have a lengthy laundry list of fixes which are going to be required that you even reside inside.</p>
                                        <p align="justify">That is not to mention that it is not possible to have financing for a home that has been foreclosed. It's possible, however it is not as simple. As a result of this barrier, many foreclosed houses are purchased in money using a cashiers' check. Having a money purchase that is paid in full, there's absolutely no danger since there is not a creditor in the image to loan you money, so there are no queries asked. If you do not have money up front to cover the house in total, it's likely to have financing, but there might be strict needs to decide on this.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Do Your Research</h1>
                                        <p align="justify">When considering purchasing a foreclosed house, it's essential that you find an experienced real estate agent that specializes in foreclosed properties. A foreclosure sale entails a whole lot of back and forth together with the lender and in addition, it needs a distinct contract, together with distinct state and local taxation laws. The foreclosure legislation are different in each state, so getting a realtor that's properly equipped to experience this journey is likely to make your life much simpler. Because most lenders are not prepared to fund a foreclosed house, the ideal method to start this is to receive a mortgage from the bank that owns it.</p>
                                        <img class="" src="{{asset('img/')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Buying Your Home At An Auction</h1>
                                        <p align="justify">A favorite way to purchase foreclosed houses is via an auction. This auction can be held in person or on the internet and it is full of individuals that are thinking about purchasing houses for flipping. Because most people that are at these auctions are professionals, so it is ideal to do your research and come ready.</p>
                                        <p align="justify">Physical auctions are often held in local authorities courthouses, hotel conference rooms, or some other location that's determined by the auction company. These auctions are usually held by bank-hired trustees. There are cases where an open house is held to your house and the list is made public with a description of their house. While open houses are potential, the majority of the time you will not receive that luxury.</p>
                                        <p align="justify">You won't have a crystal clear idea of the house before you purchase it and watch it on your own and there were cases where the land was in considerably worse shape than they'd expected. Buying foreclosed properties in an auction may be a major gamble and it is your choice to determine whether that's a risk you are prepared to take. If you are purchasing through an internet auction, then odds are you'll just be extended a restricted number of images to browse and there is a high likelihood that you are visiting the house in the very best light with the very best angles and attributes.</p>
                                        <p align="justify">Because this is an auction, so it is best advised that you come ready with a cashier's check prepared for the entire amount you are prepared to pay. This ought to consist of auction fees as well as the amount you're prepared to buy the house for since the majority of the folks you'll be competing with will be probably real estate professionals that can afford to cover the house completely in money. If you do not have that much money available, you will find auctions which allow for funding. In this circumstance, it is ideal to find pre-approved before showing up. If that is true, with a mortgage calculator to get a sense of the rates of interest from competing lenders can help. You will not be walking to the scenario blind and you may be in a position to get some leverage.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Overall Thoughts</h1>
                                        <p align="justify">Purchasing a foreclosed home might be a fantastic bargain, but it is important to remember the total amount of money that is likely to enter repairs. Repairs to the house can be unbelievably costly, so make sure you account for this when budgeting. It is important to also know yourself as a individual to find out when this is something worth going through.</p>
                                        <p align="justify">If you're the kind of person who enjoys home jobs and does not mind having hands on experience with all the repairs, then you're a wonderful candidate. If you already understand which you're going to need to hire a company to do everything for you, however do not mind since you will have the ability to personalize it and have your dream house, that is also terrific. Just know you'll have bought your house for exactly the identical amount as though you were buying a house on the market. If you're seeking a fantastic bargain, but are not seeking to lower costs by doing jobs yourself, I'd advise against purchasing foreclosed home.</p>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="col-md-4">
                             @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection