@extends('layout.app')

@section('main-content')
<main>
<!-- Start Blog -->
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">Should I See My Bank or Go With an Online Lender?</h1>
                                       
                                        <br>
                                       
                                       <p align="justify">If it is time for you to obtain a house or re finance your mortgage, then it's difficult to determine ways exactly to start. Does one really go into your financial institution? Does one get on the internet? You will find lots of sources on the web, however there's something reassuring about conversing with some body which we may physically watch versus speaking about an agent onto your device or on the web. After moving right on through daily life changing functions like having the very first mortgage our very first tendency will be always to accomplish a little bit of exploration. It's by far the most typical to inquire individuals near you exactly what the process has been similar to this you've got confirmation which you're about this that the "appropriate" manner.</p>

                                        
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">You Don’t Always Have To Listen To Your Parents</h1>
                                       <p align="justify">You may possibly consult your parents or intimate friends to their information, however only as they moved on their procedures in a particular method, it doesn't to suggest you need to check out their own footsteps. In the event you really don't desire to entail your comments along with your own family members or good friends, you will possibly surf the net to assemble your own personal info, or even begin on looking around to get a house loan directly then and there! The net has altered the planet also it's usually stated we're able to find such a thing about the world wide web now (even adore!) , also today with technological advances happening on this a normal foundation, the world wide web is presently changing how folks are looking for domiciles.</p>
                                        
                      
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Times Are Changing</h1>
                                       <p align="justify">The net is actually a rather new merchandise should you truly consider doing it. It wasn't till 1991 the "web" was launched into the overall community, and it is significantly less than thirty decades back! Ever since that time, we've made tremendous strides graduating from dial tone world-wide-web to today using elevated speed net on our mobiles and also online planes! Maybe not merely are we still in a position to inspect e mail on our mobiles, however we still are now able to inquire regarding assessing domiciles and possess some body send the exact advice that we desire having a fast telephone call following filling in a poll that just takes just 3 minutes outside of daily. It had been simply an issue of time prior to the notion of owning a digital store that's available twenty four hours every day and 7days per week, available to anybody that has world wide web, readily reachable with all the tap their palms came into fruition. As a result of the brand new solution, just about every customer's program is guaranteed and place original and also the lack of locating the opportunity to visit the lender is not any longer a situation.</p>
                                        <img class="" src="{{asset('img/1.jpg')}}">
                                        
                                      
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">It Takes Less Effort</h1>
                                       <p align="justify">You can find several websites which may aid you in your search and better still, you will find a number of which can perform all of the search for you personally whenever you want to have yourself a home finance loan. With the majority of banks using the customary company working hrs of 9 a.m. -- 6 pm, it's quite hard for somebody who functions a standard occupation job to visit the financial institution and have all those questions it's possible they have. They'd need to program their appointment throughout their lunch hour plus they'd need to return at work at that time that the hour will be completed, and that will not abandon them with the full time for you to essentially receive the info required to produce this kind of essential choice. Banks today have ATM devices operating 2-4 hours aday and internet phone test processing therefore their customers can follow their own tests in their leisure and advantage, even in the event the actual division of their lender remains shut.</p>

                                        
                                      
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">You Can “Window Shop” Without Leaving Your Seat</h1>
                                       <p align="justify">These lending sites that connect you with other lenders is the equivalent of mobile check deposits in the mortgage industry. Not only are these websites in operation 24 hours a day/7 days a week, but they will also take the information that you provided and match you with multiple lenders who want nothing more than to help you. By getting matched with multiple lenders, you have the ability to compare your different options and pick the one which works best for you. How much more convenient can it get? When it comes to shopping, the worst feeling, aside from your coveted item being sold out everywhere, is when someone who has the same item tells you that you overpaid for it. You initially feel upset at the retailer for marking up the price, but the truth is that you are upset at yourself because you failed to put in the little bit of extra effort to shop around and find it for the better price. This “shopping around” concept should be applied when you go shopping for your mortgage. You want to make sure that you are getting the best deal that you can instead of thinking that the first offer that is presented to you is the best option.</p>

                                        
                                        <img class="" src="{{asset('img/spoilt-for-choice.jpg')}}">

                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">It Is Way More Convenient</h1>
                                           <p align="justify">When researching the alternatives using various creditors, most the is sometimes performed on line once you truly feel as doing this. You may even consult with an expert in your device requesting your queries from more detail as an alternative of being required to hurry into some bank throughout your lunch hour or following job and trusting they'll not close until you arrive. As an alternative of being required to produce numerous appointments and push around the city into the numerous banking institutions to determine the things they are able to supply you with, you're able to have all of your diverse options onto your own personal computer monitor without so much as turning to your own car or truck ignition</p>
                                            
                                        
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">You Are Their Priority</h1>

                                      <p align="justify">One other amazing benefit of searching for house loans on the web is these creditors are totally ready for that inundation of internet queries they get over an everyday basis. In a tangible financial spot, you can find just a couple of bankers offered by just about every branch who are prepared to aid you personally and also after that, it's a chance perhaps not most these are mortgage officials, that could produce the access more gloomy. Understandably thus, there's a feeling of relaxation related to speaking with a real man concerning this type of delicate issue such as financing. It seems protected. If it boils right from that which might be the most significant purchase of your own life, you ought to create certain you completely know what and you never lose from some essential particulars. Inadvertently gaining the incorrect product as you misheard some thing in your telephone appears to be a nightmare which you simply wouldn't want on your worst enemy.</p>
                                       
                            
                                    </div>
                                </article>




                                  <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">The Customer Service Is Just As Good</h1>
                                       <p align="justify">Luckily, all these really are queries which were discovered and are increasingly being tackled. You'll find businesses like Quicken Loans who've been given a myriad of awards to its higher portion of the past ten years about their own outstanding client care plus they've won a number of awards for home finance loan servicing. Also on the web, it is possible to question your creditor any inquiries you might have about your house loan. Despite all these incredible added benefits, there's nonetheless the lingering dread related to offering your private and personal info to some site which likewise has to be dealt with. While that can be a valid panic and problem, it's not any different than performing this on line versus contributing into a own loan in a financial institution. After you devote the mortgage your social security number, your bank bills, and also your credit history as well as one additional records which are needed so as to get the financial loan, the bank loan will automatically enter your info in their inner database, and that can probably be on line. The sole significant distinction will be that you're bodily viewing them perform it uploading it on yourself.</p>
                                       
                                    </div>
                                </article>




                                
                             
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Online Security</h1>
                                       <p align="justify">Other on-line creditors will probably have precisely the exact same system using a similarly safe security-system by way of encryption, and it is essentially a digital safe and sound to accommodate most one's own personal information which isn't going to be distributed to anybody. These on-line lending businesses are totally conscious how crucial each of one's private details is and just how essential it's to be sure it stays safeand so they really do their best to be sure the protection of one's individuality as of their wellbeing and standing rely upon it.</p>
                                       
                                        
                                    </div>
                                </article>


                                 <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Online Can Be Better</h1>
                                       <p align="justify">While looking for mortgage loans on the internet continues to be a theory that's unknown for most, it's constantly climbing. Rocket mortgage loan from Quicken Loans can be just a item which lets end users save to get their own mortgages with their own cellular apparatus also it was quickly identified a big proportion of the users ended up time homebuyers vs. homeowners which were within a existent mortgage seeking to refinance right into some thing else. Individuals are currently applying their cellular phones to create the most significant purchase in their own lives, and it is evidence which the thing that had been believed "ordinary" is shifting in a speedy tempo. The web caused lots of change also shift there's no uncertainty, however we are not able to deny it's entire changed the society to the higher plus it really is as a result of this web we've got the other option about what steps to take to best to receive yourself a mortgage as well as this traditional bank trip. Visiting the lender to receive a home loan has ever become a reliable system for decades and although it's nevertheless chosen by most, it's likewise fully great to select the trail less traveled long since it functions for you personally.</p>
                                       
                                        
                                    </div>
                                </article>
                               
                                                               
                                <!-- End single item -->
                            </div>
                        </div>
                        <div class="col-md-4">
                        @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 
@endsection