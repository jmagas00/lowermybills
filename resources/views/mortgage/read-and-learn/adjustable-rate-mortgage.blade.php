@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">What Is An Adjustable Rate Mortgage?</h1>
                                       
                                        <br>
                                       <p align="justify">A Adjustable Rate Mortgage (ARM) is a kind of mortgage that is advantageous for homeowners whose chief purpose is to get a reduced monthly payment. Even though 30 Year fixed rate mortgages currently supply the benefit of a reduced monthly fee, an ARM can provide a homeowner a marginally lower monthly repayment amount. It seems like a no brainer, right? Well, this reduced monthly payment has a grab as many good things.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">The Basics of an ARM</h1>
                                       <p align="justify">An ARM may provide you a very low interest rate that's fixed for a particular timeframe. This is known as the "first period" and can be set by the amounts on your mortgage. The initial number will explain to you just how long the first interval is and the next number will explain to you just how often the rate of interest will adjust. 5/1 ARMs will be definitely the most popular of this ARMs and this usually means your first interval will last you 5 decades. After the first period is finished, your rate of interest will then adapt to whatever the industry is at the moment annually.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">THIS IS IMPORTANT!</h1>
                                       <p align="justify">There's no set formula for your next amount because your rate of interest could fix after a year, after every five decades, or once each six months. It truly is dependent upon the sort of that you wind up getting out of the creditor. You don't wish to presume that you're walking off with a reduced monthly payment and your rate of interest will adjust every six decades. You might easily misunderstand and it truly means the rate of interest will adjust every six months. It's vital that you're on precisely the exact same page as your creditor and you understand the complete conditions of your loan.</p>
                                        <img class="" src="{{asset('img/288761-P6NYP6-236.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">The Benefits of an ARM</h1>
                                       <p align="justify">One of the best advantages of an ARM might be the reason people wouldn't want one: that the rate of interest is only fixed for a limited timeframe. Many homeowners move into purchasing their first house with the mindset they'll continue to keep the house indefinitely, but the truth is that many homeowners' first house is typically a starter house and they'll move onto a larger house after many decades. You may either remain in your present mortgage and allow the interest rate adapt since you have probably increased your wages on time and can afford a higher monthly payment. It is also possible to opt to sell your house and repay the remaining part of the loan prior to the first period is finished and proceed to a larger house. If you would like to remain in your existing residence, but don't wish to manage a payment which affects every so often, you may even refinance into another mortgage that provides a fixed rate of interest.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Great Candidates for an ARM</h1>
                                       <p align="justify">While an ARM does Not Match everyone's lifestyles, There Are Particular Kinds of people that could greatly benefit from You:</p>
                                        <ol>
                                            <li>Army Households</li>
                                            <li>Professional athletes</li>
                                            <li>Workers of Big airline Businesses</li>
                                        </ol>
                                    </div>
                                </article>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title"><h1>"What's the Purpose of Purchasing a House if I'm Not Going to Be Living There for This Long?"</h1>
                                       <p align="justify">When you've always wished to have the advantage of owning property, but never believed you could since you were not able to commit, an ARM is a superb alternative for you. As stated earlier, the very best aspect of an ARM is that the fixed low interest being in effect for a limited timeframe. It is possible to benefit from the being a property owner and paying for the cheapest monthly payment amount. As you're already in the pattern of moving, you may already have moved on your following place by now your primary time has come to a conclusion. This usually means there's not any chance for your interest to correct and there is actually no "catch" into an ARM for you.</p>
                                        <img class="" src="{{asset('img/O6PF3M0.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Are ARMs Secure Today?</h1>
                                       <p align="justify">ARMs are still basically the exact same solution, but with applied stricter regulations they're much safer than previously. ARMs acquired a bad reputation throughout the housing crisis in 2007-2011 because most homeowners had no option except to have a brief sale on their house or foreclose on their land entirely. This is only because people were lying software about their earnings and creditors had been giving out loans which homeowners were not able to manage. Along with this, lenders were giving out ARMs since the homeowner only needed a reduced monthly repayment, not realizing that following the first period was over the rate of interest would then adapt to all of the market was in the moment. Each one these activities combined caused businesses to have massive growth and so a lot of individuals sadly lost their tasks. They were no longer getting cash and they had been not able to cover their new monthly payment amount, which had been significantly more than that which they were paying before. After dwelling values dropped, homeowners found themselves "submerged" or "upside down" in their mortgages, which means they owed over the house was worth. Feeling down on their luck, homeowners chose to leave their land into the banks. Lenders are now a lot more transparent with their clientele and the whole business is now carefully tracked so we could not have this happen again.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Overall Thoughts</h1>
                                       <p align="justify">ARMs are a fantastic product for those that find themselves needing to move around frequently. This might be caused by their livelihood or perhaps they only wish to live all around the world. Possessing a loan that best matches your lifestyle will help you the most. By using this temporary fixed interest and also the very low cost payment which accompanies it, you are getting the best deal for your property. You do not just have to have property and all of the advantages that come with this. If you found yourself registering to an ARM and afterwards realized it isn't exactly the best match for you, you aren't required to remain in your loan until you've paid off it. It is possible to refinance into another mortgage which better suits you.</p>
                                    </div>
                                </article>                                
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mu-blog-sidebar">
                                <!-- start Single Widget -->
                                <div class="mu-sidebar-widget">
                                    <h2 class="mu-sidebar-widget-title">Other Types of Mortgages</h2>
                                    <ul class="mu-sidebar-widget-nav">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Veteran Loans</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>FHA Loans</a>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>30 Year Fixed Rate Mortgage</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>What is the Difference Between a Fixed Rate Mortgage and an Adjustable Rate Mortgage?</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Cash Out Refinancing</a></li>
                                    </ul>
                                    
                                </div>
                                <div class="mu-sidebar-widget">
                                    <h2 class="mu-sidebar-widget-title">Mortgage Basics and How to Guides</h2>
                                    <ul class="mu-sidebar-widget-nav">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>What To Expect During the Mortgage Process</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>The Hidden Costs That Come With a Mortgage)</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>What Questions Should You Ask Your Lender?</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Why Should I Refinance My Home?</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>So You've Decided To Buy A Home, What Happens Now?</a></li>
                                    </ul>
                                </div>
                                <div class="mu-sidebar-widget">
                                    <h2 class="mu-sidebar-widget-title">Links To Other Tools</h2>
                                    <ul class="mu-sidebar-widget-nav">
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/calculator.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Calculator</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/money.svg')}}" alt="" style="max-width:15%;"> &nbsp; Refinance Calculator</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/comparision.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Comparison</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/analysis.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Analysis</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/bulb.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Advisor</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 
@endsection