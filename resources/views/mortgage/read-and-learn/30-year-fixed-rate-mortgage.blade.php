@extends('layout.app')

@section('main-content')
<main>
<!-- Start Blog -->
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">30 Year Fixed Rate Mortgage</h1>
                                     
                                        </ul>
                                        <br>
                                       
                                       <p align="justify">It looks such as that a 30 yr fixed home loan would be your gold typical of house loans despite the fact that you'll find various sorts of home loans out there there to satisfy every one's different requirements and choices. Monetarily speaking, it may be the optimal/optimally alternative for very first time buyers on account of the reduced month-to-month payments and also the adjusted rate of interest, however using many diverse alternatives on the market will you're convinced may be the correct merchandise for you personally?</p>
                                        
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Why Is The 30-Year The Most Popular Choice?</h1>
                                       <p align="justify">Let us focus on the basic principles. Having a thirty year mended you are in possession of a set rate of interest and also you might have thirty years to pay your mortgage off, and that will be both largest benefits which arrive with such a house loan. When taking into consideration the pros and pitfalls which arrive with a thirty yr fixed mortgage, then the more pitfalls undoubtedly outweigh the experts. The truth is that having a thirty yr fixed home loan are just two big experts: you have to get a really good low month-to-month payment and also the rate of interest is mended. Though a thirty year mended really just provides your homebuyer a lesser month-to-month fee, it's but one among the absolute most significant matters to think about when buying a household.</p>
                      
                                                             
                                        <img class="" src="{{asset('img/30year_infographic-01-1.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Pros and Cons of a 15 Year Fixed</h1>
                                       <p align="justify">Pros:</p>
                                            <ul>    
                                                <li>Lower monthly paymen Cons:</li>
                                                
                                            </ul>
                                            <br>
                                       <p align="justify">Cons:</p>
                                            <ul>
                                                <li>Higher interest rate</li>
                                                <li>In debt for 15 more years</li>
                                                <li>Higher amount to pay back</li>
                                            </ul>

                                    
                                      
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What Is The Difference Between a 15 Year and 30 Year Fixed?</h1>
                                       <p align="justify">Amongst a 15 and a 30 yr set, there's really a very big difference between them both. Having a 15 yr mended, your house buyer will probably find a much decrease rate of interest that means a reduce price to pay for in and back finish, your residence has been paid down at mere 1-5 decades. The drawback however, is there's a greater fee. Having a thirty year set, the circumstance would be the specific contrary. The debtor will probably find a high interest that means that they are going to get a greater add up to repay, plus they'll likewise be with charge to get an extra 1-5 years compared to these had to possess chosen to get a 1-5 year mended.</p>
                                        
                                        <img class="" src="{{asset('img/018 investing when interest rates are low_main image_Original.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Why Is Interest Rate Is So Important?</h1>
                                       <p align="justify">To really make the theory of interest-rates more straightforward to know, let us mention the total borrowed is200,000. Let us also pretend the rate of interest to get a 15 yr fixed home loan will be 3 percent and also the rate of interest to get a thirty yr fixed home loan will be 5 percent. For A15 calendar year, the average fee is currently6,000 and attracts that the overall balance due to £206,000. £206,000 split in to 1-5 decades of obligations attracts that the month-to-month payment for £1,144.45. Presently, to get A30 year that the 5 percent fee attracts the entire sum owed to £210,000 along with the month-to-month mortgage cost is currently determined to just £583,34. As the month-to-month repayment amount to get a thirty yr set is significantly substantially lesser than the 15 yr mended, complete you are going to probably be paying to get a thirty yr mortgage as you're going to be earning those installments to get double so muchas While most the info will seem excruciating, you've got to contemplate carefully your wellbeing and the way it's going to soon be impacted. Everyone else's situation differs and probably using a 1-5 yr fixed isn't excellent even in the event the debtor has the capability to pay for a greater monthly charge. Possessing a thirty year adjusted provides debtor an low month-to-month repayment, which likewise provides debtor greater financial living space monthly and also the chance to invest some funds now in the place of setting extra money following the home loan was paid in full.</p>

                                        

                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Who Is The Perfect Candidate For a 30 Year Fixed?</h1>
                                       <p align="justify">The first time buyers really are a amazing candidate to get a thirty yr mended on account of the month-to-month fee. Moving to a fresh life style in which you've got a better feeling of economic responsibility usually means you want to get ready for every one the challenges which include this. Possessing your house usually means that you're accountable for many of the renovation the repairs, and house owners affiliation costs (should you get an apartment) along with the upkeep which mechanically will come together with this. You aren't likely to possess home manager who may manage these matters for you anymore, so therefore it's preferred to invest some funds aside therefore you are going to undoubtedly be financially ready to care for any openings or accidents which can come on the manner. Possessing a minimal month-to-month payment may help you organize to some events which would take your prompt care and also your own wallet.</p>
                                        
                                        

                                        
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Additional Benefits Of a 30 Year Fixed</h1>

                                       <p align="justify">Another great perk that comes with having a 30 year fixed is the fixed interest rate, meaning that the interest rate stays the same throughout the entire duration of your loan. There are Adjustable Rate Mortgages (ARMs) that can offer you a lower monthly payment amount, but that offer only stands for a specific amount of years called the “initial period” and only lasts a handful of years. Once the initial period is over, the interest rate will adjust once a year and match itself to the market. With a fixed interest rate for 30 years, you know exactly what your monthly mortgage payment amount will be from day one. You do not have to worry about the payment amount fluctuating and getting hit with a “surprise” that you were not prepared for. The interest you pay on your mortgage is also tax deductible, so with a 30 year fixed you can write off a larger dollar amount since the interest associated with a 30 year is more than other mortgages. You are, however, limited to deducting on your first $500 thousand in debt. While it is a deduction and not a tax credit, meaning that if you pay $10,000 in interest you won’t be getting $10,000 deducted from how much you owe, you can significantly reduce your taxable income. If you are in a 25 percent tax bracket, you will instead reduce your taxes by $2,500 because 25 percent of $10,000 is $2,500.</p>
                            
                                    </div>
                                </article>




                                  <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What If I No Longer Want To Be In a 30 Year Fixed?</h1>
                                       <p align="justify">If you feel that you want get out of your 30 Year because you want to pay less in interest and save money on the overall cost of your loan, you are more than welcome to refinance into a different mortgage that could be the new best option for you. As time goes by, circumstances change and maybe you earned a promotion and a raise that can afford a higher monthly payment while enjoying life. It is definitely possible that a 30 year mortgage is no longer your best financial option and the best part is that you aren’t obligated to stick by your mortgage until the end of its term. If you have increased your income, but are hesitant to jump the gun and commit to refinancing, you can also stay in your current mortgage and increase what you pay monthly. Like a credit card, your monthly mortgage payment is a minimum payment that is due in order to pay off your mortgage in exactly 30 years. You can opt to pay more each month without having to go through the process of refinancing into a different type of loan and pay off your loan at a faster pace. You can even make double payments if you want to pay what is due each month and contribute a little more if you find that you have some disposable income that month.</p>

                                       
                                    </div>
                                </article>




                                
                             
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Overall Thoughts</h1>
                                       <p align="justify">If you sit down and think about it, being able to claim all of those benefits with a 15 year fixed possibly means that every spare dime you have will go towards your monthly mortgage payment. You might have to cut down on socializing and grabbing dinner with your friends and there is a chance that you might not be able to take a vacation for a long time. If this is something that you do not want for yourself, then a 30 year fixed mortgage is the right choice for you. While some people are ambitious and are focused on paying off debt as quickly as they humanly can, it is not the right choice for everybody, even if on paper it is the right choice. As life goes on and you increase your earning potential and eventually increase your income, you can switch to a different type of mortgage that’s better suited to help you reach your financial goals or you can stay in your current mortgage and increase your monthly payment amount. In the end, you are the one to decide the terms of your mortgage and your life so choose what fits you and your lifestyle the best.</p>
                                       

                                        
                                    </div>
                                </article>
                               
                                                               
                                <!-- End single item -->
                            </div>
                        </div>
                        <div class="col-md-4">
                        @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 
@endsection