@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">Home Pairing & The Hazards That Include It</h1>
                                        
                                        <br>
                                       <p align="justify">In this era, owning a house has much more caution on account of the housing crisis which occurred in 2005-2010. Ever since that time, the whole housing sector is far more rigorous and the procedure to acquire a mortgage is a whole lot more regulated since the entire fiasco occurred because of people not being cautious. Consequently, it's that much more challenging for another generation to become home owners. In the following guide, we'll discuss just how one finds themselves in this position along with also the very best choices awaiting.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">You May Lose Money On a House</h1>
                                       <p align="justify">While America has been in the practice of re-building itself, the costs of houses have steadily improved since then. Regrettably, there are still a great deal of homeowners who weren't as lucky and have not managed to bounce back. They're considered "submerged" in their mortgages, which means they owe more than that which their homes are worth. Sadly, this is a tough fact to face and it's a situation that lots of homeowners are dealing with now.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">How Does One Become Underwater On Their Mortgage?</h1>
                                       <p align="justify">Homeowners never mean to be submerged within their mortgages. This occurs to homeowners once the cost of your house drops after you get it. This scenario is very similar to purchasing a new vehicle. Also much like a new car that is driven off the lot, if you wait long enough and your automobile is regarded as a "classic" the value will appear. With homes, the market will change significance the value of houses will go up and down through recent years. While getting submerged is something which people most certainly wish to stop, there is in factn't a means to forecast the future and make certain that this doesn't occur for you. The only way that you need to know is if you're very involved with the mortgage market. While purchasing a house and moving during the mortgage process, an appraisal of the desirable property is demanded. An appraiser is somebody who takes a peek at the area and the houses nearby and then decides how much the house is worth according to the accumulated information.</p>
                                        <img class="" src="{{asset('img/ArrowBreak_LMB_532031914.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">IMPORTANT!</h1>
                                       <p align="justify">One thing which you want to remember throughout the appraisal procedure is the appraiser will provide you the value of your house in this instant. They won't inform you if your house value will rise over time or when the value will collapse. If you end up underwater several years later on, it's not the appraiser's fault. They, just like you, can not see in the future or understand why home costs are high. When they have been giving such high home worth before the housing bubble burst, it's because that is what homes were really going for at the moment. The main reason housing values were high was actually because individuals who were getting house loans couldn't manage them. In reality, since appraisers inflated the house values throughout the housing bubble, this procedure is now strictly controlled. Because of this, appraisers are currently committing more conservative home worth and it's all up to you whether you would like to proceed with the house purchase.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">What Options Do I Have If I Am Underwater?</h1>
                                       <p align="justify">The ideal choice is to maintain your ground and keep paying off your mortgage. Yes, it is not an perfect situation as you're paying more than that which your home is worth, however in the event that you're able to afford to cover the monthly obligations it's ideal to keep on doing this. If you consider it, you can not eliminate money in an underwater sale in case you don't market your property. Additionally, home values go down and up occasionally and following the housing bubble, America was working hard to re-build itself. Because of this, housing costs are going up and if you continue to make your payments, then it might be well worth it because your house could be rewarding once more. Ever since technically the definition of being submerged is paying to get a mortgage that is less than the house value, only waiting could be the simplest alternative. This option requires a fantastic deal of patience, but when it's something which's feasible for you personally, it might be best.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Home Affordable Refinance Program</h1>
                                       <p align="justify">When being individual is something which you wish to do, but you just don't have the money to keep this up, HARP is just another alternative that could greatly help you. HARP stands to your Home Affordable Refinance Program also it was made specifically for homeowners that are submerged in their mortgages on account of this housing crisis. HARP helps homeowners to refinance their loans so they can snag a lower rate of interest and really afford their monthly obligations and pay their loan off. If you have applied for HARP previously and got refused, don't have any fear. Just continue paying off your mortgage payments and other invoices and in many months, try again. Since launch in 2009, important improvements are made into the program so as to help more homeowners out. It currently has easier guidelines and does not need as much instruction as it did earlier so as to approve additional loans.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">In order to be eligible for HARP, you must meet these requirements:</h1>
                                        <ol>
                                            <li>You can not be late on any mortgage payments in the previous 6 months. In case you've been overdue, you may simply be late once inside in the previous 12 months.</li>
                                            <li>The House You're trying to refinance Should be your primary residence, a 1-unit next home, or a 1-4 unit investment Land</li>
                                            <li>Your loan Has to be Possessed by Freddie Mac or Fannie Mae (there are Readily Available Resources to Assess if you Are Unsure)</li>
                                            <li>Your loan Has Been originated Before or on May 31, 2009</li>
                                            <li>Your Present loan-to-value (LTV) Has to be Higher than 80 Percent</li>
                                        </ol>
                                       <p align="justify">You need to ensure you're obtaining a HARP loan by a creditor who's engaging in the HARP program. When using HARP is totally free, you will still must pay the closing costs which are related to refinancing as you're technically obtaining a new loan. If that is something which you're interested in, fill this form out and we could connect you to lenders who may help you to get a more HARP loan.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Rent Out The Property</h1>
                                       <p align="justify">Leasing out the house is a good alternative in almost any circumstance. This will permit you to maintain your house so that you don't really drop money in the long run. You simply eliminate money on your house when you foreclose on the house or sale. Leasing out the house or even the excess space that serves as "storage" will help considerably because they'd be pitching into the monthly mortgage payment. This will offer you the fiscal breathing space each month you desire.</p>
                                        <img class="" src="{{asset('img/262395-P54ISQ-503-01.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Overall Thoughts</h1>
                                       <p align="justify">Leasing out the house is a good alternative in almost any circumstance. This will permit you to maintain your house so that you don't really eliminate money in the long run. You simply eliminate money on your house when you foreclose on the house or sale. Leasing out the house or even the excess space that serves as "storage" will help considerably because they'd be pitching into the monthly mortgage payment. This will offer you the fiscal breathing space each month you desire.</p>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <div class="col-md-4">
                            @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog --> 
@endsection