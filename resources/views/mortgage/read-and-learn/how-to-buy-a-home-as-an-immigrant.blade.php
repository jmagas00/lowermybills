@extends('layout.app')

@section('main-content')
<main>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <article class="mu-blog-item">
                                    <!-- <a href="#"><img src="assets/images/blog-img-1.jpg" alt="blgo image"></a> -->
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title-head">How To Buy a Home as an Immigrant</h1>
                                       
                                        <br>
                                       <p align="justify">For most, owning a house is the version of "The American Dream." Having the ability to have property is something which many desire in their life so they've got something to pass on their children. A home is a financial asset and an investment which may assist many later on, but can it be feasible to extend those perks to individuals that are not US citizens? The solution is yes, people who don't hold US citizenship can buy a house.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Are Non US Citizens Able Obtain a Mortgage?</h1>
                                       <p align="justify">Purchasing a house entirely in money will help to alleviate any issues or barriers to be a homeowner, but for most that's simply not feasible. Funding a home by obtaining a mortgage has become the most realistic path so as to reach this. Happily, non US Citizens can acquire mortgages and have an opportunity to receive their own "American Dream."</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Permanent Residents and Non-Permanent Residents</h1>
                                       <p align="justify">Permanent residents are immigrants that are legally authorized to reside and work in the USA. Permanent residents are provided a card as evidence of the status known as a United States Permanent Resident Card, many commonly referred to as the green card, along with also a social security number. Green cards are valid for 10 years to get permanent inhabitants.</p>
                                       <p align="justify">Non-permanent residents will also be authorized to reside and work in the USA. They're provided a social security number and a valid work visa, however they're not allowed a green card. Although non-permanent residents aren't issued a green card, non-permanent residents continue to be able to acquire a mortgage.</p>
                                        <img class="" src="{{asset('img/820.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Obtaining a Mortgage as a Permanent Resident</h1>
                                       <p align="justify">Permanent Residents can acquire a mortgage exactly like any other US Citizen, even government backed loans like a FHA Loan (Federal Housing Administration). The hottest mortgage choices for permanent residents would be the FHA Loan and traditional home loans like the 30 or 15-Year fixed rate mortgages. To get a traditional loan, the procedure to have a mortgage is quite similar.</p>
                                       <p align="justify">To be able for a traditional loan, borrowers will have to supply their green card along with their social security number. The remainder is the exact same standard mortgage procedure that all borrowers undergo. When applicable, the debtor may need to offer evidence of income, proof of residency, employment background, and a credit history. This is simply to show the lender that you're in a position to afford paying for your loan and are not that much risk. Many US Citizens possess w2 forms and charge history, but in the event the debtor has not been residing in the nation for this long, there may not be adequate info.</p>
                                       <p align="justify">For FHA Loans, permanent residents must provide evidence of residency in the united states along with also a social security number as well as fulfilling all those FHA Loan eligibility conditions.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Obtaining a Mortgage as a Non-Permanent Resident</h1>
                                       <p align="justify">Non-Permanent Residents are issued a social security number, but don't have a green card. They're legally permitted to dwell in the US to operate with a legal work visa. As a non-permanent resident, the debtor needs to supply their visa. Along with their own work authorization, evidence of earnings, employment history, evidence of occupation, and a credit history are often needed to show the lender that they'll have the ability to earn the monthly payments.</p>
                                       <p align="justify">For FHA Loans, non-permanent residents can get accepted when the property is meant to function as creditors' permanent dwelling. They need to also supply a social security number along with a BCIS (that the Bureau pf Citizenship and Immigration Services) Employment Authorization Document.</p>
                                       <p align="justify">The debtor can also be anticipated to reside and work in the united states for at least 3 decades. If one's job visa is coming expiration, renewal is anticipated. Borrowers want at least a years' worth of credit history to be able to acquire a credit rating, therefore it's ideal to wait for a year before applying for a mortgage. In the event the borrower doesn't have any lines of credit to create a credit report, the creditor can request a unconventional credit report from different accounts like a mobile phone charge, TV invoice, water or electrical bills, etc.. An account background for 2-3 accounts is necessary so as to yield a credit report and credit rating.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Are Undocumented Immigrants Able to Find a Mortgage?</h1>
                                       <p align="justify">Most undocumented immigrants cover their houses in full and in money because this eliminates lots of the hurdles which they could face, but it's likely for undocumented immigrants to have a mortgage. There's a kind of mortgage which many don't understand about called the ITIN Mortgage.</p>
                                        <img class="" src="{{asset('img/account-achievement-bank-870902.jpg')}}">
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Personal Tax Identification Number (ITIN) Mortgages</h1>
                                       <p align="justify">ITIN Mortgages are made to aid individuals that aren't eligible to be given a social security number. ITIN stands for Personal Tax Identification Number and is a tax processing number. Undocumented immigrants together with individual tax ID numbers cover taxes just like everybody else and use their citizen ID number as evidence. When they were to confront a deportation hearing, obtaining a record of taxation may benefit them because they're abiding by the law. While ITIN mortgages have been initially made for overseas nationals who had resources in the USA, undocumented immigrants that live in the united states have been homeowners through the support of an ITIN mortgage.</p>
                                       <p align="justify">ITIN mortgages are a very different kind of loan and is not the same scenario than permanent residents and non-permanent inhabitants experience. Permanent and non-permanent residents can make an application for traditional loans and government backed loans. Individuals who have taxpayer ID numbers can simply apply for an ITIN mortgage.</p>
                                       <p align="justify">It's very important to be aware that using an ITIN doesn't qualify as a substitution to get a visa. Possessing an ITIN provides the tax payer the chance to find a tax refund and supply evidence of earnings.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">ITIN Mortgage Requirements</h1>
                                       <p align="justify">Many ITIN mortgages have a term of 30 decades and generally include a greater rate of interest, nearly double or greater than the rate of interest which is included with a traditional 30-Year mortgage. Luckily, interest levels with the majority of ITIN mortgages have a fixed interest rate so that the monthly payment will stay the same before it's repaid. Borrowers are expected to put down at least 20 percent as a deposit to decrease the danger involved with the circumstance.</p>
                                       <p align="justify">Additional qualifications Which the Debtor must meet include:</p>
                                        <ol>
                                            <li>Can provide pay stubs Bookkeeping for the past 30 days</li>
                                            <li>Have an employment Record of at least Two years in the Exact Same line of work</li>
                                            <li>Offer 2 years' worth of tax returns with the Identical Citizen ID number</li>
                                            <li>Should Have Registered taxes under the Exact Same Amount for at least 2 Decades</li>
                                            <li>Needs a credit Rating Higher than 620</li>
                                        </ol>
                                       <p align="justify">It's very important to be aware that using an ITIN doesn't qualify as a substitution to get a visa. Possessing an ITIN provides the tax payer the chance to find a tax refund and supply evidence of earnings.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">ITIN Mortgages are More Expensive</h1>
                                       <p align="justify">ITIN mortgages aren't too popular and aren't as widely accessible as traditional loans. Some creditors will only issue ITIN mortgages in certain nations and a few creditors have different eligibility conditions compared to others like a greater down payment, sometimes going as large as 30 percent. Fundamentally, ITIN mortgages need a great number of savings and could be pretty pricey. Credit unions can be a fantastic alternative since they are inclined to provide competitive rates because of being a nonprofit company, and might even have special financing programs for non-citizens.</p>
                                       <p align="justify">ITIN mortgages normally have higher rates of interest and require a bigger down payment due to the risk factor connected with the circumstance, but a few lenders believe that there's actually less danger involved in ITIN mortgages. There have been cases where even when the debtor has been deported, the mortgage payments were made.</p>
                                    </div>
                                </article>
                                <article class="mu-blog-item">
                                    <div class="mu-blog-item-content">
                                        <h1 class="mu-blog-item-title">Overall Thoughts</h1>
                                       <p align="justify">The subject of immigrants will remain sticky, no matter what. You will find lots of with particular opinions and views on this subject, but the United States has ever been there to make opportunities to people who desire them. There's a reason why many visit the nation and no matter if they're a permanent resident, a realtor, or even an undocumented immigrant: immigrants can become homeowners. Provided that they're paying their taxes, leading to their communities and authorities, and will provide evidence they can manage the house, they ought to have the ability to satisfy their goals of owning property.</p>
                                    </div>
                                </article>
                                
                                                                                                
                            </div>
                        </div>
                        <div class="col-md-4">
                             @include('layout.partials.side')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Blog -->
@endsection