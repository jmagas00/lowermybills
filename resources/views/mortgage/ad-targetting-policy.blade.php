@extends('layout.app')

@section('main-content')
<main>
<section id="mu-footer">
		<div class="container">
				<div class="row">
						<div class="col-md-12">
								<div class="mu-footer-area">
										<div class="row">
												<div class="col-md-12">
														<div class="mu-footer-title">
																<h2>Ad Targeting Policy</h2>
														</div>
												</div>
										</div>
										<div class="row">
													<div class="col-md-6">
																<div class="mu-footer-left">
																		<p>This Advertisement Targeting Policy supplements the privacy policy by explaining how individuals use and share information for internet targeted advertising functions.</p>
																		<p>Core Digital Media, which manages websites like PaymentFixer.com, ClassesUSA.com, Pricegrabber.com, and other sites we may include from time to time, can discuss information about you and other clients jointly, but not particularly identifiable to you together with our parent company, our affiliated companies, and also third parties. This advice comprises:</p>
																<ul>
																		<li>
																				<p>Demographic information (Sex, estimated age, and Overall Geographical location, and your estimated Buy Capability);</p>
																		</li>
																		<li>
																				<p>Summarized census Data and other publicly available information (estimated Schooling Degree, homeownership status, and Projected Job Kind); and</p>
																		</li>
																		<li>
																				<p>Your inferred and voiced interests, such as transactional advice and product pursuits we derive from the visits to a few of the sites listed previously.</p>
																		</li>
																</ul> 
														</div>
													</div>
												<div class="col-md-6">
														<div class="mu-footer-right">
																<p>The recipients of the information use it to grow and deliver targeted advertisements on the family of sites and on the sites of third parties. The data is used just for advertising purposes. It's not utilized to create targeted offers which are priced differently based on projected purchase ability.</p>
																<p>The targeted advertisements resulting from the information sharing is connected to shared goods and service groups, including travel and leisure, retail, retail, financial services, electronics, pharmaceutical and consumer goods, book subscriptions and related classes that you see promoted regularly. These ads aren't based on information concerning adult content, aggregate or individual health information or documents, precise geographical location, data derived from the personal credit file, or information relevant to your financial accounts.</p>
																<p>We utilize cookies to facilitate the sharing of the information as you're online. Information in these biscuits is upgraded from time to time to make certain it's current and relevant. So as to appropriately safeguard the information inside them, as explained above, those cookies are encrypted.</p>
																<p>Should you like that we don't share this advice, and wouldn't like to get targeted advertisements as explained above, please visit our Opt Out page. Be aware that in the event you opt out, you will still get advertisements. Furthermore, if you choose out and after delete your cookies, then use a separate browser, or purchase a new personal computer, you'll have to renew your pick option.</p>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
</section>
</main>

@endsection