@extends('layout.app')

@section('main-content')
<section id="mu-mort-cal">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-mort-cal-area">
						<div class="row">
							<div class="col-md-12">
								<div class="mu-mort-title">
									<h2>Analysis Results</h2>
									
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="mu-mort-cal-right">
									
									<h3>Great news, your current grade is a D!</h3><br>
                                    <h3>And yes, a D here is good news.</h3><br>
									<p align="justify">Here’s Why: It means that your current mortgage probably isn’t right for you but that you can refinance and you should be able to achieve your goal of reducing the total amount of interest you have to pay. That’s right, you just found out that you could save a bunch by reducing your interest payments. See, we told you a D was good news.</p>
                                    <p align="justify">To provide some examples, you should be able to save by switching to a 10 or 15-year fixed if you qualify.</p>
                                </div>
							</div>
							<div class="col-md-6">
								<div class="mu-mort-cal-left">
								<img class="" src="{{asset('img/d.jpg')}}" alt="img" style="width:350px;">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  </section>
   
		<!-- Start Services -->
		<section id="mu-skills">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-skills-area">
							<!-- Title -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-title">
										<h2>OTHER POPULAR TOOLS</h2>
									</div>
								</div>
							</div>
							<!-- Start Skills Content -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-skills-content">
										<div class="row">
											<!-- start single item -->
											<div class="col-md-3">
												<div class="mu-single-skills">
													<div class="mu-skills-circle">
                            <img src="{{asset('img/icons/calculator.svg')}}" alt="">
													</div>
													<h3>MORTGAGE CALCULATOR</h3>
													<p>Compute your monthly payment, interest, APR, and overall interest paid with our easy and fixed-rate mortgage calculator.</p>
												</div>
											</div>
											<!-- End single item -->
											<!-- start single item -->
											<div class="col-md-3">
												<div class="mu-single-skills">
													<div class="mu-skills-circle">
                            <img src="{{asset('img/icons/money.svg')}}" alt="">
													</div>
													<h3>REFINANCE CALCULATOR</h3>
													<p>Use our Refinance Calculator to determine different monetary opportunities that might help you reduce your monthly payment or spend less.</p>
												</div>
											</div>
											<!-- End single item -->
											<!-- start single item -->
											<div class="col-md-3">
												<div class="mu-single-skills">
													<div class="mu-skills-circle">
                            <img src="{{asset('img/icons/comparision.svg')}}" alt="">
													</div>
													<h3>MORTGAGE COMPARISON</h3>
													<p>Select distinct mortgages and see all the various advantages that every mortgage supplies in a single easy-to-read infographic.</p>
												</div>
											</div>
											<!-- End single item -->
											<!-- start single item -->
											<div class="col-md-3">
												<div class="mu-single-skills">
													<div class="mu-skills-circle">
                            <img src="{{asset('img/icons/analysis.svg')}}" alt="">
													</div>
													<h3>MORTGAGE ANALYSIS</h3>
													<p>By the supplied advice, we examine your existing mortgage and let you know if it's either helping or hurting you.</p>
												</div>
											</div>
											<!-- End single item -->
										</div>
									</div>
								</div>
								
							</div>
							<!-- End Skills Content -->
						</div>
					</div>
				</div>
			</div>
		</section>
    <!-- End Skills -->

		
@endsection