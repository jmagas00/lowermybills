@extends('layout.app')

@section('main-content')

<!-- Lower My Bills -->
<section id="mu-bills">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-bills-area">
						<!-- Title -->
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
									<h1>Mortgage Advisor</h1>
                                    <h5 align="center">Find Out in the Event That You're In The Ideal Mortgage For The Situation</h5>
								</div>
							</div>
						</div>
						<!-- Start Feature Content -->
						<div class="row">
							<div class="col-md-6">
								<div class="mu-bills-right">
                                <p align="justify">Our Mortgage Advisor provides homeowners with customized recommendations about the sort of mortgage you ought to be in based on your unique conditions.


                                </p>
                                <h4 align="left">How it Works</h4>
									<ol>
										<li>
										You supply general details regarding your present mortgage and fiscal objectives.  
										</li>
										<li>
										We will search our network to find the mortgage that makes most sense for your situation and goals.
										</li>
										<li>
										We'll then provide a detailed recommendation if we believe you can do better than your current loan.  
                                        </li>
                                        
									</ol>
								</div>
							</div>
							<div class="col-md-6">
								<div class="mu-bills-left">
									<img class="" src="{{asset('img/people-office-group-team.jpg')}}" alt="img')}}">
								</div>
							</div>
						</div>
						<!-- End Feature Content -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Bills -->
	
	<section id="para-center">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="para-center-area">
							<!-- Title -->
							<div class="row">
								<div class="col-md-12">
									<h1 align="center" class="text-white">Answer Some Easy Questions So We Can Know Your Situation  </h1>						
									<form id="msform" name=mortgagecalc method=POST class="form-group">
											<fieldset class="panel-default">
												<span class="smpc-error" id="loanError"></span>
												<select onkeypress="return validNumber(event)" type="text" class="custom-select select" onChange="validate()" data-width="10%">
														<option value="" disabled="disabled" selected>Please Home Value</option>
														<option value="2500">$1 - $5,000</option>
														<option value="7500">$5,001 - $10,000</option>
														<option value="12500">$10,001 - $15,000</option>
														<option value="17500">$15,001 - $20,000</option>
														<option value="22500">$20,001 - $25,000</option>
														<option value="27500">$25,001 - $30,000</option>
														<option value="32500">$30,001 - $35,000</option>
														<option value="37500">$35,001 - $40,000</option>
														<option value="42500">$40,001 - $45,000</option>
														<option value="47500">$45,001 - $50,000</option>
														<option value="52500">$50,001 - $55,000</option>
														<option value="57500">$55,001 - $60,000</option>
														<option value="62500">$60,001 - $65,000</option>
														<option value="67500">$65,001 - $70,000</option>
														<option value="72500">$70,001 - $75,000</option>
														<option value="77500">$75,001 - $80,000</option>
														<option value="82500">$80,001 - $85,000</option>
														<option value="87500">$85,001 - $90,000</option>
														<option value="92500">$90,001 - $95,000</option>
														<option value="97500">$95,001 - $100,000</option>
														<option value="102500">$100,001 - $105,000</option>
														<option value="107500">$105,001 - $110,000</option>
														<option value="112500">$110,001 - $115,000</option>
														<option value="117500">$115,001 - $120,000</option>
														<option value="122500">$120,001 - $125,000</option>
														<option value="127500">$125,001 - $130,000</option>
														<option value="132500">$130,001 - $135,000</option>
														<option value="137500">$135,001 - $140,000</option>
														<option value="142500">$140,001 - $145,000</option>
														<option value="147500">$145,001 - $150,000</option>
														<option value="152500">$150,001 - $155,000</option><option value="157500">$155,001 - $160,000</option><option value="162500">$160,001 - $165,000</option><option value="167500">$165,001 - $170,000</option><option value="172500">$170,001 - $175,000</option><option value="177500">$175,001 - $180,000</option><option value="182500">$180,001 - $185,000</option><option value="187500">$185,001 - $190,000</option><option value="192500">$190,001 - $195,000</option><option value="197500">$195,001 - $200,000</option><option value="205000">$200,001 - $210,000</option><option value="215000">$210,001 - $220,000</option><option value="225000">$220,001 - $230,000</option><option value="235000">$230,001 - $240,000</option><option value="245000">$240,001 - $250,000</option><option value="255000">$250,001 - $260,000</option><option value="265000">$260,001 - $270,000</option><option value="275000">$270,001 - $280,000</option><option value="285000">$280,001 - $290,000</option><option value="295000">$290,001 - $300,000</option><option value="305000">$300,001 - $310,000</option><option value="315000">$310,001 - $320,000</option><option value="325000">$320,001 - $330,000</option><option value="335000">$330,001 - $340,000</option><option value="345000">$340,001 - $350,000</option><option value="355000">$350,001 - $360,000</option><option value="365000">$360,001 - $370,000</option><option value="375000">$370,001 - $380,000</option><option value="385000">$380,001 - $390,000</option><option value="395000">$390,001 - $400,000</option><option value="405000">$400,001 - $410,000</option><option value="415000">$410,001 - $420,000</option><option value="425000">$420,001 - $430,000</option><option value="435000">$430,001 - $440,000</option><option value="445000">$440,001 - $450,000</option><option value="455000">$450,001 - $460,000</option><option value="465000">$460,001 - $470,000</option><option value="475000">$470,001 - $480,000</option><option value="485000">$480,001 - $490,000</option><option value="495000">$490,001 - $500,000</option><option value="510000">$500,001 - $520,000</option><option value="530000">$520,001 - $540,000</option><option value="550000">$540,001 - $560,000</option><option value="570000">$560,001 - $580,000</option><option value="590000">$580,001 - $600,000</option><option value="610000">$600,001 - $620,000</option><option value="630000">$620,001 - $640,000</option><option value="650000">$640,001 - $660,000</option><option value="670000">$660,001 - $680,000</option><option value="690000">$680,001 - $700,000</option><option value="710000">$700,001 - $720,000</option><option value="730000">$720,001 - $740,000</option><option value="750000">$740,001 - $760,000</option><option value="770000">$760,001 - $780,000</option><option value="790000">$780,001 - $800,000</option><option value="810000">$800,001 - $820,000</option><option value="830000">$820,001 - $840,000</option><option value="850000">$840,001 - $860,000</option><option value="870000">$860,001 - $880,000</option><option value="890000">$880,001 - $900,000</option><option value="910000">$900,001 - $920,000</option><option value="930000">$920,001 - $940,000</option><option value="950000">$940,001 - $960,000</option><option value="970000">$960,001 - $980,000</option><option value="990000">$980,001 - $1,000,000</option><option value="1050000">$1,000,000 - $1,100,000</option><option value="1150000">$1,100,000 - $1,200,000</option><option value="1250000">$1,200,000 - $1,300,000</option><option value="1350000">$1,300,000 - $1,400,000</option><option value="1450000">$1,400,000 - $1,500,000</option><option value="1550000">$1,500,000 - $1,600,000</option><option value="1650000">$1,600,000 - $1,700,000</option><option value="1750000">$1,700,000 - $1,800,000</option><option value="1850000">$1,800,000 - $1,900,000</option><option value="1950000">$1,900,000 - $2,000,000</option><option value="2050000">$2,000,000 - $2,100,000</option><option value="2150000">$2,100,000 - $2,200,000</option><option value="2250000">$2,200,000 - $2,300,000</option><option value="2350000">$2,300,000 - $2,400,000</option><option value="2450000">$2,400,000 - $2,500,000</option><option value="2550000">$2,500,000 - $2,600,000</option><option value="2650000">$2,600,000 - $2,700,000</option><option value="2750000">$2,700,000 - $2,800,000</option><option value="2850000">$2,800,000 - $2,900,000</option><option value="2950000">$2,900,000 - $3,000,000</option><option value="3050000">$3,000,000 - $3,100,000</option><option value="3150000">$3,100,000 - $3,200,000</option><option value="3250000">$3,200,000 - $3,300,000</option><option value="3350000">$3,300,000 - $3,400,000</option><option value="3450000">$3,400,000 - $3,500,000</option><option value="3550000">$3,500,000 - $3,600,000</option><option value="3650000">$3,600,000 - $3,700,000</option><option value="3750000">$3,700,000 - $3,800,000</option><option value="3850000">$3,800,000 - $3,900,000</option><option value="3950000">$3,900,000 - $4,000,000</option><option value="4050000">$4,000,000 - $4,100,000</option><option value="4150000">$4,100,000 - $4,200,000</option><option value="4250000">$4,200,000 - $4,300,000</option><option value="4350000">$4,300,000 - $4,400,000</option><option value="4450000">$4,400,000 - $4,500,000</option><option value="4550000">$4,500,000 - $4,600,000</option><option value="4650000">$4,600,000 - $4,700,000</option><option value="4750000">$4,700,000 - $4,800,000</option><option value="4850000">$4,800,000 - $4,900,000</option><option value="4950000">$4,900,000 - $5,000,000</option><option value="5000000">$5,000,000+</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-sm-4" value="Next"/>
											</fieldset>
											<fieldset  class="panel-default">
												<span class="smpc-error" id="loanError"></span>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" name="loan" onChange="validate()" >
														<option value="" disabled="disabled" selected>Current Mortgage Balance</option>
														<option value="2500">$1 - $5,000</option><option value="7500">$5,001 - $10,000</option><option value="12500">$10,001 - $15,000</option><option value="17500">$15,001 - $20,000</option><option value="22500">$20,001 - $25,000</option><option value="27500">$25,001 - $30,000</option><option value="32500">$30,001 - $35,000</option><option value="37500">$35,001 - $40,000</option><option value="42500">$40,001 - $45,000</option><option value="47500">$45,001 - $50,000</option><option value="52500">$50,001 - $55,000</option><option value="57500">$55,001 - $60,000</option><option value="62500">$60,001 - $65,000</option><option value="67500">$65,001 - $70,000</option><option value="72500">$70,001 - $75,000</option><option value="77500">$75,001 - $80,000</option><option value="82500">$80,001 - $85,000</option><option value="87500">$85,001 - $90,000</option><option value="92500">$90,001 - $95,000</option><option value="97500">$95,001 - $100,000</option><option value="102500">$100,001 - $105,000</option><option value="107500">$105,001 - $110,000</option><option value="112500">$110,001 - $115,000</option><option value="117500">$115,001 - $120,000</option><option value="122500">$120,001 - $125,000</option><option value="127500">$125,001 - $130,000</option><option value="132500">$130,001 - $135,000</option><option value="137500">$135,001 - $140,000</option><option value="142500">$140,001 - $145,000</option><option value="147500">$145,001 - $150,000</option><option value="152500">$150,001 - $155,000</option><option value="157500">$155,001 - $160,000</option><option value="162500">$160,001 - $165,000</option><option value="167500">$165,001 - $170,000</option><option value="172500">$170,001 - $175,000</option><option value="177500">$175,001 - $180,000</option><option value="182500">$180,001 - $185,000</option><option value="187500">$185,001 - $190,000</option><option value="192500">$190,001 - $195,000</option><option value="197500">$195,001 - $200,000</option><option value="205000">$200,001 - $210,000</option><option value="215000">$210,001 - $220,000</option><option value="225000">$220,001 - $230,000</option><option value="235000">$230,001 - $240,000</option><option value="245000">$240,001 - $250,000</option><option value="255000">$250,001 - $260,000</option><option value="265000">$260,001 - $270,000</option><option value="275000">$270,001 - $280,000</option><option value="285000">$280,001 - $290,000</option><option value="295000">$290,001 - $300,000</option><option value="305000">$300,001 - $310,000</option><option value="315000">$310,001 - $320,000</option><option value="325000">$320,001 - $330,000</option><option value="335000">$330,001 - $340,000</option><option value="345000">$340,001 - $350,000</option><option value="355000">$350,001 - $360,000</option><option value="365000">$360,001 - $370,000</option><option value="375000">$370,001 - $380,000</option><option value="385000">$380,001 - $390,000</option><option value="395000">$390,001 - $400,000</option><option value="405000">$400,001 - $410,000</option><option value="415000">$410,001 - $420,000</option><option value="425000">$420,001 - $430,000</option><option value="435000">$430,001 - $440,000</option><option value="445000">$440,001 - $450,000</option><option value="455000">$450,001 - $460,000</option><option value="465000">$460,001 - $470,000</option><option value="475000">$470,001 - $480,000</option><option value="485000">$480,001 - $490,000</option><option value="495000">$490,001 - $500,000</option><option value="510000">$500,001 - $520,000</option><option value="530000">$520,001 - $540,000</option><option value="550000">$540,001 - $560,000</option><option value="570000">$560,001 - $580,000</option><option value="590000">$580,001 - $600,000</option><option value="610000">$600,001 - $620,000</option><option value="630000">$620,001 - $640,000</option><option value="650000">$640,001 - $660,000</option><option value="670000">$660,001 - $680,000</option><option value="690000">$680,001 - $700,000</option><option value="710000">$700,001 - $720,000</option><option value="730000">$720,001 - $740,000</option><option value="750000">$740,001 - $760,000</option><option value="770000">$760,001 - $780,000</option><option value="790000">$780,001 - $800,000</option><option value="810000">$800,001 - $820,000</option><option value="830000">$820,001 - $840,000</option><option value="850000">$840,001 - $860,000</option><option value="870000">$860,001 - $880,000</option><option value="890000">$880,001 - $900,000</option><option value="910000">$900,001 - $920,000</option><option value="930000">$920,001 - $940,000</option><option value="950000">$940,001 - $960,000</option><option value="970000">$960,001 - $980,000</option><option value="990000">$980,001 - $1,000,000</option><option value="1050000">$1,000,000 - $1,100,000</option><option value="1150000">$1,100,000 - $1,200,000</option><option value="1250000">$1,200,000 - $1,300,000</option><option value="1350000">$1,300,000 - $1,400,000</option><option value="1450000">$1,400,000 - $1,500,000</option><option value="1550000">$1,500,000 - $1,600,000</option><option value="1650000">$1,600,000 - $1,700,000</option><option value="1750000">$1,700,000 - $1,800,000</option><option value="1850000">$1,800,000 - $1,900,000</option><option value="1950000">$1,900,000 - $2,000,000</option><option value="2050000">$2,000,000 - $2,100,000</option><option value="2150000">$2,100,000 - $2,200,000</option><option value="2250000">$2,200,000 - $2,300,000</option><option value="2350000">$2,300,000 - $2,400,000</option><option value="2450000">$2,400,000 - $2,500,000</option><option value="2550000">$2,500,000 - $2,600,000</option><option value="2650000">$2,600,000 - $2,700,000</option><option value="2750000">$2,700,000 - $2,800,000</option><option value="2850000">$2,800,000 - $2,900,000</option><option value="2950000">$2,900,000 - $3,000,000</option><option value="3050000">$3,000,000 - $3,100,000</option><option value="3150000">$3,100,000 - $3,200,000</option><option value="3250000">$3,200,000 - $3,300,000</option><option value="3350000">$3,300,000 - $3,400,000</option><option value="3450000">$3,400,000 - $3,500,000</option><option value="3550000">$3,500,000 - $3,600,000</option><option value="3650000">$3,600,000 - $3,700,000</option><option value="3750000">$3,700,000 - $3,800,000</option><option value="3850000">$3,800,000 - $3,900,000</option><option value="3950000">$3,900,000 - $4,000,000</option><option value="4050000">$4,000,000 - $4,100,000</option><option value="4150000">$4,100,000 - $4,200,000</option><option value="4250000">$4,200,000 - $4,300,000</option><option value="4350000">$4,300,000 - $4,400,000</option><option value="4450000">$4,400,000 - $4,500,000</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset class="panel-default">
												<span class="smpc-error" id="loanError"></span>
												<select name="" id="" type="text" class="custom-select selectpicker select" onChange="validate()" > 
														<option value="" disabled="disabled" selected>Select Property Use</option>
														<option value="Primary Residence">Primary Residence</option><option value="Secondary Home">Secondary Home</option><option value="Investment Property">Investment Property</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span class="smpc-error" id="rateError"></span>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" name="rate" onChange="validate()" > 
														<option value="" disabled="disabled" selected>Select a Rate</option>
														<option value="2.00">2.00%</option><option value="2.25">2.25%</option><option value="2.50">2.50%</option><option value="2.75">2.75%</option><option value="3.00">3.00%</option><option value="3.25">3.25%</option><option value="3.50">3.50%</option><option value="3.75">3.75%</option><option value="4.00">4.00%</option><option value="4.25">4.25%</option><option value="4.50">4.50%</option><option value="4.75">4.75%</option><option value="5.00">5.00%</option><option value="5.25">5.25%</option><option value="5.50">5.50%</option><option value="5.75">5.75%</option><option value="6.00">6.00%</option><option value="6.25">6.25%</option><option value="6.50">6.50%</option><option value="6.75">6.75%</option><option value="7.00">7.00%</option><option value="7.25">7.25%</option><option value="7.50">7.50%</option><option value="7.75">7.75%</option><option value="8.00">8.00%</option><option value="8.25">8.25%</option><option value="8.50">8.50%</option><option value="8.75">8.75%</option><option value="9.00">9.00%</option><option value="9.25">9.25%</option><option value="9.50">9.50%</option><option value="9.75">9.75%</option><option value="10.00">10.00%</option><option value="10.25">10.25%</option><option value="10.50">10.50%</option><option value="10.75">10.75%</option><option value="11.00">11%+</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span class="smpc-error" id="yearsError"></span>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="years" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Select Monthly Payment</option>
														<option value="200">Less than $200</option><option value="250">$201 - $300</option><option value="350">$301 - $400</option><option value="450">$401 - $500</option><option value="550">$501 - $600</option><option value="650">$601 - $700</option><option value="750">$701 - $800</option><option value="850">$801 - $900</option><option value="950">$901 - $1,000</option><option value="1050">$1,001 - $1,100</option><option value="1150">$1,101 - $1,200</option><option value="1250">$1,201 - $1,300</option><option value="1350">$1,301 - $1,400</option><option value="1450">$1,401 - $1,500</option><option value="1550">$1,501 - $1,600</option><option value="1650">$1,601 - $1,700</option><option value="1750">$1,701 - $1,800</option><option value="1850">$1,801 - $1,900</option><option value="1950">$1,901 - $2,000</option><option value="2050">$2,001 - $2,100</option><option value="2150">$2,101 - $2,200</option><option value="2250">$2,201 - $2,300</option><option value="2350">$2,301 - $2,400</option><option value="2450">$2,401 - $2,500</option><option value="2550">$2,501 - $2,600</option><option value="2650">$2,601 - $2,700</option><option value="2750">$2,701 - $2,800</option><option value="2850">$2,801 - $2,900</option><option value="2950">$2,901 - $3,000</option><option value="3050">$3,000 - $3,100</option><option value="3150">$3,100 - $3,200</option><option value="3250">$3,200 - $3,300</option><option value="3350">$3,300 - $3,400</option><option value="3450">$3,400 - $3,500</option><option value="3550">$3,500 - $3,600</option><option value="3650">$3,600 - $3,700</option><option value="3750">$3,700 - $3,800</option><option value="3850">$3,800 - $3,900</option><option value="3950">$3,900 - $4,000</option><option value="4050">$4,000 - $4,100</option><option value="4150">$4,100 - $4,200</option><option value="4250">$4,200 - $4,300</option><option value="4350">$4,300 - $4,400</option><option value="4450">$4,400 - $4,500</option><option value="4550">$4,500 - $4,600</option><option value="4650">$4,600 - $4,700</option><option value="4750">$4,700 - $4,800</option><option value="4850">$4,800 - $4,900</option><option value="4950">$4,900 - $5,000</option><option value="5050">$5,000 - $5,100</option><option value="5150">$5,100 - $5,200</option><option value="5250">$5,200 - $5,300</option><option value="5350">$5,300 - $5,400</option><option value="5450">$5,400 - $5,500</option><option value="5550">$5,500 - $5,600</option><option value="5650">$5,600 - $5,700</option><option value="5750">$5,700 - $5,800</option><option value="5850">$5,800 - $5,900</option><option value="5950">$5,900 - $6,000</option><option value="6001">More than $6,000</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<!-- Looking for loans with a monthly payment lower than $monthlypayment -->
											<fieldset  class="panel-default">
												<span>Which Best Describes How You Feel About Your Monthly Mortgage Payment?</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="lower">I Want/Need a Lower Payment</option><option value="wiggleroom">I Have Some Wiggle Room</option><option value="higher">I Could Pay More</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span>Credit Profile</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="740">Excellent (720 or above)</option><option value="670">Good (620 - 719)</option><option value="599">Fair (580-619)</option><option value="559">Needs Improvement (540-579)</option><option value="500">Poor (539 or lower)</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default radios">
												<span>Could You Use Some Cash To Pay Off Credit Cards Or Student Loans?</span><br>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=1>
														<!-- <span class="checkmark"></span> -->
														YES
												</label>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=2>
														<!-- <span class="checkmark"></span> -->
														NO
												</label>
												<!-- <Label><input type="radio" class="big-radio radio select" id="" value="1"><span>YES</span></Label>
												<Label><input type="radio" class="big-radio radio select" id="" value="2"><span>NO</span></Label> -->
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span>Number Of Late Mortgage Payments In The Past 12 Months</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="0">None</option><option value="1">1</option><option value="2">2+</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<!-- Options to Help you take Cash Out -->
											<fieldset  class="panel-default">
												<span>Which Option Best Describes Your Monthly Budget?</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="very tight">Very Tight</option><option value="tight">Tight</option><option value="future flexible">Tight, Expected To Improve</option><option value="flexible">Flexible</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span>How Many More Years Do You Plan on Owning The Property?</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="5">5</option><option value="10">10</option><option value="15">15</option><option value="30">30+</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span>In How Many Years Do You Plan To Retire?</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="15 years or less">15 years or less</option><option value="15-30 years">15 - 30 years</option><option value="More than 30 years">More than 30 years</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default radios">
												<span>Active Or Previous U.S. Military Service For you Or Your Spouse?</span><br>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=1>
														<!-- <span class="checkmark"></span> -->
														YES
												</label>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=2>
														<!-- <span class="checkmark"></span> -->
														NO
												</label>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default radios">
												<span>Do You Currently Have An FHA Loan?</span><br>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=1>
														<!-- <span class="checkmark"></span> -->
														YES
												</label>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=2>
														<!-- <span class="checkmark"></span> -->
														NO
												</label>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span>Enter Your Zip Code</span><br>
												<input type="input" name="zip" id="zip" class="zip" onkeypress="validate()"/>
												<input type="button" name="next" class="next btn btn-lg btn-success col-md-4" onClick="return myPayment()" value="Calculate"><br>
												<input type="hidden" name="mortgagevalue" id="mortgagevalue"/>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<!-- Preparing Results -->
										</form>
									<p class="smpc-monthlypayment" id="monthlyPayment"> </p> 
									@include('layout.partials.mortgage-calc')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<section id="mu-bills">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-bills-area">
						<!-- Title -->
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
									<h2>MORE ABOUT HOW MORTGAGE ADVISOR WORKS</h2>
								</div>
							</div>
						</div>
						<!-- Start Feature Content -->
						<div class="row">
							<div class="col-md-6">
								<div class="mu-bills-left">
									<img class="" src="{{asset('img/advisor.jpg')}}" alt="img" style="width:700px;">
								</div>
							</div>

							
							<div class="col-md-6">
								<div class="mu-bills-right">
									<p><b>Finally, An Answer To Your 1 Mortgage Question You Care About</b></p>
									<p align="justify">Our Mortgage Advisor provides homeowners a location where they can go to answer that the one mortgage question they actually care about having replied: "What Mortgage Should I Be In?" We'll learn about your financial position, your financial objectives, and about what sort of mortgage you're currently in, conduct that through our advisor algorithm, and return a simple to understand recommendation about what kind of mortgage is probably most appropriate for you.</p>
									<p align="justify">This is the source that homeowners've been on the lookout for.  No longer do homeowners will need to launch into complicated google searches to find out which type of mortgage they should be in.  Just answer a few of questions and we'll show you what mortgage we think could best match you, your requirements, and your objectives.  Of course, you're the only one who completely knows your fiscal situation so it's up to you to pick the ideal mortgage, but we wish to try and help get you the very best possible information by which to make this decision.
										
									</p>
								</div>
							</div>
						</div>
						<!-- End Feature Content -->
					</div>
				</div>
			</div>
		</div>
	</section>



     <section id="mu-skills">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="mu-skills-area">
							<!-- Title -->
							<div class="row">
								<div class="col-md-12">
									<div class="mu-title">
										<h2>OTHER POPULAR TOOLS</h2>
									</div>
								</div>
							</div>
							<!-- Start Skills Content -->
							<div class="row">
							<div class="col-md-12">
								<div class="mu-skills-content">
									<div class="row">
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a href="{{url('/mortgagecalculator')}}" target="_blank"><img src="{{asset('img/icons/calculator.svg')}}" alt=""></a>
												</div>
												<h3><a href="{{url('/mortgagecalculator')}}" target="_blank">MORTGAGE CALCULATOR</a></h3>
												<p>Compute your monthly payment, interest, APR, and overall interest paid with our easy and fixed-rate mortgage calculator.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a  href="{{url('/refinancecalculator')}}" target="_blank"><img src="{{asset('img/icons/money.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/refinancecalculator')}}" target="_blank">REFINANCE CALCULATOR</a></h3>
												<p>Use our Refinance Calculator to determine different monetary opportunities that might help you reduce your monthly payment or spend less.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a href="{{url('/mortgagecomparisontool')}}" target="_blank"><img src="{{asset('img/icons/comparision.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/mortgagecomparisontool')}}" target="_blank">MORTGAGE COMPARISON</a></h3>
												<p>Select distinct mortgages and see all the various advantages that every mortgage supplies in a single easy-to-read infographic.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a  href="{{url('/mortgageanalysis')}}" target="_blank"><img src="{{asset('img/icons/analysis.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/mortgageanalysis')}}" target="_blank">MORTGAGE ANALYSIS</a></h3>
												<p>By the supplied advice, we examine your existing mortgage and let you know if it's either helping or hurting you.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<!-- End Skills Content -->
						</div>
					</div>
				</div>
			</div>
		</section>

@endsection