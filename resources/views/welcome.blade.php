@extends('layout.app')

@section('main-content')

<script>

    $(window).on('load',function(){
    		//$('#success').modal('hide'); 
    		$('#myModal').modal('show'); 
    });

    function popup(){
    	$('#myModal').modal('show');
    }

	if($('#myModal').modal('show'))
	{
		$('body').css('overflow','hidden');
		/*$('body').css('position','fixed');*/
	}

</script>



<style type="text/css">
	#domecap {
			  text-transform: capitalize
			}
.bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: auto;
}



</style>

<div id="mu-slider">
		
		<div class="mu-slide">
			<div class="mu-single-slide">
				<img src="{{asset('img/counter-bg.jpg')}}" alt="slider img" style="height:400px;">
				<div class="mu-single-slide-content-area">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="center text-white">
									<h1 class="display-5" align="center">Are You Really In the Perfect Mortgage?</h1>
									<h3 align="center">Learn By Utilizing Our Mortgage Advisor Tool!</h3>
									<div class="smpc-div" align="center">
										<form id="msform" name=mortgagecalc method=POST class="form-group">
											<fieldset class="panel-default">
												<span class="smpc-error" id="loanError"></span>
												<select onkeypress="return validNumber(event)" type="text" class="custom-select select" onChange="validate()" data-width="10%">
														<option value="" disabled="disabled" selected>Please Home Value</option>
														<option value="2500">$1 - $5,000</option>
														<option value="7500">$5,001 - $10,000</option>
														<option value="12500">$10,001 - $15,000</option>
														<option value="17500">$15,001 - $20,000</option>
														<option value="22500">$20,001 - $25,000</option>
														<option value="27500">$25,001 - $30,000</option>
														<option value="32500">$30,001 - $35,000</option>
														<option value="37500">$35,001 - $40,000</option>
														<option value="42500">$40,001 - $45,000</option>
														<option value="47500">$45,001 - $50,000</option>
														<option value="52500">$50,001 - $55,000</option>
														<option value="57500">$55,001 - $60,000</option>
														<option value="62500">$60,001 - $65,000</option>
														<option value="67500">$65,001 - $70,000</option>
														<option value="72500">$70,001 - $75,000</option>
														<option value="77500">$75,001 - $80,000</option>
														<option value="82500">$80,001 - $85,000</option>
														<option value="87500">$85,001 - $90,000</option>
														<option value="92500">$90,001 - $95,000</option>
														<option value="97500">$95,001 - $100,000</option>
														<option value="102500">$100,001 - $105,000</option>
														<option value="107500">$105,001 - $110,000</option>
														<option value="112500">$110,001 - $115,000</option>
														<option value="117500">$115,001 - $120,000</option>
														<option value="122500">$120,001 - $125,000</option>
														<option value="127500">$125,001 - $130,000</option>
														<option value="132500">$130,001 - $135,000</option>
														<option value="137500">$135,001 - $140,000</option>
														<option value="142500">$140,001 - $145,000</option>
														<option value="147500">$145,001 - $150,000</option>
														<option value="152500">$150,001 - $155,000</option><option value="157500">$155,001 - $160,000</option><option value="162500">$160,001 - $165,000</option><option value="167500">$165,001 - $170,000</option><option value="172500">$170,001 - $175,000</option><option value="177500">$175,001 - $180,000</option><option value="182500">$180,001 - $185,000</option><option value="187500">$185,001 - $190,000</option><option value="192500">$190,001 - $195,000</option><option value="197500">$195,001 - $200,000</option><option value="205000">$200,001 - $210,000</option><option value="215000">$210,001 - $220,000</option><option value="225000">$220,001 - $230,000</option><option value="235000">$230,001 - $240,000</option><option value="245000">$240,001 - $250,000</option><option value="255000">$250,001 - $260,000</option><option value="265000">$260,001 - $270,000</option><option value="275000">$270,001 - $280,000</option><option value="285000">$280,001 - $290,000</option><option value="295000">$290,001 - $300,000</option><option value="305000">$300,001 - $310,000</option><option value="315000">$310,001 - $320,000</option><option value="325000">$320,001 - $330,000</option><option value="335000">$330,001 - $340,000</option><option value="345000">$340,001 - $350,000</option><option value="355000">$350,001 - $360,000</option><option value="365000">$360,001 - $370,000</option><option value="375000">$370,001 - $380,000</option><option value="385000">$380,001 - $390,000</option><option value="395000">$390,001 - $400,000</option><option value="405000">$400,001 - $410,000</option><option value="415000">$410,001 - $420,000</option><option value="425000">$420,001 - $430,000</option><option value="435000">$430,001 - $440,000</option><option value="445000">$440,001 - $450,000</option><option value="455000">$450,001 - $460,000</option><option value="465000">$460,001 - $470,000</option><option value="475000">$470,001 - $480,000</option><option value="485000">$480,001 - $490,000</option><option value="495000">$490,001 - $500,000</option><option value="510000">$500,001 - $520,000</option><option value="530000">$520,001 - $540,000</option><option value="550000">$540,001 - $560,000</option><option value="570000">$560,001 - $580,000</option><option value="590000">$580,001 - $600,000</option><option value="610000">$600,001 - $620,000</option><option value="630000">$620,001 - $640,000</option><option value="650000">$640,001 - $660,000</option><option value="670000">$660,001 - $680,000</option><option value="690000">$680,001 - $700,000</option><option value="710000">$700,001 - $720,000</option><option value="730000">$720,001 - $740,000</option><option value="750000">$740,001 - $760,000</option><option value="770000">$760,001 - $780,000</option><option value="790000">$780,001 - $800,000</option><option value="810000">$800,001 - $820,000</option><option value="830000">$820,001 - $840,000</option><option value="850000">$840,001 - $860,000</option><option value="870000">$860,001 - $880,000</option><option value="890000">$880,001 - $900,000</option><option value="910000">$900,001 - $920,000</option><option value="930000">$920,001 - $940,000</option><option value="950000">$940,001 - $960,000</option><option value="970000">$960,001 - $980,000</option><option value="990000">$980,001 - $1,000,000</option><option value="1050000">$1,000,000 - $1,100,000</option><option value="1150000">$1,100,000 - $1,200,000</option><option value="1250000">$1,200,000 - $1,300,000</option><option value="1350000">$1,300,000 - $1,400,000</option><option value="1450000">$1,400,000 - $1,500,000</option><option value="1550000">$1,500,000 - $1,600,000</option><option value="1650000">$1,600,000 - $1,700,000</option><option value="1750000">$1,700,000 - $1,800,000</option><option value="1850000">$1,800,000 - $1,900,000</option><option value="1950000">$1,900,000 - $2,000,000</option><option value="2050000">$2,000,000 - $2,100,000</option><option value="2150000">$2,100,000 - $2,200,000</option><option value="2250000">$2,200,000 - $2,300,000</option><option value="2350000">$2,300,000 - $2,400,000</option><option value="2450000">$2,400,000 - $2,500,000</option><option value="2550000">$2,500,000 - $2,600,000</option><option value="2650000">$2,600,000 - $2,700,000</option><option value="2750000">$2,700,000 - $2,800,000</option><option value="2850000">$2,800,000 - $2,900,000</option><option value="2950000">$2,900,000 - $3,000,000</option><option value="3050000">$3,000,000 - $3,100,000</option><option value="3150000">$3,100,000 - $3,200,000</option><option value="3250000">$3,200,000 - $3,300,000</option><option value="3350000">$3,300,000 - $3,400,000</option><option value="3450000">$3,400,000 - $3,500,000</option><option value="3550000">$3,500,000 - $3,600,000</option><option value="3650000">$3,600,000 - $3,700,000</option><option value="3750000">$3,700,000 - $3,800,000</option><option value="3850000">$3,800,000 - $3,900,000</option><option value="3950000">$3,900,000 - $4,000,000</option><option value="4050000">$4,000,000 - $4,100,000</option><option value="4150000">$4,100,000 - $4,200,000</option><option value="4250000">$4,200,000 - $4,300,000</option><option value="4350000">$4,300,000 - $4,400,000</option><option value="4450000">$4,400,000 - $4,500,000</option><option value="4550000">$4,500,000 - $4,600,000</option><option value="4650000">$4,600,000 - $4,700,000</option><option value="4750000">$4,700,000 - $4,800,000</option><option value="4850000">$4,800,000 - $4,900,000</option><option value="4950000">$4,900,000 - $5,000,000</option><option value="5000000">$5,000,000+</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-sm-4" value="Next"/>
											</fieldset>
											<fieldset  class="panel-default">
												<span class="smpc-error" id="loanError"></span>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" name="loan" onChange="validate()" >
														<option value="" disabled="disabled" selected>Current Mortgage Balance</option>
														<option value="2500">$1 - $5,000</option><option value="7500">$5,001 - $10,000</option><option value="12500">$10,001 - $15,000</option><option value="17500">$15,001 - $20,000</option><option value="22500">$20,001 - $25,000</option><option value="27500">$25,001 - $30,000</option><option value="32500">$30,001 - $35,000</option><option value="37500">$35,001 - $40,000</option><option value="42500">$40,001 - $45,000</option><option value="47500">$45,001 - $50,000</option><option value="52500">$50,001 - $55,000</option><option value="57500">$55,001 - $60,000</option><option value="62500">$60,001 - $65,000</option><option value="67500">$65,001 - $70,000</option><option value="72500">$70,001 - $75,000</option><option value="77500">$75,001 - $80,000</option><option value="82500">$80,001 - $85,000</option><option value="87500">$85,001 - $90,000</option><option value="92500">$90,001 - $95,000</option><option value="97500">$95,001 - $100,000</option><option value="102500">$100,001 - $105,000</option><option value="107500">$105,001 - $110,000</option><option value="112500">$110,001 - $115,000</option><option value="117500">$115,001 - $120,000</option><option value="122500">$120,001 - $125,000</option><option value="127500">$125,001 - $130,000</option><option value="132500">$130,001 - $135,000</option><option value="137500">$135,001 - $140,000</option><option value="142500">$140,001 - $145,000</option><option value="147500">$145,001 - $150,000</option><option value="152500">$150,001 - $155,000</option><option value="157500">$155,001 - $160,000</option><option value="162500">$160,001 - $165,000</option><option value="167500">$165,001 - $170,000</option><option value="172500">$170,001 - $175,000</option><option value="177500">$175,001 - $180,000</option><option value="182500">$180,001 - $185,000</option><option value="187500">$185,001 - $190,000</option><option value="192500">$190,001 - $195,000</option><option value="197500">$195,001 - $200,000</option><option value="205000">$200,001 - $210,000</option><option value="215000">$210,001 - $220,000</option><option value="225000">$220,001 - $230,000</option><option value="235000">$230,001 - $240,000</option><option value="245000">$240,001 - $250,000</option><option value="255000">$250,001 - $260,000</option><option value="265000">$260,001 - $270,000</option><option value="275000">$270,001 - $280,000</option><option value="285000">$280,001 - $290,000</option><option value="295000">$290,001 - $300,000</option><option value="305000">$300,001 - $310,000</option><option value="315000">$310,001 - $320,000</option><option value="325000">$320,001 - $330,000</option><option value="335000">$330,001 - $340,000</option><option value="345000">$340,001 - $350,000</option><option value="355000">$350,001 - $360,000</option><option value="365000">$360,001 - $370,000</option><option value="375000">$370,001 - $380,000</option><option value="385000">$380,001 - $390,000</option><option value="395000">$390,001 - $400,000</option><option value="405000">$400,001 - $410,000</option><option value="415000">$410,001 - $420,000</option><option value="425000">$420,001 - $430,000</option><option value="435000">$430,001 - $440,000</option><option value="445000">$440,001 - $450,000</option><option value="455000">$450,001 - $460,000</option><option value="465000">$460,001 - $470,000</option><option value="475000">$470,001 - $480,000</option><option value="485000">$480,001 - $490,000</option><option value="495000">$490,001 - $500,000</option><option value="510000">$500,001 - $520,000</option><option value="530000">$520,001 - $540,000</option><option value="550000">$540,001 - $560,000</option><option value="570000">$560,001 - $580,000</option><option value="590000">$580,001 - $600,000</option><option value="610000">$600,001 - $620,000</option><option value="630000">$620,001 - $640,000</option><option value="650000">$640,001 - $660,000</option><option value="670000">$660,001 - $680,000</option><option value="690000">$680,001 - $700,000</option><option value="710000">$700,001 - $720,000</option><option value="730000">$720,001 - $740,000</option><option value="750000">$740,001 - $760,000</option><option value="770000">$760,001 - $780,000</option><option value="790000">$780,001 - $800,000</option><option value="810000">$800,001 - $820,000</option><option value="830000">$820,001 - $840,000</option><option value="850000">$840,001 - $860,000</option><option value="870000">$860,001 - $880,000</option><option value="890000">$880,001 - $900,000</option><option value="910000">$900,001 - $920,000</option><option value="930000">$920,001 - $940,000</option><option value="950000">$940,001 - $960,000</option><option value="970000">$960,001 - $980,000</option><option value="990000">$980,001 - $1,000,000</option><option value="1050000">$1,000,000 - $1,100,000</option><option value="1150000">$1,100,000 - $1,200,000</option><option value="1250000">$1,200,000 - $1,300,000</option><option value="1350000">$1,300,000 - $1,400,000</option><option value="1450000">$1,400,000 - $1,500,000</option><option value="1550000">$1,500,000 - $1,600,000</option><option value="1650000">$1,600,000 - $1,700,000</option><option value="1750000">$1,700,000 - $1,800,000</option><option value="1850000">$1,800,000 - $1,900,000</option><option value="1950000">$1,900,000 - $2,000,000</option><option value="2050000">$2,000,000 - $2,100,000</option><option value="2150000">$2,100,000 - $2,200,000</option><option value="2250000">$2,200,000 - $2,300,000</option><option value="2350000">$2,300,000 - $2,400,000</option><option value="2450000">$2,400,000 - $2,500,000</option><option value="2550000">$2,500,000 - $2,600,000</option><option value="2650000">$2,600,000 - $2,700,000</option><option value="2750000">$2,700,000 - $2,800,000</option><option value="2850000">$2,800,000 - $2,900,000</option><option value="2950000">$2,900,000 - $3,000,000</option><option value="3050000">$3,000,000 - $3,100,000</option><option value="3150000">$3,100,000 - $3,200,000</option><option value="3250000">$3,200,000 - $3,300,000</option><option value="3350000">$3,300,000 - $3,400,000</option><option value="3450000">$3,400,000 - $3,500,000</option><option value="3550000">$3,500,000 - $3,600,000</option><option value="3650000">$3,600,000 - $3,700,000</option><option value="3750000">$3,700,000 - $3,800,000</option><option value="3850000">$3,800,000 - $3,900,000</option><option value="3950000">$3,900,000 - $4,000,000</option><option value="4050000">$4,000,000 - $4,100,000</option><option value="4150000">$4,100,000 - $4,200,000</option><option value="4250000">$4,200,000 - $4,300,000</option><option value="4350000">$4,300,000 - $4,400,000</option><option value="4450000">$4,400,000 - $4,500,000</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset class="panel-default">
												<span class="smpc-error" id="loanError"></span>
												<select name="" id="" type="text" class="custom-select selectpicker select" onChange="validate()" > 
														<option value="" disabled="disabled" selected>Select Property Use</option>
														<option value="Primary Residence">Primary Residence</option><option value="Secondary Home">Secondary Home</option><option value="Investment Property">Investment Property</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span class="smpc-error" id="rateError"></span>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" name="rate" onChange="validate()" > 
														<option value="" disabled="disabled" selected>Select a Rate</option>
														<option value="2.00">2.00%</option><option value="2.25">2.25%</option><option value="2.50">2.50%</option><option value="2.75">2.75%</option><option value="3.00">3.00%</option><option value="3.25">3.25%</option><option value="3.50">3.50%</option><option value="3.75">3.75%</option><option value="4.00">4.00%</option><option value="4.25">4.25%</option><option value="4.50">4.50%</option><option value="4.75">4.75%</option><option value="5.00">5.00%</option><option value="5.25">5.25%</option><option value="5.50">5.50%</option><option value="5.75">5.75%</option><option value="6.00">6.00%</option><option value="6.25">6.25%</option><option value="6.50">6.50%</option><option value="6.75">6.75%</option><option value="7.00">7.00%</option><option value="7.25">7.25%</option><option value="7.50">7.50%</option><option value="7.75">7.75%</option><option value="8.00">8.00%</option><option value="8.25">8.25%</option><option value="8.50">8.50%</option><option value="8.75">8.75%</option><option value="9.00">9.00%</option><option value="9.25">9.25%</option><option value="9.50">9.50%</option><option value="9.75">9.75%</option><option value="10.00">10.00%</option><option value="10.25">10.25%</option><option value="10.50">10.50%</option><option value="10.75">10.75%</option><option value="11.00">11%+</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span class="smpc-error" id="yearsError"></span>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="years" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Select Monthly Payment</option>
														<option value="200">Less than $200</option><option value="250">$201 - $300</option><option value="350">$301 - $400</option><option value="450">$401 - $500</option><option value="550">$501 - $600</option><option value="650">$601 - $700</option><option value="750">$701 - $800</option><option value="850">$801 - $900</option><option value="950">$901 - $1,000</option><option value="1050">$1,001 - $1,100</option><option value="1150">$1,101 - $1,200</option><option value="1250">$1,201 - $1,300</option><option value="1350">$1,301 - $1,400</option><option value="1450">$1,401 - $1,500</option><option value="1550">$1,501 - $1,600</option><option value="1650">$1,601 - $1,700</option><option value="1750">$1,701 - $1,800</option><option value="1850">$1,801 - $1,900</option><option value="1950">$1,901 - $2,000</option><option value="2050">$2,001 - $2,100</option><option value="2150">$2,101 - $2,200</option><option value="2250">$2,201 - $2,300</option><option value="2350">$2,301 - $2,400</option><option value="2450">$2,401 - $2,500</option><option value="2550">$2,501 - $2,600</option><option value="2650">$2,601 - $2,700</option><option value="2750">$2,701 - $2,800</option><option value="2850">$2,801 - $2,900</option><option value="2950">$2,901 - $3,000</option><option value="3050">$3,000 - $3,100</option><option value="3150">$3,100 - $3,200</option><option value="3250">$3,200 - $3,300</option><option value="3350">$3,300 - $3,400</option><option value="3450">$3,400 - $3,500</option><option value="3550">$3,500 - $3,600</option><option value="3650">$3,600 - $3,700</option><option value="3750">$3,700 - $3,800</option><option value="3850">$3,800 - $3,900</option><option value="3950">$3,900 - $4,000</option><option value="4050">$4,000 - $4,100</option><option value="4150">$4,100 - $4,200</option><option value="4250">$4,200 - $4,300</option><option value="4350">$4,300 - $4,400</option><option value="4450">$4,400 - $4,500</option><option value="4550">$4,500 - $4,600</option><option value="4650">$4,600 - $4,700</option><option value="4750">$4,700 - $4,800</option><option value="4850">$4,800 - $4,900</option><option value="4950">$4,900 - $5,000</option><option value="5050">$5,000 - $5,100</option><option value="5150">$5,100 - $5,200</option><option value="5250">$5,200 - $5,300</option><option value="5350">$5,300 - $5,400</option><option value="5450">$5,400 - $5,500</option><option value="5550">$5,500 - $5,600</option><option value="5650">$5,600 - $5,700</option><option value="5750">$5,700 - $5,800</option><option value="5850">$5,800 - $5,900</option><option value="5950">$5,900 - $6,000</option><option value="6001">More than $6,000</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<!-- Looking for loans with a monthly payment lower than $monthlypayment -->
											<fieldset  class="panel-default">
												<span>Which Best Describes How You Feel About Your Monthly Mortgage Payment?</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="lower">I Want/Need a Lower Payment</option><option value="wiggleroom">I Have Some Wiggle Room</option><option value="higher">I Could Pay More</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span>Credit Profile</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="740">Excellent (720 or above)</option><option value="670">Good (620 - 719)</option><option value="599">Fair (580-619)</option><option value="559">Needs Improvement (540-579)</option><option value="500">Poor (539 or lower)</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default radios">
												<span>Could You Use Some Cash To Pay Off Credit Cards Or Student Loans?</span><br>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=1>
														<!-- <span class="checkmark"></span> -->
														YES
												</label>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=2>
														<!-- <span class="checkmark"></span> -->
														NO
												</label>
												<!-- <Label><input type="radio" class="big-radio radio select" id="" value="1"><span>YES</span></Label>
												<Label><input type="radio" class="big-radio radio select" id="" value="2"><span>NO</span></Label> -->
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span>Number Of Late Mortgage Payments In The Past 12 Months</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="0">None</option><option value="1">1</option><option value="2">2+</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<!-- Options to Help you take Cash Out -->
											<fieldset  class="panel-default">
												<span>Which Option Best Describes Your Monthly Budget?</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="very tight">Very Tight</option><option value="tight">Tight</option><option value="future flexible">Tight, Expected To Improve</option><option value="flexible">Flexible</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span>How Many More Years Do You Plan on Owning The Property?</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="5">5</option><option value="10">10</option><option value="15">15</option><option value="30">30+</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span>In How Many Years Do You Plan To Retire?</span><br>
												<select type=text onkeypress="return validNumber(event)" class="custom-select selectpicker select" id="monthlyPayment" name="monthlyPayment" onChange="validate()" > 	
														<option value="" disabled="disabled" selected>Please Select</option>
														<option value="15 years or less">15 years or less</option><option value="15-30 years">15 - 30 years</option><option value="More than 30 years">More than 30 years</option>
												</select>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default radios">
												<span>Active Or Previous U.S. Military Service For you Or Your Spouse?</span><br>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=1>
														<!-- <span class="checkmark"></span> -->
														YES
												</label>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=2>
														<!-- <span class="checkmark"></span> -->
														NO
												</label>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default radios">
												<span>Do You Currently Have An FHA Loan?</span><br>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=1>
														<!-- <span class="checkmark"></span> -->
														YES
												</label>
												<label><input type="radio" class="big-radio radio" name="radio" onClick="validateRadio()" value=2>
														<!-- <span class="checkmark"></span> -->
														NO
												</label>
												<input type="button" name="next" class="next action-button btn btn-md btn-primary col-md-4" value="Next"/><br>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<fieldset  class="panel-default">
												<span>Enter Your Zip Code</span><br>
												<input type="input" name="zip" id="zip" class="zip" onkeypress="validate()"/>
												<input type="button" name="next" class="next btn btn-lg btn-success col-md-4" onClick="return myPayment()" value="Calculate"><br>
												<input type="hidden" name="mortgagevalue" id="mortgagevalue"/>
												<a name="previous" class="previous text-white" onClick="validate()"/><strong><< Back </strong> </a>
											</fieldset>
											<!-- Preparing Results -->
										</form>
										<p class="smpc-monthlypayment" id="monthlyPayment"> </p>
									</div>
									@include('layout.partials.mortgage-calc')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<main>
		<section id="mu-skills" >
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-skills-area">
							 <h3  align="center" ><p style="color: #007BFF">Sign Up Now to Lower Your Payments By Up to 40%!!!</p></h3>
							<div  style="border-radius: 15px" >
                                            
                                                    <form action = "/" method = "post">
                                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                                        <div class="form-row ">
                                                        <div class="form-group col-md-3" required>                                                     	
                                                         <input type="fname" class="form-control" id="firstname" placeholder="First Name" value=""  name='firstname' required>
                                                         </div>

                                                         <div class="form-group col-md-3">                                                
                                                         <input type="lname" class="form-control" id="lastname" placeholder="Last Name" name="lastname" required>
                                                         </div>
                                                         <div class="form-group col-md-2">
                                                        	<select class="form-control" name="age" id="hID">
                                                        		<option>Select Age:</option>
                                                        		<option>	18	</option>
																<option>	19	</option>
																<option>	20	</option>
																<option>	21	</option>
																<option>	22	</option>
																<option>	23	</option>
																<option>	24	</option>
																<option>	25	</option>
																<option>	26	</option>
																<option>	27	</option>
																<option>	28	</option>
																<option>	29	</option>
																<option>	30	</option>
																<option>	31	</option>
																<option>	32	</option>
																<option>	33	</option>
																<option>	34	</option>
																<option>	35	</option>
																<option>	36	</option>
																<option>	37	</option>
																<option>	38	</option>
																<option>	39	</option>
																<option>	40	</option>
																<option>	41	</option>
																<option>	42	</option>
																<option>	43	</option>
																<option>	44	</option>
																<option>	45	</option>
																<option>	46	</option>
																<option>	47	</option>
																<option>	48	</option>
																<option>	49	</option>
																<option>	50	</option>
																<option>	51	</option>
																<option>	52	</option>
																<option>	53	</option>
																<option>	54	</option>
																<option>	55	</option>
																<option>	56	</option>
																<option>	57	</option>
																<option>	58	</option>
																<option>	59	</option>
																<option>	60	</option>
																<option>	61	</option>
																<option>	62	</option>
																<option>	63	</option>
																<option>	64	</option>
																<option>	65	</option>
																<option>	66	</option>
																<option>	67	</option>
																<option>	68	</option>
																<option>	69	</option>
																<option>	70	</option>
																<option>	71	</option>
																<option>	72	</option>
																<option>	73	</option>
																<option>	74	</option>
																<option>	75	</option>
																<option>	76	</option>
																<option>	77	</option>
																<option>	78	</option>
																<option>	79	</option>
																<option>	80	</option>
																<option>	81	</option>
																<option>	82	</option>
																<option>	83	</option>
																<option>	84	</option>
																<option>	85	</option>
																<option>	86	</option>
																<option>	87	</option>
																<option>	88	</option>
																<option>	89	</option>
																<option>	90	</option>
																<option>	91	</option>
																<option>	92	</option>
																<option>	93	</option>
																<option>	94	</option>
																<option>	95	</option>

                                                        	</select>
                                                         </div>

                                                         <div class="form-group col-md-4">                                                     	
                                                         <input type="phone" class="form-control" id="" placeholder="Phone" value="" name="phone_mobile" required>
                                                         </div>

                                                         <div class="form-group col-md-3">                                                
                                                         <input type="lname" class="form-control" id="" placeholder="Mobile" name="mobile" required>
                                                         </div>

                                                          <div class="form-group col-md-3">                                                         
                                                            <input type="" class="form-control" id="" placeholder="Address" name="address" required>
                                                            </div>

                                                           <div class="form-group col-md-3" required>                                                         
                                                            <input type="city" class="form-control" id="" placeholder="City" name="city">
                                                            </div>

                                                         <div class="form-group col-md-3">                       
                                                            <select class="form-control" name="state" required>
                                                            	<option value="ALL">State:</option>
																<option value="AL">Alabama</option>
																<option value="AK">Alaska</option>
																<option value="AZ">Arizona</option>
																<option value="AR">Arkansas</option>
																<option value="CA">California</option>
																<option value="CO">Colorado</option>
																<option value="DC">Columbia (D.C.)</option>
																<option value="CT">Connecticut</option>
																<option value="DE">Delaware</option>
																<option value="FL">Florida</option>
																<option value="GA">Georgia</option>
																<option value="HI">Hawaii</option>
																<option value="ID">Idaho</option>
																<option value="IL">Illinois</option>
																<option value="IN">Indiana</option>

																<option value="IA">
																	Iowa
																</option>

																<option value="KS">
																	Kansas
																</option>

																<option value="KY">
																	Kentucky
																</option>

																<option value="LA">
																	Louisiana
																</option>

																<option value="ME">
																	Maine
																</option>

																<option value="MD">
																	Maryland
																</option>

																<option value="MA">
																	Massachusetts
																</option>

																<option value="MI">
																	Michigan
																</option>

																<option value="MN">
																	Minnesota
																</option>

																<option value="MS">
																	Mississippi
																</option>

																<option value="MO">
																	Missouri
																</option>

																<option value="MT">
																	Montana
																</option>

																<option value="NE">
																	Nebraska
																</option>

																<option value="NV">
																	Nevada
																</option>

																<option value="NH">
																	New Hampshire
																</option>

																<option value="NJ">
																	New Jersey
																</option>

																<option value="NM">
																	New Mexico
																</option>

																<option value="NY">
																	New York
																</option>

																<option value="NC">
																	North Carolina
																</option>

																<option value="ND">
																	North Dakota
																</option>

																<option value="OH">
																	Ohio
																</option>

																<option value="OK">
																	Oklahoma
																</option>

																<option value="OR">
																	Oregon
																</option>

																<option value="PA">
																	Pennsylvania
																</option>

																<option value="PR">
																	Puerto Rico
																</option>

																<option value="RI">
																	Rhode Island
																</option>

																<option value="SC">
																	South Carolina
																</option>

																<option value="SD">
																	South Dakota
																</option>

																<option value="TN">
																	Tennessee
																</option>

																<option value="TX">
																	Texas
																</option>

																<option value="UT">
																	Utah
																</option>

																<option value="VT">
																	Vermont
																</option>

																<option value="VA">
																	Virginia
																</option>

																<option value="WA">
																	Washington
																</option>

																<option value="WV">
																	West Virginia
																</option>

																<option value="WI">
																	Wisconsin
																</option>

																<option value="WY">
																	Wyoming
																</option>

																</select>
                                                            </div>

                                                            <div class="form-group col-md-6">                                                         
                                                            <input type="zip" class="form-control" id="" placeholder="zip" name="zip" required>
                                                            </div>

                                                            <div class="form-group col-md-6">                                                         
                                                            <input type="email" class="form-control" id="" placeholder="Email" name="mail" required>
                                                            </div>

                                                            <div class="form-group col-md-12">
                                                            	<p align="center" style="margin-left: 2%; font-weight: bold;font-size: 130%">CHECK THE BOXES BELLOW FOR WHAT PAYMENTS YOU WOULD LIKE TO LOWER!</p>
                                                            </div>


                                                          <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="Lowering_Mortgage" name="Lowering_Mortgage" value="Lowering Mortgage"><label for="Lowering_Mortgage" style="font-size: 88%">Lowering Mortgage</label>                          	  
                                                            </div>

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="41" name="Home_Owner_Insurance" value="Home Owner Insurance"><label for="41" >Home Owner Insurance</label>                        	  
                                                            </div>

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="51" name="Life_Insurance" value="Life Insurance"><label for="51">Life Insurance</label>                      	  
                                                            </div>

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="311" name="Reverse_Mortgage" value="Reverse Mortgage"><label for="311" >Reverse Mortgage</label>                          	  
                                                            </div>

                                                            <!-- 2ns checkbox -->

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="321" name="Auto_Insurance" value="Auto Insurance"><label for="321" >Auto Insurance</label>                          	  
                                                            </div>

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="331" name="Medical_Insurance_Plan" value="Medical Insurance Plan"><label for="331"  >Medical Insurance Plan</label>                        	  
                                                            </div>

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="3231" name="Home_Security" value="Home Security"><label for="3231" >Home Security</label>                      	  
                                                            </div>

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="3321" name="Medication" value="Medication"><label for="3321" >Medication</label>                          	  
                                                            </div>

                                                            <!-- 3rs checkbox -->

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="3121" name="Electric_Bill" value="Electric Bill"><label for="3121" >Electric Bill</label>                          	  
                                                            </div>

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="3131" name="Home_Warranty" value="Home Warranty"><label for="3131">Home Warranty</label>                        	  
                                                            </div>

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="3331" name="Internet_Bill" value="Internet Bill"><label for="3331">Internet Bill</label>                      	  
                                                            </div>

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="3441" name="Cell_Phonr_Bill" value="Cell Phonr Bill"><label for="3441" >Cell Phonr Bill</label>                          	  
                                                            </div>

                                                             <!-- 4th checkbox -->

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="341" name="Home_Phone_Bill"  value="Home Phone Bill"><label for="341" >Home Phone Bill</label>                          	  
                                                            </div>

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="31112" name="Credit_Card_Bill" value="Credit Card Bill"><label for="31112" >Credit Card Bill</label>                        	  
                                                            </div>

                                                            <div class="form-group col-md-3">    
                                                             	<input type="checkbox" id="32111" name="Cable Satellite_Tv_Bill" value="Cable Satellite Tv Bill"><label for="32111" 	>Cable Satellite Tv Bill</label>                      	  
                                                            </div>


															<div class="form-group col-md-12 " style="font-size: 11px; text-transform: uppercase" >
																BY SUBMITTING THIS FORM YOU AGREE THE INFORMATION YOU PROVIDED TODAY CAN BE  USED FOR SERVICING YOUR INQUIRY REGARDING LOWERING YOUR EXISTING PAYMENTS OR PROVIDING YOU ALTERNATIVE SERVICES FROM PAYMENTFIXER.COM OR OUR PARTNERS. INCLUDING GIVING CONSENT TO CONTACT YOU REGARDING MEDICARE RELATED OFFERS. YOU ARE UNDER NO OBLIGATION TO PURCHASE GOODS OR SERVICES FROM US OR OUR PARTNERS YOU MAY BE CONTACTED BY PHONE, WIRELESS NUMBER, INCLUDING WITH THE USE AUTO DIAL TECHNOLOGIES VIA SMS/TEXT, EMAIL AND/OR POSTAL MAIL. EVEN IF YOUR NUMBER IS LISTED ON A STATE OR FEDERAL DO NOT CALL LIST. FOR MORE INFORMATION, CONTACT <a href="mailto:INFO@PAYMENTFIXER.COM">INFO@PAYMENTFIXER.COM</a> OR TO OPT OUT OF FUTURE COMUNICATIONS 
                                                             	<!-- <p>  The information you provided today is used solely for servicing this inquiry about lowering your existing payments or providing you alternative services from paymentfixer.com or our partners. You are under no obligation to purchase goods or services from the you are interested in and you may opt out of their communications ata ny time, including phone, wireless number with use auto dial technologies via SMS/text, email and/or postal mail. For more information, contact <span style="color: blue">info@paymentfixer.com</span> --><br><br><br>
                                                             	<div class="form-group col-md-12" align="center">
                                                             		
                                                             		<button type="submit" class="btn btn-primary col-md-4"  >SUBMIT</button>
                                                             	</div>
																<!--  <br><br><br>  -->
                                                         	</p>
                                                    </div>
                                                        
                                             </div>
                                     <div >
                                                        	
                                 </div>                                     
                             </form>
                         </div>
					</div>
				</div>
			</div>
		</div>
	</section> 
	<section id="mu-skills">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-skills-area">
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
									<h2>OTHER POPULAR TOOLS</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="mu-skills-content">
									<div class="row">
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a href="{{url('/mortgagecalculator')}}"><img src="{{asset('img/icons/calculator.svg')}}" alt=""></a>
												</div>
												<h3><a href="{{url('/mortgagecalculator')}}">MORTGAGE CALCULATOR</a></h3>
												<p>Compute your monthly payment, interest, APR, and overall interest paid with our easy and fixed-rate mortgage calculator.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a  href="{{url('/refinancecalculator')}}"><img src="{{asset('img/icons/money.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/refinancecalculator')}}">REFINANCE CALCULATOR</a></h3>
												<p>Use our Refinance Calculator to determine different monetary opportunities that might help you reduce your monthly payment or spend less.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a href="{{url('/mortgagecomparisontool')}}"><img src="{{asset('img/icons/comparision.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/mortgagecomparisontool')}}">MORTGAGE COMPARISON</a></h3>
												<p>Select distinct mortgages and see all the various advantages that every mortgage supplies in a single easy-to-read infographic.</p>
											</div>
										</div>
										<div class="col-md-3">
											<div class="mu-single-skills">
												<div class="mu-skills-circle">
													<a  href="{{url('/mortgageanalysis')}}"><img src="{{asset('img/icons/analysis.svg')}}" alt=""></a>
												</div>
												<h3><a  href="{{url('/mortgageanalysis')}}">MORTGAGE ANALYSIS</a></h3>
												<p>By the supplied advice, we examine your existing mortgage and let you know if it's either helping or hurting you.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="mu-works">
	<div class="container">
			<div class="row">
					<div class="col-md-12">
							<div class="mu-works-area">
									<div class="row">
											<div class="col-md-12">
													<div class="mu-works-title">
													</div>
											</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="mu-works-left">
											<h1 style="text-align:center;">How PaymentFixer.com Works</h1>
											<br />
												<ol>
													<li style="font-weight:bold;">
														<p>You Complete our Simple 3 Second form</p>
													</li>
													<li style="font-weight:bold;">
														<p>We Meet you to Best Suppliers</p>
													</li>
													<li style="font-weight:bold;">
														<p>They contact you with mortgage details.</p>
													</li>
												</ol>
												<button type="button" class="btn btn-warning btn-lg btn-works" style="margin-left:25px;" onclick="popup()">Click Here</button> 
											</div>
										</div>
										<div class="col-md-6">
												<div class="mu-works-right">
													<img class="" src="{{asset('img/pexels-photo-920382.jpeg')}}" alt="img" style="width:500px;">
												</div>
										</div>
									</div>
							</div>
					</div>
			</div>
		</div>
	</section>	
	<section id="mu-bills">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="mu-bills-area">
					<div class="row">
						<div class="col-md-12">
							<div class="mu-title">
								<h2>Other Bills We Could Reduce</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="mu-bills-content">
								<div class="row">
									<div class="col-md-4">
										<div class="mu-single-service">
											<div class="mu-bills-single-service-icon"  onclick="popup()"><i class="fas fa-home" aria-hidden="true" onclick="popup()" ></i></div>
											<div class="mu-bills-single-service-content">
												<h3 onclick="popup()"> <a href=""> Home Refinance</a></h3>
												<p>It might be simpler than you think to refinance your present mortgage.</p>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="mu-bills-single-service">
											<div class="mu-bills-single-service-icon"  onclick="popup()"><i class="fa fa-shopping-cart" aria-hidden="true" onclick="popup()"></i></div>
											<div class="mu-bills-single-service-content">
												<h3 onclick="popup()"> <a href="">Home Purchase</a></h3>
												
												<p>Compare offers from other lenders: save money and time</p>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="mu-bills-single-service">
											<div class="mu-bills-single-service-icon"  onclick="popup()"><i class="fa fa-wheelchair" aria-hidden="true" onclick="popup()"></i></div>
											<div class="mu-bills-single-service-content">
												<h3 onclick="popup()"> <a href="">Reverse Mortgage</a></h3>
												
												<p>Seniors over 62 can utilize their home equity for money.</p>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="mu-bills-single-service">
											<div class="mu-bills-single-service-icon"  onclick="popup()"><i class="fas fa-male" aria-hidden="true" onclick="popup()"></i></div>
											<div class="mu-bills-single-service-content">
												<h3 onclick="popup()"> <a href="">Personal Loans</a></h3>
												
												<p>Loans of up to $35,000 accessible from a assortment of lenders.</p>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="mu-bills-single-service">
											<div class="mu-bills-single-service-icon"  onclick="popup()"><i class="fa fa-car" aria-hidden="true"></i></div>
											<div class="mu-bills-single-service-content">
												<h3 onclick="popup()"> <a href="">Auto Insurance</a></h3>
					
												<p>You could have the ability to save on car insurance by switching.</p>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="mu-bills-single-service">
											<div class="mu-bills-single-service-icon"  onclick="popup()"><i class="fa fa-cog" aria-hidden="true"></i></div>
											<div class="mu-bills-single-service-content">
													<h3 onclick="popup()"> <a href="">Life Insurance</a></h3>
												<p>Provide financial protection for your family when they want it.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="mu-about">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-about-area">
						<!-- Title -->
						<div class="row">
							<div class="col-md-12">
								<div class="mu-title">
									<h2>About PaymentFixer.com</h2>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="mu-about-left">
									<img class="" src="{{asset('img/pexels-photo-1056552.jpeg')}}" alt="img">
								</div>
							</div>
							<div class="col-md-6">
								<div class="mu-about-right">
									<p><strong>PaymentFixer.com is the one-stop destination that gives savings through relationships with over 500 service providers across multiple categories, including home loans, credit cards, automobile and health insurance, and extended distance and solutions.</strong> Together with PaymentFixer.com, clients can quickly search, compare and decrease a number of the monthly debts, free of price. The free services functions by enabling customers to input particular information that is used to match with companies that may fulfill their requirements. PaymentFixer.com supplies savings through over 500 service providers across a number of categories, such as home loans, auto loans, auto loans, automobile loans, personal loans, charge cards, charge cards, and auto and life insurance. If you would like to use our services click get started below and complete the form.
									<!--If you would like to use us-please click here and complete the form.--></p>
									<button type="button" class="btn btn-warning btn-lg btn-works" onclick="popup()">Get Started</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- start pop up here -->
			<div class="modal fade col-md-12" id="myModal" role="dialog" class="clickable">
		    	<div class="modal-dialog modal-ku" >
		    
			      	<!-- Modal content-->
			      	<div class="modal-content clickable" >
		        		<div class="modal-header clickable"  align="center">
		        			<h3 class="modal-title " ><p style="color: #007BFF;margin-bottom:25%">Sign Up Now to Lower Your Payments By Up to 40%!!!</p></h3>
		          			<button  type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-bottom: 50%"><span aria-hidden="true">&times;</span></button>
		        		</div>
		        		<div class="modal-body col-md-12 clickable" style="margin-top: -30%">
		         		<div style="border-radius: 15px" class="clickable">
		         			 				<form action = "/" method = "post">
                                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                                        <div class="form-row clickable">
                                                          <div class="form-group col-md-6 clickable">                                                
                                                         <input type="lname" class="form-control clickable" id="firstname" placeholder="First Name" name="firstname" value="">
                                                         </div>

                                                         <div class="form-group col-md-6 clickable">                                                
                                                         <input type="lname" class="form-control clickable" id="lastname" placeholder="Last Name" name="lastname">
                                                         </div>

                                                         <div class="form-group col-md-2 clickable">                                                     	
                                                         
                                                        	<select class="form-control clickable" style="font-size: 13px" name="age" >
                                                        			<option >Age</option>
                                                        		<option>	18	</option>
																<option>	19	</option>
																<option>	20	</option>
																<option>	21	</option>
																<option>	22	</option>
																<option>	23	</option>
																<option>	24	</option>
																<option>	25	</option>
																<option>	26	</option>
																<option>	27	</option>
																<option>	28	</option>
																<option>	29	</option>
																<option>	30	</option>
																<option>	31	</option>
																<option>	32	</option>
																<option>	33	</option>
																<option>	34	</option>
																<option>	35	</option>
																<option>	36	</option>
																<option>	37	</option>
																<option>	38	</option>
																<option>	39	</option>
																<option>	40	</option>
																<option>	41	</option>
																<option>	42	</option>
																<option>	43	</option>
																<option>	44	</option>
																<option>	45	</option>
																<option>	46	</option>
																<option>	47	</option>
																<option>	48	</option>
																<option>	49	</option>
																<option>	50	</option>
																<option>	51	</option>
																<option>	52	</option>
																<option>	53	</option>
																<option>	54	</option>
																<option>	55	</option>
																<option>	56	</option>
																<option>	57	</option>
																<option>	58	</option>
																<option>	59	</option>
																<option>	60	</option>
																<option>	61	</option>
																<option>	62	</option>
																<option>	63	</option>
																<option>	64	</option>
																<option>	65	</option>
																<option>	66	</option>
																<option>	67	</option>
																<option>	68	</option>
																<option>	69	</option>
																<option>	70	</option>
																<option>	71	</option>
																<option>	72	</option>
																<option>	73	</option>
																<option>	74	</option>
																<option>	75	</option>
																<option>	76	</option>
																<option>	77	</option>
																<option>	78	</option>
																<option>	79	</option>
																<option>	80	</option>
																<option>	81	</option>
																<option>	82	</option>
																<option>	83	</option>
																<option>	84	</option>
																<option>	85	</option>
																<option>	86	</option>
																<option>	87	</option>
																<option>	88	</option>
																<option>	89	</option>
																<option>	90	</option>
																<option>	91	</option>
																<option>	92	</option>
																<option>	93	</option>
																<option>	94	</option>
																<option>	95	</option>

                                                        	</select>
                                                         </div>

                                                         <div class="form-group col-md-4 clickable">                                                     	
                                                         <input type="phone" class="form-control clickable" id="firstname" placeholder="Phone" value="" name="phone_mobile" required>
                                                         </div>

                                                         <div class="form-group col-md-3 clickable">                                                
                                                         <input type="phone" class="form-control clickable" id="lastname" placeholder="Mobile" name="mobile" required>
                                                         </div>

                                                          <div class="form-group col-md-3 clickable">                                                         
                                                            <input type="" class="form-control clickable" id="city" placeholder="Address" name="address" required>
                                                            </div>

                                                           <div class="form-group col-md-3 clickable" required>                                                         
                                                            <input type="city" class="form-control clickable" id="" placeholder="City" name="city">
                                                            </div>

                                                          <div class="form-group col-md-3 clickable">                       
                                                            <select class="form-control clickable" name="state" required>
                                                            	<option value="ALL">State:</option>
																<option value="AL">Alabama</option>
																<option value="AK">Alaska</option>
																<option value="AZ">Arizona</option>
																<option value="AR">Arkansas</option>
																<option value="CA">California</option>
																<option value="CO">Colorado</option>
																<option value="DC">Columbia (D.C.)</option>
																<option value="CT">Connecticut</option>
																<option value="DE">Delaware</option>
																<option value="FL">Florida</option>
																<option value="GA">Georgia</option>
																<option value="HI">Hawaii</option>
																<option value="ID">Idaho</option>
																<option value="IL">Illinois</option>
																<option value="IN">Indiana</option>

																<option value="IA">
																	Iowa
																</option>

																<option value="KS">
																	Kansas
																</option>

																<option value="KY">
																	Kentucky
																</option>

																<option value="LA">
																	Louisiana
																</option>

																<option value="ME">
																	Maine
																</option>

																<option value="MD">
																	Maryland
																</option>

																<option value="MA">
																	Massachusetts
																</option>

																<option value="MI">
																	Michigan
																</option>

																<option value="MN">
																	Minnesota
																</option>

																<option value="MS">
																	Mississippi
																</option>

																<option value="MO">
																	Missouri
																</option>

																<option value="MT">
																	Montana
																</option>

																<option value="NE">
																	Nebraska
																</option>

																<option value="NV">
																	Nevada
																</option>

																<option value="NH">
																	New Hampshire
																</option>

																<option value="NJ">
																	New Jersey
																</option>

																<option value="NM">
																	New Mexico
																</option>

																<option value="NY">
																	New York
																</option>

																<option value="NC">
																	North Carolina
																</option>

																<option value="ND">
																	North Dakota
																</option>

																<option value="OH">
																	Ohio
																</option>

																<option value="OK">
																	Oklahoma
																</option>

																<option value="OR">
																	Oregon
																</option>

																<option value="PA">
																	Pennsylvania
																</option>

																<option value="PR">
																	Puerto Rico
																</option>

																<option value="RI">
																	Rhode Island
																</option>

																<option value="SC">
																	South Carolina
																</option>

																<option value="SD">
																	South Dakota
																</option>

																<option value="TN">
																	Tennessee
																</option>

																<option value="TX">
																	Texas
																</option>

																<option value="UT">
																	Utah
																</option>

																<option value="VT">
																	Vermont
																</option>

																<option value="VA">
																	Virginia
																</option>

																<option value="WA">
																	Washington
																</option>

																<option value="WV">
																	West Virginia
																</option>

																<option value="WI">
																	Wisconsin
																</option>

																<option value="WY">
																	Wyoming
																</option>

																</select>
                                                            </div>

                                                            <div class="form-group col-md-6 clickable">                                                         
                                                            <input type="zip" class="form-control clickable" id="zip" placeholder="zip" name="zip" required>
                                                            </div>

                                                            <div class="form-group col-md-12 clickable">                                                         
                                                            <input type="email" class="form-control clickable" id="email" placeholder="Email" name="mail" required>
                                                            </div>


                                                            <div class="form-group col-md-12 clickable">
                                                            	<p align="center" style="margin-left: 2%; font-weight: bold;font-size: 130%">CHECK THE BOXES BELLOW FOR WHAT PAYMENTS YOU WOULD LIKE TO LOWER!</p>
                                                            </div>


                                                          <div class="form-group col-md-4 clickable">    
                                                             	<input type="checkbox" id="3" name="Lowering_Mortgage" value="Lowering Mortgage"><label for="3" style="font-size: 88%">Lowering Mortgage</label>                          	  
                                                            </div>

                                                            <div class="form-group col-md-4 clickable">    
                                                             		<input type="checkbox" id="4" name="Home_Owner_Insurance" value="Home Owner Insurance"><label for="4" style="font-size: 75%">Home Owner Insurance</label>                        	  
                                                            </div>

                                                            <div class="form-group col-md-4 clickable">    
                                                             	<input type="checkbox" id="5" name="Life_Insurance" value="Life Insurance"><label for="5" style="font-size: 88%">Life Insurance</label>                      	  
                                                            </div>

                                                            <div class="form-group col-md-4 clickable">    
                                                             	<input type="checkbox" id="31" name="Reverse_Mortgage" value="Reverse Mortgage"><label for="31" style="font-size: 88%">Reverse Mortgage</label>                          	  
                                                            </div>

                                                            <!-- 2ns checkbox -->

                                                            <div class="form-group col-md-4 clickable">    
                                                             <input type="checkbox" id="32" name="Auto_Insurance" value="Auto Insurance"><label for="32"  style="font-size: 88%">Auto Insurance</label>                          	  
                                                            </div>

                                                            <div class="form-group col-md-4 clickable">    
                                                             	<input type="checkbox" id="33" name="Medical_Insurance_Plan" value="Medical Insurance Plan"><label for="33"  style="font-size: 75%">Medical Insurance Plan</label>                        	  
                                                            </div>

                                                            <div class="form-group col-md-4 clickable">    
                                                             	<input type="checkbox" id="323" name="Home_Security" value="Home Security"><label for="323" style="font-size: 88%">Home Security</label>                      	  
                                                            </div>

                                                            <div class="form-group col-md-4 clickable">    
                                                             	<input type="checkbox" id="332" name="Medication" value="Medication"><label for="332" style="font-size: 88%">Medication</label>                          	  
                                                            </div>

                                                            <!-- 3rs checkbox -->

                                                            <div class="form-group col-md-4 clickable">    
                                                             	<input type="checkbox" id="312" name="Electric_Bill" value="Electric Bill"><label for="312" style="font-size: 88%">Electric Bill</label>                          	  
                                                            </div>

                                                            <div class="form-group col-md-4 clickable">    
                                                             <input type="checkbox" id="313" name="Home_Warranty" value="Home Warranty"><label for="313" style="font-size: 88%">Home Warranty</label>                        	  
                                                            </div>

                                                            <div class="form-group col-md-4 clickable">    
                                                             <input type="checkbox" id="333" name="Internet_Bill" value="Internet Bill"><label for="333" style="font-size: 88%">Internet Bill</label>                      	  
                                                            </div>

                                                            <div class="form-group col-md-4 clickable">    
                                                             	<input type="checkbox" id="344" name="Cell_Phonr_Bill" value="Cell Phonr Bill"><label for="344" style="font-size: 88%">Cell Phone Bill</label>                          	  
                                                            </div>

                                                             <!-- 4th checkbox -->

                                                            <div class="form-group col-md-4 clickable">    
                                                             	<input type="checkbox" id="34" name="Home_Phone_Bill"  value="Home Phone Bill"><label for="34" style="font-size: 88%">Home Phone Bill</label>                          	  
                                                            </div>

                                                            <div class="form-group col-md-4 clickable">    
                                                             	<input type="checkbox" id="3112" name="Credit_Card_Bill" value="Credit Card Bill"><label for="3112" style="font-size: 88%">Credit Card Bill</label>                        	  
                                                            </div>

                                                            <div class="form-group col-md-4 clickable">    
                                                             	<input type="checkbox" id="3211" name="Cable Satellite_Tv_Bill" value="Cable Satellite Tv Bill"><label for="3211" style="font-size: 80%"  >Cable Satellite Tv Bill</label>                      	  
                                                            </div>


															<div class="form-group col-md-12 clickable" style="font-size: 11px; text-transform: uppercase" >
																BY SUBMITTING THIS FORM YOU AGREE THE INFORMATION YOU PROVIDED TODAY CAN BE  USED FOR SERVICING YOUR INQUIRY REGARDING LOWERING YOUR EXISTING PAYMENTS OR PROVIDING YOU ALTERNATIVE SERVICES FROM PAYMENTFIXER.COM OR OUR PARTNERS. INCLUDING GIVING CONSENT TO CONTACT YOU REGARDING MEDICARE RELATED OFFERS. YOU ARE UNDER NO OBLIGATION TO PURCHASE GOODS OR SERVICES FROM US OR OUR PARTNERS YOU MAY BE CONTACTED BY PHONE, WIRELESS NUMBER, INCLUDING WITH THE USE AUTO DIAL TECHNOLOGIES VIA SMS/TEXT, EMAIL AND/OR POSTAL MAIL. EVEN IF YOUR NUMBER IS LISTED ON A STATE OR FEDERAL DO NOT CALL LIST. FOR MORE INFORMATION, CONTACT <a href="mailto:INFO@PAYMENTFIXER.COM">INFO@PAYMENTFIXER.COM</a> OR TO OPT OUT OF FUTURE COMUNICATIONS 
     
																 <br><br><br> <button type="submit" class="btn btn-primary col-md-4" >SUBMIT</button>
                                                             		
                                                         	</p>
                                            </div>
                                        </div>
                                    <div >   	
                                </div>                                     
 
                             </form>
		         		</div>
		         	</div>
		        </div>
            </div>
		<!-- endsection pop up here -->
	</section>
</main><!-- start -->
<!-- start -->

@if (session()->has('success'))
   <script type="text/javascript">
   	$('#myModal').modal('hide'); 
   alert('Thank you for sign up..');
 
</script>
@endif
@endsection