<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

@include('layout.partials.head')

<body id="page-top" data-spy="scroll" data-target="#mainNav">
    <div id="app">

        @include('layout.partials.nav')

        @yield('main-content')

    </div>

    @include('layout.partials.footer')
    @include('layout.partials.scripts')

</body>
</html>
