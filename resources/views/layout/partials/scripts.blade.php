   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script> -->

<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script> -->

    <!-- Slick slider -->
<!-- <script type="text/javascript" src="{{ asset('js/slick.min.js')}}"></script> -->

    <!-- Filterable Gallery js -->
<!-- <script type="text/javascript" src="{{ asset('js/jquery.filterizr.min.js') }}"></script>
 -->
    <!-- Gallery Lightbox -->
<!-- <script type="text/javascript" src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script> -->

<script>
	
	var current_fs, next_fs, previous_fs;

	$(".next").prop('disabled', 'disabled');
	$(".submit").prop('disabled', 'disabled');

	$(".next").click(function(){

		$(".next").prop('disabled', 'disabled');

		current_fs = $(this).parent();
		next_fs = $(this).parent().next();

		next_fs.show(); 

		current_fs.hide();
		
	});

	$(".previous").click(function(){

		current_fs = $(this).parent();
		previous_fs = $(this).parent().prev();

		previous_fs.show(); 

		current_fs.hide();
		
	});

	function validate()
	{
		select = $(".select");
		zip = $(".zip").val();

		if (select.value == ""||null) {
			$(".next").prop('disabled', 'disabled');
			$(".submit").prop('disabled', 'disabled');
			if (zip === "") {
				$(".next").prop('disabled', 'disabled');
				$(".submit").prop('disabled', 'disabled');
			}else{
				$(".next").removeAttr("disabled");
				$(".submit").removeAttr("disabled");
			}
		}else{
			$(".next").removeAttr("disabled");
			$(".submit").removeAttr("disabled");
		}
	}

	function validateRadio()
	{
		var radio = $(".radio");

		if(radio.checked==false)
		{
				alert("You must select yes or no.");
				return false;
		}   else{
			$(".next").removeAttr("disabled");
			$(".submit").removeAttr("disabled");
		}

	}

	function mcvalidate()
	{
		mcselect = $(".mcselect").val(); 
		mcselect1 = $(".mcselect1").val(); 
		mczip = $(".mczip").val().trim();

		if (mcselect&&mcselect1 != null||"") {
			$(".submit").removeAttr("disabled");
			if (mczip === "") {
				$(".submit").prop('disabled', 'disabled');
			}else{
				$(".submit").removeAttr("disabled");
			}
		}else{
			$(".submit").prop('disabled', 'disabled');
		}

		
	}
	
	//drop-down
	var countChecked = function() {
	var n = $( "input:checked" ).length;
	(n >= 2 ? $(".submit").removeAttr("disabled") : $(".submit").prop('disabled', 'disabled'))
	};

	$( ".big-checkbox[type=checkbox]" ).on( "click", countChecked );

	$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
	if (!$(this).next().hasClass('show')) {
		$(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
	}
	var $subMenu = $(this).next(".dropdown-menu");
	$subMenu.toggleClass('show');

	$(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
		$('.dropdown-submenu .show').removeClass("show");
	});

	return false;
	});

</script>


   
@stack('scripts')