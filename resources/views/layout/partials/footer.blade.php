<footer>
	<div class="mu-footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="mu-footer-bottom-area">
						<p class="mu-copy-right">© 2018 PaymentFixer.com All Rights Reserved.</p>
						<p class="mu-copy-right">222 S. Main Street, 5th Floor, Salt Lake City, Utah, 8410 | Email: <a href="mailto:info@paymentfixer.com">INFO@PaymentFixer.com</a><!--Phone Number: 855-901-7220--></p>
						<p class="mu-copy-right">
						<a href="{{url('/terms-and-conditions')}}" target="_blank">Terms and Conditions</a> | 
						<a href="{{url('/our-privacy-notice')}}" target="_blank"> Our Privacy Notice</a> | 
						<a href="{{url('/ad-targetting-policy')}}" target="_blank"> Ad Targetting Policy</a> | 
						<a href="{{url('/licenses-and-disclosure')}}" target="_blank"> Licenses and Disclosures </a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>