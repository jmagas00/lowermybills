
<div class="mu-blog-sidebar">
    <div class="mu-sidebar-widget">
        <h2 class="mu-sidebar-widget-title">Other Types of Mortgages</h2>
        <ul class="mu-sidebar-widget-nav">
            <li><a href="{{url('/read-and-learn/veteran-loans')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Veteran Loans</a></li>
            <li><a href="{{url('/read-and-learn/fha-loans')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>FHA Loans</a>
            <li><a href="{{url('/read-and-learn/30-year-fixed-rate-mortgage')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>30 Year Fixed Rate Mortgage</a></li>
            <li><a href="{{url('/read-and-learn/fixed-vs-adjustable')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>What is the Difference Between a Fixed Rate Mortgage and an Adjustable Rate Mortgage?</a></li>
            <li><a href="{{url('/read-and-learn/cash-out')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Cash Out Refinancing</a></li>
        </ul>                                    
    </div>
    <div class="mu-sidebar-widget">
        <h2 class="mu-sidebar-widget-title">Mortgage Basics and How to Guides</h2>
        <ul class="mu-sidebar-widget-nav">
            <li><a href="{{url('/read-and-learn/expect-mortgage-process')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>What To Expect During the Mortgage Process</a></li>
            <li><a href="{{url('/read-and-learn/closing-costs-hidden-costs-come-mortgage')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>The Hidden Costs That Come With a Mortgage)</a></li>
            <li><a href="{{url('/read-and-learn/questions-ask-meeting-lender')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>What Questions Should You Ask Your Lender?</a></li>
            <li><a href="{{url('/read-and-learn/why-should-i-refinance-my-home')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Why Should I Refinance My Home?</a></li>
            <li><a href="{{url('/read-and-learn/youve-decided-buy-home-happens-now')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i>So You've Decided To Buy A Home, What Happens Now?</a></li>
        </ul>
    </div>
    <div class="mu-sidebar-widget">
        <h2 class="mu-sidebar-widget-title">Links To Other Tools</h2>
        <ul class="mu-sidebar-widget-nav">
            <li><a href="{{url('/mortgagecalculator')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/calculator.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Calculator</a></li>
            <li><a href="{{url('/refinancecalculator')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/money.svg')}}" alt="" style="max-width:15%;"> &nbsp; Refinance Calculator</a></li>
            <li><a href="{{url('/mortgagecomparisontool')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/comparision.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Comparison</a></li>
            <li><a href="{{url('/mortgageanalysis')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/analysis.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Analysis</a></li>
            <li><a href="{{url('/mortgageadvisor')}}" target="_blank"><i class="fa fa-angle-double-right" aria-hidden="true"></i><img src="{{asset('img/icons/lightbulb.svg')}}" alt="" style="max-width:15%;"> &nbsp; Mortgage Advisor</a></li>
        </ul>
    </div>
</div>