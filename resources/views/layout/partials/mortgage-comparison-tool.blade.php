<script>
	function updateCompare()
	{
		$('.comparison').show();

		if ($("#30-year-fixed").prop('checked'))
		{
			$('.30yf').show();
		}else{
			$('.30yf').hide();
		}
		 if ($("#15-year-fixed").prop('checked'))
		{
			$('.15yf').show();
		}else{
			$('.15yf').hide();
		}
		 if ($("#cash-out-refinance").prop('checked'))
		{
			$('.cashout').show();
		}else{
			$('.cashout').hide();
		}
		 if ($("#5-1-year-adjustable").prop('checked'))
		{
			$('.arm').show();
		}else{
			$('.arm').hide();
		}
		 if ($("#fha-loan").prop('checked'))
		{
			$('.fha').show();
		}else{
			$('.fha').hide();
		}
		 if ($("#va-loan").prop('checked'))
		{
			$('.va').show();
		}else{
			$('.va').hide();
		}
		 if ($("#harp-refinance").prop('checked'))
		{
			$('.harp').show();
		}else{
			$('.harp').hide();
		}
		
	}

</script>