<header id="mu-hero">
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light mu-navbar">
			<a class="navbar-brand mu-logo" href="/"><img src="{{asset('/img/logo.svg')}}" style="width:100%;"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fa fa-bars"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto mu-navbar-nav">
					<li class="nav-item dropdown">
						<a class="dropdown-toggle" href="#" role="button" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Mortgage Calculators</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="{{url('/mortgagecalculator')}}">Mortgage Calculator</a>
							<a class="dropdown-item" href="{{url('/refinancecalculator')}}">Refinance Calculator</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="dropdown-toggle" href="#" role="button" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Compare Mortgages</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="{{url('/mortgageanalysis')}}">Analyze My Mortgage</a>
							<a class="dropdown-item" href="{{url('/mortgageadvisor')}}">Mortgage Advisor</a>
							<a class="dropdown-item" href="{{url('/mortgagecomparisontool')}}">Mortgage Comparison Tool</a>
						</div>
					</li>
					<li class="nav-item dropdown">						
						<a class="dropdown-toggle" href="#" role="button" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Read &amp; Learn</a>	
						<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Types of Mortgages</a>
								<ul class="dropdown-menu">
									<li><a class="dropdown-item" href="{{url('/read-and-learn/what-is-an-interest-rate-reduction-refinance-loan')}}">What is an interest rate reduction <br>refinance load</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/what-is-a-home-equity-conversion-mortgage')}}">What is a Home Equity Conversion <br>Mortgage?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/15-year-fixed-rate-mortgage')}}">15-Year Fixed Rate Mortgage</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/what-is-a-subprime-mortgage')}}">What is a Subprime Mortgage?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/veteran-loans')}}">Veteran Loans</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/fha-loans')}}">FHA Loans</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/fixed-vs-adjustable')}}">What is the Difference Between<br> a FixedRate Mortgage and an <br>Adjustable Rate Mortgage?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/cash-out')}}">Cash Out Refinancing</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/15-year-vs-30-year')}}">15 Year Fixed vs 30 Year Fixed</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/30-year-fixed-rate-mortgage')}}">30 Year Fixed Rate Mortgage</a></li>
								</ul>
							</li>
							<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Mortgage Basics</a>
								<ul class="dropdown-menu">
									<li><a class="dropdown-item" href="{{url('/read-and-learn/what-is-the-difference-between-a-reverse-mortgage-and-a-cash-out-refinance')}}">What is the difference between<br> a Reverse Mortgage and a Cash <br>Out Refinance?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/why-are-interest-rates-so-important')}}">Should I Buy a Condo or a House?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/how-trumps-tax-cuts-and-job-act-could-affect-the-market')}}">How Trump’s Tax Cuts and <br>Job Act Could Affect the Market</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/adjustable-rate-mortgage')}}">What Is An Adjustable Rate Mortgage?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/different-types-mortgages')}}">Different Types of Mortgages</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/why-should-i-refinance-my-home')}}">Why Should I Refinance My Home?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/home-ownership-risks-come')}}">Home Ownership & The Risks That Come With It</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/condo-or-house')}}">Should I Buy a Condo or a House?</a></li>	
								</ul>
							</li>
							<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">First Time Home Buyers</a>
								<ul class="dropdown-menu">
									<li><a class="dropdown-item" href="{{url('/read-and-learn/what-is-homeowners-insurance')}}">What is Homeowners Insurance?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/what-is-a-mortgage-broker')}}">What is a Mortgage Broker?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/what-is-apr')}}">What is APR?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/should-i-see-my-bank-or-go-with-an-online-lender')}}">Should I See My Bank or Go With an Online Lender?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/should-i-continue-renting-or-buy-a-home')}}">Should I Continue Renting or Buy a Home?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/closing-costs-hidden-costs-come-mortgage')}}">The Hidden Costs That Come With a Mortgage</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/youve-decided-buy-home-happens-now')}}">So You’ve Decided To Buy A Home, What Happens Now?</a></li>
								</ul>
							</li>
							<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">How To Guides</a>
								<ul class="dropdown-menu">
								
									<li><a class="dropdown-item" href="{{url('/read-and-learn/buying-foreclosed-property')}}">Buying Foreclosed Property</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/how-to-buy-a-home-with-student-debt')}}">How to Buy a Home with Student Debt</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/how-to-buy-a-home-as-an-immigrant')}}">How To Buy a Home as an Immigrant</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/how-can-you-get-cash-from-your-home')}}">How Can You Get Cash From Your Home?</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/expect-mortgage-process')}}">What To Expect During the Mortgage Process</a></li>
									<li><a class="dropdown-item" href="{{url('/read-and-learn/questions-ask-meeting-lender')}}">What Questions Should You Ask Your Lender?</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li class="nav-item"><a href="{{url('')}}" data-toggle="modal" data-target="#exampleModal">Disclosure</a></li>
					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Disclosure</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<p style="text-align:left;">
										<strong>Paymentfixer</strong> doesn't endorse, warrant, or guarantee any mortgage product, company or service and makes no representation of any charges or fiscal applications.
									</p>
									<p style="text-align:left;">The information shown at the rate table doesn't take under account your own financial situation, confirmed credit rating, income, present debt, or other aspects. Actual prices and payment advice will change based on your particular circumstance. Tips are based on services and products available from creditors participating on our community or at the market generally, nevertheless it doesn't signify every financial services firm or financial product or service on the marketplace.</p>

									<p style="text-align:left;"><strong>PaymentFixer</strong> isn't a mortgage broker or lender. The information given by you to PaymentFixer isn't an application for a mortgage, nor can it be utilized to notify you with any creditor.</p>
								</div>
							</div>
						</div>
					</div>
				</ul>
			</div>
		</nav>
	</div>
</header>